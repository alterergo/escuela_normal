<?php
include '../includes/conexion.php';
session_start();

if(empty($_POST["estatus"])){
	$_SESSION["estatusDocente"]="activo";
	$estatus=$_SESSION["estatusDocente"];	
}else{	
	$_SESSION["estatusDocente"]=$_POST["estatus"];
	$estatus=$_SESSION["estatusDocente"];
}	


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrador Docentes</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>


<script>
	function docentes(){
	var elemento = document.getElementById("docentes");
	elemento.className = "negritas";
	}
</script>
</head>
<body onload="docentes();">
<div id="contenedorAdmin" class="row" ><!-- CONTENEDOR PRINCIPAL-->

    
    <?php include 'menu.php'; ?>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td width="15%" id="nuevoDocente" style="cursor:pointer; color:#0066CC; padding-top:17px"><img src="../images/add.png" class="icono" title="Nuevo Docente">Nuevo Docente</td>
        <td width="45%">&nbsp;</td>
        <td width="40%" id="buscador" align="right">
            <form method="POST" name="enviarEstatus">
		<label>Estatus</label>
		<select class="ui-widget-content" id="estatus" name="estatus" onChange="document.enviarEstatus.submit()">
			<!--option value="">Seleccione</option-->
			<option value="activo" <?php if($estatus == "activo"){ ?> selected="selected" <?php }?> >Activos</option>
			<option value="temporal" <?php if($estatus == "temporal"){ ?> selected="selected" <?php }?> >Baja Temporal</option>
			<option value="definitiva" <?php if($estatus == "definitiva"){ ?> selected="selected" <?php }?> >Baja Definitiva</option>
		</select>
		
                <input type="text" id="search" name="search" onKeyUp="buscarDocente()" placeholder="Nombre del Docente" class="ui-widget-content " autofocus="autofocus" /> <img src="../images/lupa.png" id="lupa" class="lupa" />
         </form>       
        </td>   
    </table>
   	
<br />
      <?
	  require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#	
	$query = "Select * from usuarios WHERE estatus='".$estatus."' AND tipo_usuario='2'";
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = ($_GET['page']);
	$cantidad = 20;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#

	?>
   <div id="myDiv"></div>
   <div id="mensaje_restau" class="mensajes" align="center"></div>
    <table id="tablaDocentes" cellpadding="15" cellspacing="0" border="0" class='table_cons'>
    <thead>
		<th width='15%'>ID</th>
        <th width='50%'>Matricula</th>
        <th width='35%'>Nombre</th>        
    </thead>
     <?    
         
	$i = 1;
	#--- EN LA CONSULTA DEBE IR EL LIMITE DE RESULTADOS PARA MOSTRAR ---#
	$consulta = "Select * from usuarios WHERE estatus='".$estatus."' AND tipo_usuario='2' ORDER BY apellido_pat ASC LIMIT $desde, $cantidad"; //echo ($consulta);
	$respuesta = mysql_query($consulta,$conexion);
	
	while($row = mysql_fetch_array($respuesta)){
		$id=$row['id_usuario'];
		$nombre=utf8_encode($row['nombre']);
		$ap_pat=utf8_encode($row['apellido_pat']);
		$ap_mat=utf8_encode($row['apellido_mat']);
		$nom_comp=$ap_pat." ".$ap_mat." ".$nombre;
		
	echo "<tr>
	<td>".$i."</td>
	<td>".$nom_comp."</td>";
	if ($estatus=='activo'):
	echo "<td>
		<img src='../images/pizarron.png' title='Materias Asignadas' onclick='materias_asignadas(".$id.")' class='icono' width='20'>
		<img src='../images/grade.png' title='Asignar Materias' onclick='asignar_materias(".$id.")' class='icono' width='20'>
		<img src='../images/info.png' title='Información' onclick='informacion_doc(".$id.")' class='icono' width='20'>
		<img src='../images/delete.png' title='Eliminar' onclick='eliminar_doc(".$id.")' class='icono' width='20'>
	</td>";
	
	elseif($estatus=='temporal'):
	echo "<td>		
		<img src='../images/info.png' title='Información' onclick='informacion_doc(".$id.")' class='icono' width='20'>
		<img src='../images/restaurar.png' title='Restaurar' onclick='restaurarTemporal(".$id.")' class='icono' width='20'>
	</td>";
	
	elseif($estatus=='definitiva'):
	echo "<td>	
		<img src='../images/info.png' title='Información' onclick='informacion_doc(".$id.")' class='icono' width='20'>	
	</td>";
	endif;
	
	echo "</tr>";
	$i++;
	}
	?>
          
    </table>
	<?
	
	#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div class='paginacion'>";
	$url = "index.php?";
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#--------------------------------------------------------------------#
	
	?>

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->


<!----------------------------- CONTENEDOR MATERIAS ASIGNADAS ------------------------------------------->
<div id="materias-asignadas" title="Materias Asignadas"> <div id="materiasAsignadas"></div> </div>

<!------------------------- DIV CONTENEDOR DE LA ASIGNACION DE MATERIAS A DOCENTES ----------------->
<div id="materias-Docente" title="Asignación de materias"> <div id="materiasDocente"></div> </div>

<!------------------------- DIV CONTENEDOR DE LA INFORMACION DEL DOCENTE ----------------->
<div id="dialog-info" title="Información del docente"> <div id="infoDocente"></div> </div>

<!--------------------------- CONTENEDOR ELIMINACION DEL DOCENTE--------------------------->
<div id="dialog-bajaDocente" title="Baja de docente"><div id="bajaDocente"></div></div>




<!-- INICIA FORMULARIO DE DOCENTE -->
<div id="dialog-form1" title="Nuevo Docente">
    <p class="validateTips">Complete los campos del formulario</p>
    <!-- div id="agregandoAlumno"></div -->
    <form id="agregarDocente">
        <fieldset>
        <legend>Datos Personales</legend>
        <div id="nvo_doc" class="mensajes"></div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:'Comic Sans MS', cursive;">
        <tr>
            <td width="25%">Nombre</td>
            <td width="45%"><input type="text" name="nombre" id="nombre" class='textinf' style='width:270px; text-transform:uppercase;' autofocus/>*</td>        
            <td width="30%" id="errNom" class="error" ></td>
        </tr>
    
        <tr>
            <td ><label for="apellido_paterno">Apellido Paterno</label></td>
            <td><input type="text" name="apellido_paterno" id="apellido_paterno" class='textinf' style='width:270px; text-transform:uppercase;' />*</td>
            <td id="errApp" class="error"></td>
        </tr>
        <tr>
            <td ><label for="apellido_materno">Apellido Materno</label></td>
            <td><input type="text" name="apellido_materno" id="apellido_materno" class='textinf' style='width:270px; text-transform:uppercase;' />*</td>
            <td id="errApm" class="error"></td>
        </tr>   
        <tr>
            <td ><label for="sexo">Sexo</label></td>
            <td>
                <select name="sexo" id="sexo">        
                    <option value="">Seleccione...</option>    
                    <option value="H">Hombre</option>
                    <option value="M">Mujer</option>            
              </select>*
            </td>
            <td id="errSexo" class="error"></td>
        </tr>    
        <tr>
            <td><label for="correo">Correo</label></td>
            <td><input type="text" name="correo" id="correo" class='textinf' style='width:270px; ' />*</td>
            <td id="errEmail" class="error"></td>
        </tr>  
        
        <tr>
        <td>Telefono</td>
        <td><input type="text" name="tel" id="tel" class='textinf' style='width:150px;' />
        *</td>
        <td id="errTel" class="error"></td>
        </tr>
        
        <tr>
        <td>Dirección</td>
        <td><input type="text" name="direcc" id="direcc" class='textinf' style='width:270px;' />
        *</td>
        <td id="errDirecc" class="error"></td>
        </tr>
        
        <tr>
        <td>RFC</td>
        <td><input type="text" name="rfc" id="rfc" class='textinf' style='width:150px; text-transform:uppercase;' />
        *</td>
        <td id="errRfc" class="error"></td>
        </tr>
        
        <tr>
        <td>CURP</td>
        <td><input type="text" name="curp" id="curp" class='textinf' style='width:150px; text-transform:uppercase;' />
        *</td>
        <td id="errCurp" class="error"></td>
        </tr>
        
        <tr>
        <td>Clave Presupuestal</td>
        <td><input type="text" name="clave_p" id="clave_p" class='textinf' style='width:150px;' /></td>
        <td id="errClave_p" class="error"></td>
        </tr>
         
         <tr>
        <td>Grado Academico</td>
        <td><input type="text" name="grad_acad" id="grad_acad" class='textinf' style='width:270px; text-transform:uppercase;' />
        *</td>
        <td id="errGrad_acad" class="error"></td>
        </tr>
        
        <tr>
        <td>Cedula Profesional</td>
        <td><input type="text" name="cedula" id="cedula" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td id="errCedula" class="error"></td>
        </tr>
          
        <tr>
            <td><label for="usuario">Usuario</label></td>
            <td><input type="text" name="usuario" id="usuario" class='textinf' style='width:150px;' />*</td>
            <td id="errUsu" class="error"></td>
        </tr>   
        <tr>
            <td><label for="pass">Contraseña</label></td>
            <td><input type="password" name="pass" id="pass" class='textinf' style='width:150px;' />*</td>
            <td id="errPass" class="error"></td>
        </tr>   
        
        
        </table><!-- </div> -->
        </fieldset>
            
    </form>

</div>
<!-- TERMINA FORMULARIO DE DOCENTE -->

<!-- INICIA FORMULARIO INFO DE ADMIN DOCENTE -->
<div id="dialog-infoadmin" title="Editar Información">
    
    <!-- div id="agregandoAlumno"></div -->
    <form id="infoadminDocente">
        <fieldset>
        <legend>Datos Personales</legend>
        <div id="updt_msg_adm_doc" class="mensajes" align="center"></div>
        <? $consulta_adm = "Select * from usuarios WHERE id_usuario='$id_usuario' "; 
	$respuesta_adm = mysql_query($consulta_adm,$conexion);
	
	while($consul = mysql_fetch_array($respuesta_adm)){		
		$nombre_con=utf8_encode($consul['nombre']);
		$ap_pat_con=utf8_encode($consul['apellido_pat']);
		$ap_mat_con=utf8_encode($consul['apellido_mat']);
		$user_con=$consul['user'];
		$pass_con=$consul['pass'];
		$sexo_con=$consul['sexo'];
		$correo_con=$consul['correo'];
		
	}
		?>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:'Comic Sans MS', cursive;">
        <tr>
            <td width="25%">Nombre</td>
            <td width="45%"><input type="text" name="nombre_edit" id="nombre_edit" class='textinf' style='width:270px; text-transform:uppercase;' autofocus value="<? echo $nombre_con?>"/>*</td>        
            <td width="30%" id="errNom_edit" class="error" ></td>
        </tr>
    
        <tr>
            <td ><label for="apellido_paterno_edit">Apellido Paterno</label></td>
            <td><input type="text" name="apellido_paterno_edit" id="apellido_paterno_edit" class='textinf' style='width:270px; text-transform:uppercase;' value="<? echo $ap_pat_con?>"/>*</td>
            <td id="errApp_edit" class="error"></td>
        </tr>
        <tr>
            <td ><label for="apellido_materno_edit">Apellido Materno</label></td>
            <td><input type="text" name="apellido_materno_edit" id="apellido_materno_edit" class='textinf' style='width:270px; text-transform:uppercase;' value="<? echo $ap_mat_con ?>" />*</td>
            <td id="errApm_edit" class="error"></td>
        </tr>   
        <tr>
            <td ><label for="sexo_edit">Sexo</label></td>
            <td>
                <select name="sexo_edit" id="sexo_edit">                           
                    <option value="H" <? if ($sexo_con=='H'):?> selected="selected" <? endif;?>>Hombre</option>
                    <option value="M" <? if ($sexo_con=='M'):?> selected="selected" <? endif;?>>Mujer</option>            
              </select>*
            </td>
            <td id="errSexo_edit" class="error"></td>
        </tr>    
        <tr>
            <td><label for="correo_edit">Correo</label></td>
            <td><input type="text" name="correo_edit" id="correo_edit" class='textinf' style='width:270px; ' value="<? echo $correo_con ?>" />*</td>
            <td id="errEmail_edit" class="error"></td>
        </tr>                 
           
        <tr>
            <td><label for="usuario_edit">Usuario</label></td>
            <td><input type="text" name="usuario_edit" id="usuario_edit" class='textinf' style='width:150px;' readonly="readonly" value="<? echo $user_con ?>" />*</td>
            <td id="errUsu" class="error"></td>
        </tr>   
        <tr>
            <td><label for="pass_edit">Contraseña</label></td>
            <td><input type="password" name="pass_edit" id="pass_edit" class='textinf' style='width:270px;' placeholder='Nueva contraseña' />*</td>
            <td id="errPass" class="error"></td>
        </tr>   
        </table><!-- </div> -->
        </fieldset>
            
    </form>

</div>
<!-- TERMINA FORMULARIO INFO DE DOCENTE -->


<!------------------------- DIV CONTENEDOR DE LA INFORMACION DEL ALUMNO ----------------->
<!-- div id="dialog-info" title="Información del alumno"><div id="infoAlumno"></div> </div -->

<div id="dialog-asig-materia" title="Asignacion de materias"> <div id="infoAlumno"></div> </div>




<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<!--script type="text/javascript" src="../js/liquidmetal.js"></script>
<script type="text/javascript" src="../js/domsearch.js"></script-->
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/jpopit.jquery.js"></script>
<!-- <script type="text/javascript" src="../js/funciones.js"></script> -->
<script type="text/javascript" src="../js/adm_doc.js"></script>
<script type="text/javascript" src="../js/jquery-validate.js"></script>
</body>
</html>