(function(){

/*
* Agregar Nuevo Plan
*/

$("#nuevoPlan").click(function(){
	$( "#dialog-nuevoPlan" ).dialog( "open" );
});

/*
* Form Agregar Plan
*/
$( "#dialog-nuevoPlan" ).dialog({
            autoOpen: false,
            height: 500,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
					
					if($("#nombrePlan").val()==""){
						$("#nombrePlan").focus();
						$("#errPlan").html("El campo Nombre es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#aniosPlan").val()==""){
						$("#aniosPlan").focus();
						$("#errAnios").html("El campo A&ntilde;os es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					var nombrePlan = $("#nombrePlan").val();
					var aniosPlan = $("#aniosPlan").val();
					$.post("ciclos_escolares/agregarPlan.php",
					{nombrePlan: nombrePlan,aniosPlan: aniosPlan},
					function(data){ 
						if(data == 1){
							//alert("ciclo agregado");
							$("#resultPlan").html(" Plan Agregado ");
							document.location.reload();
						}else{
							alert("Error al registrar el Ciclo");	
						}
					});
					
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


/////////////////////////// dialog editar plan estudios ///////////////
$( "#editarPlan" ).dialog({
            autoOpen: false,
            height: 350,
            width: 700,
            modal: true,
            buttons: {
                "Aceptar": function() {
					
					if($("#nuevoPlanEdt").val() == ""){
						$("#nuevoPlanEdt").focus();
						$("#errEditPlan").html("Escriba un nombre");
						return;
					}
					
					else {$("#errEditPlan").html();}
										
					 if($("#nuevoAnioEdt").val() == ""){					 
						$("#nuevoAnioEdt").focus();
						$("#errEditAnio").html("Escriba el a�o");
						return;
					}
					else {$("#errEditAnio").html();}
					
							$.post("ciclos_escolares/editarPlan_post.php",
								$("#form_editPlan").serialize(),
								function(data){ 
								//alert(data);	
									if(data == 1){
										//alert("ciclo modificado");
										$("#msg_editPlan").html("Modifici&oacute;n Exitosa").fadeIn(1000).delay(1000).fadeOut(1000);
										setTimeout("document.location.reload()",2000);
										}else{
										$("#msg_editPlan").html("No se pudo modificar");
										//alert("Error al modificar el Ciclo");	
										}
								});
								
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});
////////////////////////////////////////////////////////////////////


})();


/*****************EDITAR PLAN ESTUDIOS ***********************/
function editarPlan(id){
	//alert(id);
	$("#edit_Plan").load(
		"ciclos_escolares/editarPlan.php",
		{ idPlan: id },
		function(data){
			/*alert(data);*/  
			$( "#editarPlan" ).dialog( "open" );
	});
}