<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../index.php");
endif;

#------------------ VARIABLE SDE SESION PARA ESTATUTS -- ------------#
if(empty($_POST["estatus"])){
	$_SESSION["estatusAlumno"]="activo";
}else{
	$_SESSION["estatusAlumno"]=$_POST["estatus"];
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Alumnos</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>

</head>
<body onload="alumnos();">
<div id="contenedorAdmin" class="row">

<div id="headerAdmin" class="grid_11">
	<?php include 'menu.php'; ?>
</div>

<div id="contenido" class="grid_11" >
<?
if($tipo_usuario==5 || $tipo_usuario==4):
?>
    <div class="grid_3" id="nuevoAlumno" style="cursor:pointer;">
        <img src="../images/add.png"  class="icono">
        <span style="color:#0066CC;">Nuevo Alumno</span>
    </div>
<? endif; ?>
<div class="margen_7 search" id="buscador">
		<!--div id="contSistema" class="contEstatus">
			<form method="POST" name="enviarSistema">
			<label>Sistema</label>
			<select class="ui-widget-content" id="sistema" name="sistema" onChange="document.enviarSistema.submit()">
				<!--option value="">Seleccione</option>
				<option value="escolarizado" <?php /*if($_POST["sistema"] == "escolarizado"){ ?> selected="selected" <?php }?> >Escolarizado</option>
				<option value="mixto" <?php if($_POST["sistema"] == "mixto"){ ?> selected="selected" <?php }*/?> >Mixto</option>
			</select>
			</form>
		</div-->
		
		<div id="contEstatus" class="contEstatus">
		<form method="POST" name="enviarEstatus">
		<label>Estatus</label>
		<select class="ui-widget-content" id="estatus" name="estatus" onChange="document.enviarEstatus.submit()">
			<!--option value="">Seleccione</option-->
			<option value="activo" <?php if($_POST["estatus"] == "activo"){ ?> selected="selected" <?php }?> >Activos</option>
			<option value="temporal" <?php if($_POST["estatus"] == "temporal"){ ?> selected="selected" <?php }?> >Baja Temporal</option>
			<option value="definitiva" <?php if($_POST["estatus"] == "definitiva"){ ?> selected="selected" <?php }?> >Baja Definitiva</option>
		</select>
		</form>
		</div>
		
		<div id="contBusqueda" class="contBusqueda">
		Buscar <input type="text" id="search" name="search" onKeyUp="buscarAlumno()" placeholder="Nombre del alumno" class="ui-widget-content " autofocus /> 
		<img src="../images/lupa.png" id="lupa" class="lupa">
		</div>
</div>
<!--div class="margen_7"><span id="registros" class="restaurar">Lista de Bajas</span></div-->

<div id="myDiv" class="tooltip" align="center"></div>
<table id="tablaAlumnos" cellpadding="15" class="table_cons">
    <thead>
		<th>#</th>
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php 
	require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#
	if(empty($_POST["estatus"])){
		$estatus = "activo";
	}else{
		$estatus = $_POST["estatus"];
	}
	/*
	if(empty($_POST["sistema"])){
		$query = "Select * from alumnos WHERE estatus= '".$estatus."'";
	}else{
		$query = " SELECT * FROM ingreso, alumnos WHERE alumnos.id_alumno = ingreso.id_alumno AND ingreso.tipo_ingreso = 'mixto' AND alumnos.estatus = '$estatus'  ";
	}*/
	$query = "Select * from alumnos WHERE estatus= '".$estatus."'";
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = $_GET['page'];
	$cantidad = 25;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#
	
	$i = 1;
	#--- EN LA CONSULTA DEBE IR EL LIMITE DE RESULTADOS PARA MOSTRAR ---#		
	
	$consulta = " Select * from alumnos,ingreso WHERE alumnos.id_alumno = ingreso.id_alumno and alumnos.estatus='$estatus' and ingreso.tipo_ingreso <> 'mixto' ORDER BY alumnos.apellido_paterno ASC LIMIT $desde, $cantidad ";
	$respuesta = mysql_query($consulta,$conexion);
	while($row = mysql_fetch_array($respuesta)){
		echo "<tr>";
		echo "<td>".$i."</td>";
		echo "<td>".$row["matricula"]."</td>";
		echo "<td>". utf8_encode($row["apellido_paterno"].' '.$row["apellido_materno"].' '.$row["nombre"])."</td>";
		echo "<td>";
		if($estatus=="activo"){
			
		/******** Materias Reprobadas **********/
		if($tipo_usuario==5):
			echo "<img src='../images/materiaReprobada.png' title='Materias Reprobadas' id='reprobada' onClick='materiasReprobadas(".$row['id_alumno'].")' class='icono'>";
		endif;
		/***************** Grupos *********************/
		if($tipo_usuario==5):
			echo "<img src='../images/grupos.png' title='Grupos' id='grupos' onClick='grupoAlumno(".$row['id_alumno'].")' class='icono'>";
		endif;
		
		if($tipo_usuario==5):
			echo "<img src='../images/update.png' title='Reinscripcion de ". $row["nombre"]."' id='reinscipcion' onClick='inscripcion_reinscripcion(".$row['id_alumno'].")' class='icono'>";
		endif;
		
		echo "<img src='../images/document.png' title='Documentación' onclick='documentos(".$row['id_alumno'].")' class='icono'>";
		
		if($tipo_usuario==5 || $tipo_usuario==4):
			echo "<img src='../images/info.png' title='Información' onclick='informacion(".$row['id_alumno'].")' class='icono'>";
		endif;	
		
		if($tipo_usuario==5 || $tipo_usuario==4):	
			echo "<img src='../images/user_change.png' title='Cambio de Especialidad' id='cambioEspecialidad' onclick='cambio_especialidad(".$row['id_alumno'].")' class='icono'>";
		endif;	
			
			echo "<img src='../images/grade.png' title='Semestre Actual' id='semestreActual' onclick='semestre(".$row['id_alumno'].")' class='icono'>";
		
		if($tipo_usuario==5 || $tipo_usuario==4):
			echo "<img src='../images/delete.png' title='Eliminar' onclick='eliminar_alumno(".$row['id_alumno'].")' class='icono'>";
		endif;
		
		}elseif($estatus=="temporal"){
			if($tipo_usuario==5 || $tipo_usuario==4):
				echo "<img src='../images/restaurar.png' title='Restaurar' onclick='bajaTemporal(". $row["id_alumno"].")' class='icono'>";
			endif;
			
				echo "<img src='../images/document.png' title='Documentación' onclick='documentos(".$row['id_alumno'].")' class='icono'>";
			
			if($tipo_usuario==5 || $tipo_usuario==4):
				echo "<img src='../images/info.png' title='Información' onclick='informacion(".$row['id_alumno'].")' class='icono'>";
			endif;	
				
		}elseif($estatus=="definitiva"){
			if($tipo_usuario==5 || $tipo_usuario==4):
				echo "<img src='../images/info.png' title='Información' onclick='informacion(".$row['id_alumno'].")' class='icono'>";
			endif;
		}
		echo "</td>";
		echo "</tr>";
		$i++;
	}
	
?>
</tbody>
</table>

<?php
	#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div id='paginate' class='paginacion'>";
	$url = "index.php?";
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#--------------------------------------------------------------------#
?>
</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->

<!------------------------- DIV CONTENEDOR DEL SEMESTRE DEL ALUMNO ----------------->
<div id="dialog-semestre" title="Semestre del alumno">
<form id="semestreIngreso">
<fieldset>
<legend>Reinscripción</legend>
<div>
	<div class="grid_4">
    	<label>Semestre al que Ingresa: </label>
    </div>
    <div>
    	<select name="idIngreso">
        	
			<?php
            	$consultarSemestre = "SELECT * FROM semestre";
				$respuesta = mysql_query($consultarSemestre,$conexion);
				while($row = mysql_fetch_array($respuesta)){
			?>
            <option value="<?php echo $row["id_semestre"]; ?>"><?php echo $row["semestre"];?></option>
            <?php } ?>
        </select>
    </div>
</div>

</fieldset>
</form>
</div> 
</div>
<!------------------------- DIV CONTENEDOR DE LA RE-INSCRIPCION DEL ALUMNO ----------------->
<div id="dialog-reinscripcion" title="Reinscripción del alumno"> <div id="inscripcionAlumno"></div> </div>

<!------------------------- DIV CONTENEDOR DE LA INFORMACION DEL ALUMNO ----------------->
<div id="dialog-info" title="Información del alumno"> <div id="infoAlumno"></div> </div>

<!------------------------- DIV CONTENEDOR DEL SEMESTRE ACTUAL DEL ALUMNO ----------------->
<div id="dialog-semActual" title="Semestre actual del alumno"> 
	<div id="semAlumno"></div> <div id="aux" name="aux"> </div>
</div>
<!--------------------------- CONTENEDOR ELIMINACION --------------------------->
<div id="dialog-bajaAlumno" title="Baja del alumno"><div id="bajaAlumno"></div></div>

<!----------------------------- CONTENEDOR REGISTROS ELIMINADOS ------------------------------------------->
<div id="dialog-registrosEliminados" title="Registros eliminados"> <div id="registrosElimindaos"></div> </div>

<!------------------------------------ CONTENEDOR DE CAMBIO DE ESPECIALIDAD ------------------------------------>
<div id="dialog-cambioEspecialidad" title="Cambio de Especialidad"> <div id="frmCambioEspecialidad"></div> </div>

<!-------------------------- CONTENEDOR DE SELECCION DE FORMATOS -------------------------------->
<div id="dialog-formatos" title="Documentos"> <div id="contFormatos"></div> </div>

<!-- INICIA FORMULARIO DE ALUMNO -->
<div id="dialog-form" title="Crear nuevo alumno">
<p class="validateTips">Complete los campos del formulario</p>
<div id="agregandoAlumno"></div>
<form id="agregarAlumno">
    <fieldset>
    <legend>Datos Personales</legend>
    <!-- <div class="grid_9"> -->
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:'Comic Sans MS', cursive;">
    <tr>
        <td width="25%">Nombre(s)</td>
        <td width="45%"><input type="text" name="nombre" id="nombre" class='textinf' style='width:270px; text-transform:uppercase;' autofocus/>*</td>        
        <td width="30%" id="errNom" class="error" ></td>
    </tr>

	<tr>
        <td ><label for="apellido_paterno">Apellido Paterno</label></td>
        <td><input type="text" name="apellido_paterno" id="apellido_paterno" class='textinf' style='width:270px; text-transform:uppercase;' />*</td>
        <td id="errApp" class="error"></td>
    </tr>
    <tr>
        <td ><label for="apellido_materno">Apellido Materno</label></td>
        <td><input type="text" name="apellido_materno" id="apellido_materno" class='textinf' style='width:270px; text-transform:uppercase;' />*</td>
        <td id="errApm" class="error"></td>
    </tr>
    <tr>
        <td ><label for="edad">Edad</label></td>
        <td><input type="text" name="edad" id="edad" class='textinf' style='width:150px; ' />*</td>
        <td id="errEdd" class="error"></td>
    </tr>
    <tr>
        <td ><label for="sexo">Sexo</label></td>
   	  	<td>
        	<select name="sexo" id="sexo">        
            	<option value="">Seleccione...</option>    
                <option value="H">Hombre</option>
                <option value="M">Mujer</option>            
          </select>*
        </td>
        <td id="errSexo" class="error"></td>
    </tr>
    <tr>
        <td ><label for="calle">Calle</label></td>
        <td><input type="text" name="calle" id="calle" class='textinf' style='width:270px; ' />*</td>
        <td id="errCalle" class="error"></td>
    </tr>
    <tr>
    	<td><label for="numero_interior">Número Interior</label></td>
        <td><input type="text" name="numero_interior" id="numero_interior" class='textinf' style='width:150px; ' />*</td>
        <td id="errNumInt" class="error"></td>
    </tr>
    <tr>
    	<td><label for="numero_exterior">Número Exterior</label></td>
        <td><input type="text" name="numero_exterior" id="numero_exterior" class='textinf' style='width:150px; ' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="colonia">Colonia</label></td>
        <td><input type="text" name="colonia" id="colonia" class='textinf' style='width:270px; ' />*</td>
        <td id="errCol" class="error"></td>
    </tr>
    <tr>
    	<td><label for="codigo_postal">Código Postal</label></td>
        <td><input type="text" name="codigo_postal" id="codigo_postal" class='textinf' style='width:150px; ' />*</td>
        <td id="errC_P" class="error"></td>
    </tr>
    <tr>
    	<td><label for="telefono">Teléfono</label></td>
        <td><input type="text" name="telefono" id="telefono" class='textinf' style='width:150px; ' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="celular">Celular</label></td>
        <td><input type="text" name="celular" id="celular" class='textinf' style='width:270px; ' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="correo">Correo Electrónico</label></td>
        <td><input type="text" name="correo" id="correo" class='textinf' style='width:270px; ' />*</td>
        <td id="errEmail" class="error"></td>
    </tr>
    <tr>
    	<td><label for="curp">CURP</label></td>
        <td><input type="text" name="curp" id="curp" class='textinf' style='width:150px; text-transform:uppercase;' /></td>
    </tr>
    <tr>
    	<td><label for="lugar_nacimiento">Lugar Nacimiento</label></td>
        <td><input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class='textinf' style='width:270px; text-transform: lowercase;' />*</td>
        <td id="errLugNac" class="error"></td>
    </tr>
    <tr>
    	<td><label for="fecha_nacimiento">Fecha Nacimiento</label></td>
        <td>
        <select name="dia_nacimiento" id="dia_nacimiento">
		<option value="">Día..</option>
        	<? for($dia=1;$dia<=31;$dia++):?>			
        <option value="<? echo $dia?>"><? echo $dia ?></option>
        	<? endfor; ?>    
        </select>
        
        <select name="mes_nacimiento" id="mes_nacimiento">
		<option value="">Mes..</option>
        	<? for($mes=1;$mes<=12;$mes++):
			if($mes=='1'): $mes_n='Enero'; elseif($mes=='2'): $mes_n='Febrero'; elseif($mes=='3'): $mes_n='Marzo'; elseif($mes=='4'): $mes_n='Abril'; elseif($mes=='5'): $mes_n='Mayo'; elseif($mes=='6'): $mes_n='Junio'; elseif($mes=='7'): $mes_n='Julio'; elseif($mes=='8'): $mes_n='Agosto'; elseif($mes=='9'): $mes_n='Septiembre'; elseif($mes=='10'): $mes_n='Octubre'; elseif($mes=='11'): $mes_n='Noviembre'; elseif($mes=='12'): $mes_n='Diciembre'; endif;?>	
        <option value="<? echo $mes?>"><? echo $mes_n ?></option>
        	<? endfor; ?>    
        </select>
        
        <select name="anio_nacimiento" id="anio_nacimiento">
		<option value="">Año..</option>
        	<? $anio_actual=date("Y"); 
			for($anio=$anio_actual;$anio>=1960;$anio--):?>			
        <option value="<? echo $anio?>"><? echo $anio?></option>
        	<? endfor; ?>    
        </select>
        
        *</td>
        <td id="errFecNac" class="error"></td>
    </tr>
    <tr>
    	<td><label for="municipio">Municipio</label></td>
        <td><input type="text" name="municipio" id="municipio" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="estado">Estado</label></td>
        <td><input type="text" name="estado" id="estado" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="nacionalidad">Nacionalidad</label></td>
        <td><input type="text" name="nacionalidad" id="nacionalidad" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="lugar_procedencia_bachillerato">Procedencia del Bachillerato</label></td>
        <td><input type="text" name="lugar_procedencia_bachillerato" id="lugar_procedencia_bachillerato" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="nombre_bachillerato">Nombre del Bachillerato</label></td>
        <td><input type="text" name="nombre_bachillerato" id="nombre_bachillerato" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>            
    <tr>
    	<td><label for="sangre">Tipo Sanguineo</label></td>
        <td><input type="text" name="sangre" id="sangre" class='textinf' style='width:150px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="alergias">Alergias</label></td>
        <td><input type="text" name="alergias" id="alergias" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="enfermedades">Enfermedades Crónicas</label></td>
        <td><input type="text" name="enfermedades" id="enfermedades" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="edo_civil">Estado Civil</label></td>
        <td><select name="edo_civil" id="edo_civil">
        <option value="">Seleccione...</option>
        <option value="soltero">Soltero(a)</option>
        <option value="casado">Casado(a)</option>
        <option value="divorciado">Divorciado(a)</option>
        <option value="viudo">Viudo(a)</option>        
        </select>*</td>
        <td id="errCivil"></td>
    </tr>
    <tr>
    	<td><label for="hijos">Tiene Hijos</label></td>
        <td>Si <input type="radio" name="hijos" id="hijos" class='textinf' value="si" /> No <input type="radio" name="hijos" id="hijos" class='textinf' value="no" checked="checked"/></td>
        <td></td>
    </tr>
    
    
    </table><!-- </div> -->
    </fieldset>
    <!-- ///////////////////////////////////////////////////////////////-->
    <fieldset>
    <legend>Datos Padre o Tutor</legend>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:'Comic Sans MS', cursive;">
    <tr>
    	<td width="25%"><label for="padre_tutor">Nombre del Padre o Tutor</label></td>
        <td width="45%"><input type="text" name="padre_tutor" id="padre_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td width="30%"></td>
    </tr>
    <tr>
    	<td><label for="direccion_tutor">Dirección</label></td>
        <td><input type="text" name="direccion_tutor" id="direccion_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="colonia_tutor">Colonia</label></td>
        <td><input type="text" name="colonia_tutor" id="colonia_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="localidad_tutor">Localidad</label></td>
        <td><input type="text" name="localidad_tutor" id="localidad_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="municipio_tutor">Municipio</label></td>
        <td><input type="text" name="municipio_tutor" id="municipio_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="estado_tutor">Estado</label></td>
        <td><input type="text" name="estado_tutor" id="estado_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="tel_tutor">Telefono</label></td>
        <td><input type="text" name="tel_tutor" id="tel_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="cel_tutor">Celular</label></td>
        <td><input type="text" name="cel_tutor" id="cel_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    <tr>
    	<td><label for="mail_tutor">Correo Electrónico</label></td>
        <td><input type="text" name="mail_tutor" id="mail_tutor" class='textinf' style='width:270px; text-transform:uppercase;' /></td>
        <td></td>
    </tr>
    </table>
    </fieldset>
    <!-- ///////////////////////////////////////////////////////////////-->
    <fieldset>
    	<legend>Datos de Ingreso</legend>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:'Comic Sans MS', cursive;">
    <tr>
    	<td><label for="matricula">Matricula</label></td>
        <td><input type="text" name="matricula" id="matricula" class='textinf' style='width:150px; text-transform:uppercase;' />*</td>
        <td id="errMat" class="error"></td>
    </tr>
    <tr>
    	<td><label for="ciclo_escolar">Ciclo Escolar</label></td>
   	  <td>
            <select name="id_ciclo" id="id_ciclo" >
            
            	<!--option value="">Seleccione...</option-->
            <?php 
			$cicloEscolar = "select id_ciclo , ciclo_escolar from ciclo_escolar where activo = 1 order by en_curso DESC ";
			$consultar = mysql_query($cicloEscolar,$conexion);
			while($row = mysql_fetch_array($consultar)){
			?>
            	<option value="<?php echo $row["id_ciclo"]; ?>" <? if($row["en_curso"]=='1'):?> selected="selected" <? endif;?> ><?php echo $row["ciclo_escolar"];?></option>
			<?php
				}
			?>
            
            </select>
            *
            </td>
            <td id="errCiclo" class="error"></td>
    </tr>
    <!-- 
    	INICIA MODIFICACION PARA GRUPOS
     -->        
    <tr>
        <td>Tipo de Ingreso</td>
        <td>
        <input type="radio" name="nuevo" value="nuevo" id="nuevo" checked> Nuevo 
        <input type="radio" name="nuevo" id="traslado" value="traslado"> Traslado 
        </td>
        <td></td>
    </tr>
    
    <tr>
        <td width="25%"><label for="especialidad">Especialidad</label></td>
        <td width="45%">
        	<!--input type="text" id="especialidad" name="especialidad"/-->
        <select name="idEspecialidad" id="idEspecialidad" > 
        
        	<option value="">Seleccione...</option>
			<?php 
			$c_planEspe="";
            $consultaEspecialidad = "Select * from catalogoespecialidades where estatus = 1 order by plan_estudios ASC  ";
            $resultadoEspecialidad = mysql_query($consultaEspecialidad,$conexion);
            while($row = mysql_fetch_array($resultadoEspecialidad)){
				$c_idEspe=$row["idEspecialidad"];
				$c_planEspe=$row["plan_estudios"];
				
					$pl_es=mysql_query("SELECT plan_estudios FROM plan_estudios WHERE id_plan='$c_planEspe'",$conexion);
					if($nom_plan=mysql_fetch_assoc($pl_es)):
					$plan_nombre=$nom_plan['plan_estudios'];
					endif;
            
				if($c_planEspe!="" && $c_planEspe!=$plan_ante): //otro
			?>
            
            	<optgroup label="Plan <? echo $plan_nombre?>">
                <option value="<?php echo $c_idEspe ?>" > <?php echo utf8_encode($row["nombreEspecialidad"]); ?> </option>
               <?           
				else:
			?>	
					
                <option value="<?php echo $c_idEspe ?>" > <?php echo utf8_encode($row["nombreEspecialidad"]); ?> </option>
                    </optgroup>
			<?	
                endif;	
			$plan_ante=$c_planEspe;
            }
            ?>
         
        </select>
        *
        </td>  
        <td id="errEspe" class="error" width="30%"></td>              
        </tr>
        <!-- SE AGREGA EL CAMPO DE GRUPOS -->
        <tr>
        	<td>
        		<span id="etiquetaGrupo" class="oculto" style="display: none;">Grupo</span>
        	</td>
        	<td>
        		<div id="grupos" class="oculto" style="display: none;">
	        		<select id='grupoAlumno' name='grupoAlumno'>
	        			<option value=''> Seleccione... </option>
	        			<option value='A'> A </option>
	        			<option value='B'> B </option>
	        		</select>
        		</div>
        	</td>
        </tr>
        
           <tr >
<td  ><label for="semestre_entrada">Semestre de Ingreso</label></td>
<td>
<select id="semestre_entrada" name="semestre_entrada" style='width:150px;'>
        	           
            <?php
				$consultarSemAct = " SELECT * FROM semestre ";//limit $decremento, $incremento; 
				$respuestaSemAct = mysql_query($consultarSemAct,$conexion);
				while($row = mysql_fetch_array($respuestaSemAct)){
			?>
			<option value="<?php echo $row["id_semestre"]; ?>" style="text-transform:uppercase"><?php echo $row["semestre"];?></option>
			<?php
				}
			?>
            </select>
</td>            
<td id="errSemestre">&nbsp;</td>
        </tr>    

		</table>
        <!-- 
			TERMINA MODIFICACION DE GRUPOS 
		-->
        
    </fieldset>
	<div id="aviso" name="aviso" class="mensajes"></div>
</form>
</div><!-- TERMINA FORMULARIO -->

<!-- Div Contenedor del formulario de Grupos -->
<div id="dialog-editarGrupo" title="Editar Grupo"> <div id="frmEditarGrupo"></div> </div>

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/grupos.js"></script>
<script>
(function(){
	var $myObj = $("#idEspecialidad").change(function(){
					var $this = $(this);
					var idEspecialidad = $this.val();
					$.post("grupos/validarGrupo.php",
							{idEspecialidad: idEspecialidad},
							function(data){ 
								//alert(data);
								if(data[0]==1){
									//$("#grupos").append("<td>Grupo</td><td><select id='grupoAlumno' name='grupoAlumno'><option value='a'> A </option><option value='b'> B </option></select></td>");
									//$("#etiquetaGrupo").css("display","block");
									//$("#grupos").css("display","block");
									$(".oculto").css("display","block");
								}else{
									//$("#etiquetaGrupo").css("display","none");
									//$("#grupos").css("display","none");
									$(".oculto").css("display","none");
								}
					});
				});
})();

/*******************************************************************************
					FUNCION DE MATERIAS REPROBADAS
/*******************************************************************************/

function materiasReprobadas(id){
	//alert("Reprobado: "+id);
	document.location.href='materiasReprobadas.php?id='+id;	
}


</script>
</body>
</html>