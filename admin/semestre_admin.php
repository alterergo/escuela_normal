<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../index.php");
endif;
//echo $id_usuario;

$idEsp = $_GET["idEsp"];
$_SESSION["id_esp"] = $idEsp;
$id_esp = $_SESSION["id_esp"];



//echo $idEsp;

$consultaEsp = "SELECT nombreEspecialidad FROM catalogoespecialidades WHERE idEspecialidad = $idEsp";
$respuestaEsp = mysql_query($consultaEsp,$conexion);

while($row = mysql_fetch_array($respuestaEsp)){
	$nomEsp = $row["nombreEspecialidad"];
	$plan=$row['plan_estudios'];
}


if(empty($_POST["estatus"])){
	$_SESSION["estatusSem"]=1;
}else{
	$_SESSION["estatusSem"]=$_POST["estatus"];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Semestres</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>
</head>
<body>
<div id="contenedorAdminSemestres" class="row">

<div id="headerAdmin" class="grid_11">
	<?php include 'menu.php'; ?>
</div>

<div id="contenido" class="grid_11">
<? if($tipo_usuario==5 || $tipo_usuario==4): ?>
<div class="grid_3" style="cursor:pointer" id="nuevoSemestre"><img src="../images/agregar.png"  class="icono"><span style="color:#0066CC;">Nuevo Semestre</span></div>
<? endif; ?>
<div class="grid_4"><b><?php echo utf8_encode($nomEsp); ?> / SEMESTRES</b></div>
<div class="grid_2"><img src="../images/atras.png" class="icono" id="regresarEsp" name="regresarEsp"></div>
<div class="margen_7 search" id="buscador">Buscar <input type="text" id="searchSem" name="searchSem" onKeyUp="buscarSemestre()" placeholder="Semestre" class="ui-widget-content "/> <img src="../images/lupa.png" id="lupa" class="lupa"></div>
<div class="margen_7">
	<!--span id="registrosSem" class="restaurar">Lista de Bajas</span-->
	<div id="contEstatus" class="contEstatus">
		<form method="POST" name="enviarEstatus">
		<label>Estatus</label>
		<select class="ui-widget-content" id="estatus" name="estatus" onChange="document.enviarEstatus.submit()">
			<!--option value="">Seleccione</option-->
			<option value="1" <?php if($_POST["estatus"] == 1){ ?> selected="selected" <?php }?> >Activos</option>
			<option value="2" <?php if($_POST["estatus"] == 2){ ?> selected="selected" <?php }?> >Inactivos</option>
		</select>
		</form>
	</div>
</div>
<div id="myDiv" class="tooltip"></div>
<table id="tablaSemestres" cellpadding="15" class="table_cons">
    <thead>
        <th>#</th>	
        <th>Semestre</th>
        <th>Grado</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php 
	require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#
	if(empty($_POST["estatus"])){
		$estatus = 1;
	}else{
		$estatus = $_POST["estatus"];
	}
	$query = "SELECT * FROM sem_espe where estatus = ".$estatus." AND id_especialidad='$idEsp' ORDER BY id_semestre ASC";
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = $_GET['page'];
	$cantidad = 10;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#
	
	$i = 1;
	$consulta = "select * from sem_espe where estatus = '$estatus' AND id_especialidad='$idEsp' ORDER BY id_semestre ASC LIMIT $desde, $cantidad ";
	$respuesta = mysql_query($consulta,$conexion);
	while($row = mysql_fetch_array($respuesta)){
		$id_s=$row['id_semestre'];
	echo "<tr>";
	echo "<td>".$i."</td>";
	
		$se_da=mysql_query("SELECT * FROM semestre WHERE id_semestre='$id_s'",$conexion);
		if($s_d=mysql_fetch_assoc($se_da)):
			$sem_nombre=$s_d['semestre'];
			$grado_nombre=$s_d['grado'];
		endif;
		
	echo "<td>".utf8_encode($sem_nombre)."</td>";
	echo "<td>".utf8_encode($grado_nombre)."</td>";
	echo "<td>";
	if($estatus == 1){
	echo "<img src='../images/materias.png' title='Materias' onClick='materias(".$row['id_semestre'].",".$idEsp.")' class='icono'>";
	
		if($tipo_usuario==5 || $tipo_usuario==4):
		echo "<img src='../images/editar.png' title='Editar' onClick='editarSemestre(".$row['id_semestre'].")' class='icono'> ";
		endif;
		//<img src='../images/eliminar.png' title='Eliminar' onclick='eliminarSemestre(".$row['id_semestre'].")' class='icono'>
	
	}elseif($estatus == 2){
		if($tipo_usuario==5 || $tipo_usuario==4):
		echo "<img src='../images/restaurar.png' title='Restaurar' onclick='restaurarSem(".$row['id_semestre'].")' class='icono'>";
		endif;
	}
	echo "</td>";
	echo "</tr>";
	$i++;
	}
?>
</tbody>
</table>
<?php
	#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div id='paginate' class='paginacion'>";
	$url = "semestre_admin.php?idEsp=".$idEsp;
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#--------------------------------------------------------------------#
?>
</div><!-- TERMINA CONTENIDO -->
<!---------------- FORMULARIOS ------------------->

<div id="dialog-semestres" title="Agregar Semestre">

<form id="semestre">
<fieldset>
<legend>Nuevo Semestre</legend>
<div>
<div id="msg_semestre" class="mensajes"></div>
	<div class="grid_4">
		<label>Nombre del Semestre</label>
	</div>
	<div class="grid_4">
		
        
        <select name="nombreSemestre" id="nombreSemestre">
        <option value="">Seleccione...</option>
        <? $semes=mysql_query("SELECT * FROM semestre",$conexion);
			while($sem=mysql_fetch_assoc($semes)):
			$id_semestre=$sem['id_semestre'];
			$semestre=$sem['semestre'];
			$grado=$sem['grado'];
			 echo "<option value='".$id_semestre."'>".$semestre."</option>";	
			
			endwhile;
		 ?>
  
        </select>
        
        
		<br/><span class="error" id="errNombreSemestre"></span>
	</div>
	
</div>

<div>
	
	
</div>
<input type="hidden" name="id_espe_hidd" id="id_espe_hidd" value="<? echo $idEsp?>" />
</fieldset>
</form>
</div>
<!---------------------- CONTENEDOR PARA EDITAR SEMESTRE ---------------------------------------->
<div id="dialog-editarSemestres" title="Editar Semestre"> <div id="formEdtSemestre"></div> </div>

<div id="dialog-restaurarSem" title="Restaurar Semestres"> <div id="frmRestaurarSem"></div> </div>

<!----------- TERMINAN FORMULARIOS --------------->
</div>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/liquidmetal.js"></script>
<script type="text/javascript" src="../js/domsearch.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
</body>
</html>