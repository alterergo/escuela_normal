<?php
	include '../../includes/conexion.php';

	$cicloEscolarPost = $_GET["ciclo"];
	$planEstudiosPost = $_GET["plan"];
	
	//$cicloEscolarPost = 1;
	///$planEstudiosPost = 1;

	$reprobados = mysql_query("
							SELECT 
								distinct t5.id_alumno,
								count(t5.id_alumno),
								t1.nombreEspecialidad,
								t3.id_semestre,
								AVG(t5.calificacion)
							FROM 
								catalogoespecialidades AS t1,
								ingreso AS t2,
								sem_espe AS t3,
								materias AS t4,
								calif_parc AS t5
							WHERE 
								t1.plan_estudios = '$planEstudiosPost'
							AND 	t2.id_especialidad = t1.idEspecialidad
							AND		t3.id_especialidad = t2.id_especialidad 
							AND 	t4.id_semestre = t3.id_semestre 
							AND 	t4.id_especialidad = t1.idEspecialidad
							AND		t4.estatus = 1 
							AND 	t5.id_alumno = t2.id_alumno
							AND 	t5.id_materia = t4.id_materia
							AND 	t5.ciclo_escolar = '$cicloEscolarPost'
							AND 	t5.calificacion < 6
							GROUP BY t3.id_semestre
							",$conexion);
							
	//GROUP BY t1.nombreEspecialidad, t3.id_semestre -- distinct t5.id_alumno,
?>

<div style="border: #000 solid 1px; width: 816px; height: 1056px; ">
<table width='100%'>
<tr>
	<td align='center' width='80%'><b>ESCUELA NORMAL SUPERIOR FEDERALIZADA DEL ESTADO DE PUEBLA</b></td>
	<td width='20%'><img src="../../images/logo.png" width='100' /></td>
</tr>
</table>

<table border='1' cellpadding='0' cellspacing='0'>
<tr>
	<th> Semestre y Especialidad </th>
	<th> % de Alumnos <br/> Reprobados</th>
</tr>
<?php
		while($reprobadosResult = mysql_fetch_assoc($reprobados)){
			$especialidad = utf8_encode($reprobadosResult["nombreEspecialidad"]);
			$semestre = $reprobadosResult["id_semestre"];
			$porcentage = $reprobadosResult["AVG(t5.calificacion)"];
			$total = $reprobadosResult["count(t5.id_alumno)"];
			
			$resultado = $porcentage/$total;
			
			
?>
<tr>
	<td><b><?php echo $semestre." Sem. ".$especialidad; ?></b></td>
	<td align="center"> <?php echo number_format($resultado,1); ?> </td>
</tr>
<?php
		}
?>

</table>
<br/><br/>

<div style="text-align: center; ">
		<img src="alumnosReprobados_grafica.php" />
	</div>

</div>