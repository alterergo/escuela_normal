(function($){

	
	$( "#dialog-editarGrupo" ).dialog({
		autoOpen: false,
		modal: true,
		width: 400,
		height: 300,
		buttons: [
			{
				text: "Asignar",
				click: function() {
					// $( this ).dialog( "close" );
					var grupo = $("#grupoEditar").val();
					var idAlumno = $("#idAlumno").val();
					//alert(grupo+" "+idAlumno);
					
					var confirmar = confirm("Desea editar el Grupo de este Alumno?");
					if(confirmar){
						
						$.post("grupos/editarGrupo.php",
								{idAlumno: idAlumno, 
								grupo: grupo},
								function(data){ 
									//alert(data);
									if(data[0]==1){
										$("#result").html("Grupo Actualizado").fadeIn(1000).delay(1000).fadeOut(1000, function(){
                                                                                    $( "#dialog-editarGrupo" ).dialog( "close" );                                                    
                                                                                });
										//document.location.reload();
									}else{
										$("#result").html("Error. Intente mas tarde.").fadeIn(1000).delay(1000).fadeOut(1000);
										document.location.reload();
									}
								});
						
					}else{
						return;
					}
				}
			},
			{
				text: "Cancel",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
})(jQuery);

function grupoAlumno(id){
	//alert(id);
	$("#frmEditarGrupo").load("grupos/frmEditarGrupos.php",
			{idAlumno: id},
			function(data){ 
				$( "#dialog-editarGrupo" ).dialog( "open" ); 
			});
}