$(document).ready(function(){

	/********** tooltip ************/
	 $( ".icono" ).tooltip({
		show: null,
		position: {
		my: "left top",
		at: "left bottom"
		},
		open: function( event, ui ) {
		ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
		}
	});

	$( ".tooltip" ).tooltip({
		show: null,
		position: {
		my: "left top",
		at: "left bottom"
		},
		open: function( event, ui ) {
		ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
		}
	});
	
	/***************************************************************************/
	/******************** AGREGAR ADMIN ****************************************/
	/***************************************************************************/
	function validar_email(valor)
    {
        // creamos nuestra regla con expresiones regulares.
        var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        // utilizamos test para comprobar si el parametro valor cumple la regla
        if(filter.test(valor))
            return true;
        else
            return false;
    }
	
	
	$( "#nuevoAdministrativo" ).click(function() {
        $( "#dialog-nuevoUser" ).dialog( "open" );
    });
	
	
	$( "#dialog-nuevoUser" ).dialog({
            autoOpen: false,
            height: 500,
            width: 800,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    
					if($("#nombreAdmin").val() == ""){
						$("#nombreAdmin").focus();
						$("#errNom").html("El campo Nombre es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#apellidoPat").val() == ""){
						$("#apellidoPat").focus();
						$("#errApp").html("El apellido Paterno es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#apellidoMat").val() == ""){
						$("#apellidoMat").focus();
						$("#errApm").html("El apellido Materno es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#user").val() == ""){
						$("#user").focus();
						$("#errUsr").html("El Usuario es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#pass").val() == ""){
						$("#pass").focus();
						$("#errPas").html("El Pasword es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#sexo").val() == ""){
						$("#sexo").focus();
						$("#errSex").html("El Sexo de la persona es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#correo").val() == ''){
						$("#errCorreo").html("Ingrese un correo");
						
					}else if(validar_email($("#correo").val()))
					{
						$("#errCorreo").html("");
					}else
					{
						$("#errCorreo").html("El correo no es valido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#privilegios").val() == ""){
						$("#privilegios").focus();
						$("#errPriv").html("Los Privilegios es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					$.post("administrador/crearUsuario.php",
						$("#frmNuevoAdmin").serialize(),
						function(data){
							//alert(data);
							$("#result").html("<p>Usuario Registrado</p>");
							setTimeout("location.href='administrador.php'",2000);
							//document.location.reload();
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });
	
	/*************************************************************************/
	/*********************** FORMULARIO DE EDICION DE USUARIO ****************/
	/*************************************************************************/
	$( "#dialog-editarUsuario" ).dialog({
            autoOpen: false,
            height: 500,
            width: 800,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    
					if($("#nombreEdtUsr").val() == ""){
						$("#nombreEdtUsr").focus();
						$("#errNomEdt").html("El campo Nombre es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#apellidoPatEdt").val() == ""){
						$("#apellidoPatEdt").focus();
						$("#errAppEdt").html("El apellido Paterno es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#userEdt").val() == ""){
						$("#userEdt").focus();
						$("#errUsrEdt").html("El Usuario es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#sexoEdt").val() == ""){
						$("#sexoEdt").focus();
						$("#errSexEdt").html("El Sexo de la persona es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#correoEdt").val() == ''){
						$("#errCorreoEdt").html("Ingrese un correo");
						
					}else if(validar_email($("#correoEdt").val()))
					{
						$("#errCorreoEdt").html("");
					}else
					{
						$("#errCorreoEdt").html("El correo no es valido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					if($("#privilegiosEdt").val() == ""){
						$("#privilegiosEdt").focus();
						$("#errPrivEdt").html("Los Privilegios es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					
					$.post("administrador/editarUsuario.php",
						$("#frmEdtUsr").serialize(),
						function(data){
							//alert(data);
							if(data==1){
								$("#resultEdt").html("<div class='mensajes'>Usuario Modificado</div>");
								setTimeout("location.href='administrador.php'", 2200);
							}else{
								$("#resultEdt").html("<div class='error'>Ocurrio un error. Intentelo de nuevo</div>");
								setTimeout("location.href='administrador.php'", 2200);
							}
							
							
							//document.location.reload();
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });
	
	
});



/********************* ESTILO A LA OPCION DEL MENU *********/
function adminstrativo(){
	var elemento = document.getElementById("administrador");
	elemento.className = "negritas";
}

function editarUsuario(id){
	//alert(id);
	$("#frmEditarUsuario").load("administrador/frmEditarUsuario.php",{id: id},function(data){ $( "#dialog-editarUsuario" ).dialog( "open" ); });
	
}

function restaurarUser(id){
	//alert(id);
	var acept = confirm("Desea Restaurar al Usuario?");
	if(acept){
		$.ajax({
			url: 'administrador/restaurarUsuario.php',
			type: 'POST',
			async: true,
			data: 'idUser='+id,
			dataType: 'html',
			success: function(data){
			//alert(data);
				if(data == 1){
					//alert("Consulta hecha");
					$("#myDiv").html("<div class='grid_12 mensajes'>Usuario Restaurado</div>");
					setTimeout("location.href='administrador.php'", 2200);
				}else{
					//alert("Error al hacer la consulta");
					$("#myDiv").html("<div class='grid_12 error'>Hubo un Error. Intentelo Nuevamente</div>");
					setTimeout("location.href='administrador.php'", 2200);
				}
			},
			error: function(data){
				alert("Error General");
			}
		});
		
	}else{
		document.location.reload();
	}
}

function eliminarAdmin(id){
	//alert(id);
	var baja = confirm("Desea dar de baja a este Usuario?");
	if(baja){
		$.ajax({
			url: 'administrador/bajaAdministrador.php',
			type: 'POST',
			async: true,
			data: 'idUser='+id,
			dataType: 'html',
			success: function(data){
			//alert(data);
				if(data == 1){
					//alert("Consulta hecha");
					//document.location.reload();
					$("#myDiv").html("<div class='grid_12 mensajes'>Usuario Dado de Baja</div>");
					setTimeout("location.href='administrador.php'", 2200);
				}else{
					//alert("Error al hacer la consulta. Intentelo de nuevo");
					$("#myDiv").html("<div class='grid_12 mensajes'>Ocurrio un Error. Intente Nuevamente</div>");
					setTimeout("location.href='administrador.php'", 2200);
				}
			},
			error: function(data){
				alert("Error General");
			}
		});
		
	}else{
		
	}
	
}

//BUSCADOR DE ADMINISTRADOR
function buscarAdministrativo(){
	var xmlhttp;
	var n=document.getElementById('searchAdministrador').value;
	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaAdministrador").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{ 
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />'; 
			document.getElementById("tablaAdministrador").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	xmlhttp.open("POST","busquedaAdministrador.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
}