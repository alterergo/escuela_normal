<?php
require('fpdf/fpdf.php');
require('fpdf/fpdfHTML.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;


$id_alumno=$_GET['id'];
//$id_alumno=231;
$dia=strtoupper(utf8_encode($_GET['dia']));
$mes=strtoupper(utf8_encode($_GET['mes']));
$anio=strtoupper(utf8_encode($_GET['anio']));

$fecha=date("d-m-Y");	
$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	//$grado = $row["grado"];
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	
	function Header()
	{
		 //$this->Image('titulo.png',0,0,216);
		 //$this->Image('titulo_blanco.jpg',0,0,216);
		//$this->Image('lunketec_logo.png',186,8,17);
	}
	/*
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf=new PDF('P','mm',array(216,290));//PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetFont('Helvetica','B',14); //Times, Helvetica, Arial, Courier

//NOMBRE GOBIERNO
$pdf->SetXY(42,66);
$pdf->Cell(50,4,"EL GOBIERNO CONSTITUCIONAL DEL ESTADO DE PUEBLA",0,0,'L');

// NOMBRE ALUMNO
$pdf->SetXY(79,110);
$pdf->Cell(50,3,"".utf8_decode($nombre_completo)."",0,0,'L');

//ESPECIALIDAD
$pdf->SetFont('Arial','',12);
$pdf->SetXY(61,142);
$pdf->Cell(50,4,utf8_decode("LICENCIADO EN EDUCACIÓN SECUNDARIA CON"),0,0,'L');
$pdf->SetXY(79,151);
$pdf->Cell(50,4,"ESPECIALIDAD EN ".$nombreEspecialidad." ",0,0,'L');

// ESCUELA
$pdf->SetXY(60,170);
$pdf->Cell(50,4,"LA ESCUELA NORMAL SUPERIOR FEDERALIZADA",0,0,'L');
$pdf->SetXY(84,177);
$pdf->Cell(50,4,"DEL ESTADO DE PUEBLA",0,0,'L');

// EXPEDIDO
$pdf->SetXY(62,216);
$pdf->Cell(50,4,"SAN JUAN B. CUAUTLANCINGO, PUEBLA",0,0,'L');

$pdf->SetXY(48,225);
$pdf->Cell(50,4,"".$dia."",0,0,'L');

$pdf->SetXY(147,225);
$pdf->Cell(50,4,"".$mes."",0,0,'L');

$pdf->SetXY(30,229);
$pdf->Cell(50,4,"DEL ".$anio."",0,0,'L');

//FIRMAS
$pdf->SetXY(22,254);//30,249
$pdf->Cell(10,4,utf8_decode("JORGE BENITO CRUZ BERMÚDEZ"),0,0,'L');
$pdf->SetFont('Arial','',7);
$pdf->SetXY(15,260);//23,255
$pdf->Cell(50,4,utf8_decode("SECRETARIO DE EDUCACIÓN PÚBLICA DEL ESTADO DE PUEBLA"),0,0,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetXY(131,254);//131,249
$pdf->Cell(50,4,"RAFAEL MORENO VALLE ROSAS",0,0,'L');
$pdf->SetFont('Arial','',7);
$pdf->SetXY(129,260);//129,255
$pdf->Cell(50,4,"GOBERNADOR CONSTITUCIONAL DEL ESTADO DE PUEBLA",0,0,'L');


$pdf->Output("Titulo frente","I");

?>