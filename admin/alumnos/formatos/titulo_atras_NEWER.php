<?php
require('fpdf/fpdf.php');
require('fpdf/fpdfHTML.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$id_alumno=$_GET['id'];
//$id_alumno=231;
$generacion=$_GET['G'];
$anio=$_GET['A'];
$numero=$_GET['N'];
$fecha=$_GET['F'];
$titulo_num=$_GET['TN'];
//$fecha=date("d-m-Y");	
$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	//$grado = $row["grado"];
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	$curp=$row['curp'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	
	function Header()
	{		 
		 //$this->Image('titulo atras.jpg',0,0,216);
		//$this->Image('lunketec_logo.png',186,8,17);
	}
	/*
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf = new PDF_HTML('P','mm','Letter');
//$pdf=new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetFont('Arial','',10); //Times, Helvetica, Arial, Courier

// NOMBRE
$pdf->SetXY(127,11);
$pdf->Cell(50,3,"".utf8_decode($nombre_completo)."",0,0,'L');

//CURP
$pdf->SetXY(127,24);
$pdf->Cell(50,4,"".$curp."",0,0,'L');

//clave ESCUELA
$pdf->SetXY(150,46);
$pdf->Cell(50,4,"21DNS0001J",0,0,'L');

//PLAN
$pdf->SetXY(165,54);
$pdf->Cell(50,4,"".$plan."",0,0,'L');

//GENERACION
$pdf->SetXY(170,63);
$pdf->Cell(50,4,"".$generacion."",0,0,'L');

//AÑO EGRESO
$pdf->SetXY(170,71);
$pdf->Cell(50,4,"".$anio."",0,0,'L');

//# ACTA
$pdf->SetXY(160,84);//86 - OK
$pdf->Cell(50,4,$numero,0,0,'L');

//FECHA
$pdf->SetXY(160,93);//95 - OK
$pdf->Cell(50,4,"".$fecha."",0,0,'L');

//EXPEDIDA
$pdf->SetXY(151,101);//103 - OK
$pdf->Cell(50,4,"CUAUTLANCINGO, PUEBLA",0,0,'L');

//TITULO #
$pdf->SetXY(160,109);//111 - OK
$pdf->Cell(50,4,"".$titulo_num."",0,0,'L');

//FIRMA
$pdf->SetY(177);//175 - OK
$pdf->SetX(133);
$pdf->WriteHTML('<b>'.utf8_decode("GERARDO PAUL ARVIZU SERAPIO").'</b>');
//$pdf->SetXY(8,69);
//$pdf->Cell(83,4,"_________________________________________",0,0,'L');
$pdf->SetXY(138,184);//182 - OK
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE LA ESCUELA"),0,0,'L');

//---------------------------------
//---------LADO DERECHO----------//
//---------------------------------

/************ENCABEZADOS**********/
//REGISTRO
//$pdf->SetXY(8,26);
//$pdf->Cell(50,4,"REGISTRO:",0,0,'L');

//LIBRO NUM
//$pdf->SetXY(8,33);
//$pdf->Cell(50,4,"LIBRO NUM:",0,0,'L');

//FOJA NUM
//$pdf->SetXY(8,40);
//$pdf->Cell(50,4,"FOJA NUM:",0,0,'L');

//LUGAR
//$pdf->SetXY(8,47);
//$pdf->Cell(50,4,"LUGAR:",0,0,'L');

//FECHA
//$pdf->SetXY(8,54);
//$pdf->Cell(50,4,"FECHA:",0,0,'L');

/**********VALORES****************/

//REGISTRO
$pdf->SetXY(33,18);
$pdf->Cell(33,4,"".$_GET['AD1']."",0,0,'L');
//$pdf->SetXY(33,27);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//LIBRO NUM
$pdf->SetXY(33,26);
$pdf->Cell(33,4,"".$_GET['AD2']."",0,0,'L');
//$pdf->SetXY(33,34);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FOJA NUM
$pdf->SetXY(33,34);
$pdf->Cell(33,4,"".$_GET['AD3']."",0,0,'L');
//$pdf->SetXY(33,41);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//LUGAR
$pdf->SetXY(33,43);
$pdf->Cell(33,4,"".utf8_decode($_GET['AD4'])."",0,0,'L');
//$pdf->SetXY(33,48);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FECHA
$pdf->SetXY(33,51);
$pdf->Cell(33,4,"".$_GET['AD5']."",0,0,'L');
//$pdf->SetXY(33,55);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FIRMA
$pdf->SetY(68);//70 - OK
$pdf->SetX(23);
$pdf->WriteHTML('<b>'.utf8_decode("ALEJANDRO GÓMEZ GARCÍA").'</b>');
//$pdf->SetXY(8,69);
//$pdf->Cell(83,4,"_________________________________________",0,0,'L');
$pdf->SetXY(10,75);//77 - OK
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE CONTROL Y GESTIÓN ESCOLAR"),0,0,'L');

//-------------------------------
$pdf->Output("Titulo atras","I");
//-------------------------------
?>