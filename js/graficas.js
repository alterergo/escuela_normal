(function(){

/********** tooltip ************/
 $( ".icono" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});

$( ".tooltip" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});

/*
*	Porcentaje de bajas
*/

$( "#dialog-porcentajeBajas" ).dialog({
	autoOpen: false,
	width: 400,
	height: 300,
	modal: true,
	buttons: [
		{
			text: "Ok",
			click: function() {
				//$( this ).dialog( "close" );
				var ciclo = $("#cicloEscolar").val();
				//alert(ciclo);
				$.post("graficas/bajas_grafica.php",{ciclo: ciclo});
				window.open('graficas/verGrafica.php?ciclo='+ciclo,'Graficas','scrollbars=1,width=999,height=800');
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

/*
*	Porcentaje de reinscripcion
*/
$( "#dialog-porcentajeInscripcion" ).dialog({
	autoOpen: false,
	width: 400,
	height: 300,
	modal: true,
	buttons: [
		{
			text: "Ok",
			click: function() {
				//$( this ).dialog( "close" );
				var ciclo = $("#cicloEscolar").val();
				//alert(ciclo);
				$.post("graficas/inscripcionReinsc_grafica.php",{ciclo: ciclo});
				window.open('graficas/inscripcionReinscripcion.php?ciclo='+ciclo,'Graficas','scrollbars=1,width=999,height=800');
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

/*
*	Porcentaje de Aprovechameintp
*/
$( "#dialog-porcentajeAprovechamiento" ).dialog({
	autoOpen: false,
	width: 400,
	height: 400,
	modal: true,
	buttons: [
		{
			text: "Ok",
			click: function() {
				//$( this ).dialog( "close" );
				var ciclo = $("#cicloEscolar").val();
				var plan = $("#planEstudios").val();
				
				//alert(ciclo);
				$.post("graficas/aprovechamiento_grafica.php",{ciclo: ciclo,plan: plan});
				window.open('graficas/aprovechamiento.php?ciclo='+ciclo+'&plan='+plan,'Graficas','scrollbars=1,width=999,height=800');
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

/*
*	Porcentaje de Reprobados
*/
$( "#dialog-porcentajeReprobados" ).dialog({
	autoOpen: false,
	width: 400,
	height: 400,
	modal: true,
	buttons: [
		{
			text: "Ok",
			click: function() {
				//$( this ).dialog( "close" );
				var ciclo = $("#cicloEscolarRepro").val();
				var plan = $("#planEstudiosRepro").val();
				
				//alert(ciclo);
				$.post("graficas/alumnosReprobados_grafica.php",{ciclo: ciclo,plan: plan});
				window.open('graficas/alumnosReprobados.php?ciclo='+ciclo+'&plan='+plan,'Graficas','scrollbars=1,width=999,height=800');
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});


/*
*	Porcentaje de Asistencias
*/
$( "#dialog-porcentajeAsistencia" ).dialog({
	autoOpen: false,
	width: 400,
	height: 400,
	modal: true,
	buttons: [
		{
			text: "Ok",
			click: function() {
				//$( this ).dialog( "close" );
				var ciclo = $("#cicloEscolarAsist").val();
				var plan = $("#planEstudiosAsist").val();
				
				//alert(ciclo);
				$.post("graficas/asistencias_grafica.php",{ciclo: ciclo,plan: plan});
				window.open('graficas/asistencias.php?ciclo='+ciclo+'&plan='+plan,'Graficas','scrollbars=1,width=999,height=800');
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});

})();

function grafias(){
	var elemento = document.getElementById("graficas");
	elemento.className = "negritas";
}

function graficar(id){
	//alert(id);
	switch (id){
		case 1:
			//alert("Grafica 1");
			$( "#dialog-porcentajeBajas" ).dialog( "open" );
			break;
		case 2:
			//alert("Grafica 2");
			$( "#dialog-porcentajeInscripcion" ).dialog( "open" );
			break;
		case 3:
			//alert("Grafica 3");
			$( "#dialog-porcentajeAprovechamiento" ).dialog( "open" );
			break;
		case 4:
			//alert("Grafica 4");
			$( "#dialog-porcentajeReprobados" ).dialog( "open" );
			break;
		case 5:
			//alert("Grafica 5");
			$( "#dialog-porcentajeAsistencia" ).dialog( "open" );
			break;
	}
}