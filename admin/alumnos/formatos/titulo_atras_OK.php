<?php
require('fpdf/fpdf.php');
require('fpdf/fpdfHTML.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$id_alumno=$_GET['id'];
//$id_alumno=231;
$generacion=$_GET['G'];
$anio=$_GET['A'];
$numero=$_GET['N'];
$fecha=$_GET['F'];
$titulo_num=$_GET['TN'];
//$fecha=date("d-m-Y");	
$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	//$grado = $row["grado"];
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	$curp=$row['curp'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	
	function Header()
	{		 
		 //$this->Image('titulo atras.jpg',0,0,216);
		//$this->Image('lunketec_logo.png',186,8,17);
	}
	/*
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf = new PDF_HTML('P','mm',array(216,290));//PDF_HTML('P','mm','Letter');
//$pdf=new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetFont('Arial','',10); //Times, Helvetica, Arial, Courier

// NOMBRE
$pdf->SetXY(138,12);
$pdf->Cell(50,3,"".utf8_decode($nombre_completo)."",0,0,'L');

//CURP
$pdf->SetXY(145,25);
$pdf->Cell(50,4,"".$curp."",0,0,'L');

//clave ESCUELA
$pdf->SetXY(150,49);
$pdf->Cell(50,4,"21DNL0006L",0,0,'L');

//PLAN
$pdf->SetXY(169,59);
$pdf->Cell(50,4,"".$_GET['AD6']."",0,0,'L');

//GENERACION
$pdf->SetXY(170,67);
$pdf->Cell(50,4,"".$generacion."",0,0,'L');

//AÑO EGRESO
$pdf->SetXY(174,75);
$pdf->Cell(50,4,"".$anio."",0,0,'L');

//# ACTA
$pdf->SetXY(160,92);
$pdf->Cell(50,4,$numero,0,0,'L');

//FECHA
$pdf->SetXY(160,101);
$pdf->Cell(50,4,"".$fecha."",0,0,'L');

//EXPEDIDA
$pdf->SetXY(143,108);
$pdf->Cell(50,4,"SAN JUAN B.CUAUTLANCINGO,PUEBLA",0,0,'L');

//TITULO #
$pdf->SetXY(160,118);
$pdf->Cell(50,4,"".$titulo_num."",0,0,'L');

//FIRMA
$pdf->SetY(185);
$pdf->SetX(133);
$pdf->WriteHTML('<b>'.utf8_decode("GERARDO PAUL ARVIZU SERAPIO").'</b>');
$pdf->SetXY(138,192);
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE LA ESCUELA"),0,0,'L');

//---------------------------------
/**********VALORES****************/

// ENCABEZADO TITULO REVERSO
$pdf->SetY(11);//13
$pdf->SetX(19);//25
$pdf->WriteHTML('<b>'.utf8_decode("DIRECCIÓN DE CONTROL ESCOLAR").'</b>');

//REGISTRO
$pdf->SetXY(33,19);
$pdf->Cell(33,4,"".$_GET['AD1']."",0,0,'L');

//LIBRO NUM
$pdf->SetXY(33,27);
$pdf->Cell(33,4,"".$_GET['AD2']."",0,0,'L');

//FOJA NUM
$pdf->SetXY(33,36);
$pdf->Cell(33,4,"".$_GET['AD3']."",0,0,'L');

//LUGAR
$pdf->SetXY(33,44);
$pdf->Cell(33,4,"".utf8_decode($_GET['AD4'])."",0,0,'L');

//FECHA
$pdf->SetXY(33,54);
$pdf->Cell(33,4,"".$_GET['AD5']."",0,0,'L');

//FIRMA
$pdf->SetY(75);
$pdf->SetX(21);
$pdf->WriteHTML('<b>'.utf8_decode("ALEJANDRO GÓMEZ GARCÍA").'</b>');
$pdf->SetXY(15,81);
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE CONTROL ESCOLAR"),0,0,'L');


//-------------------------------
/****PARTE CENTRAL INFERIOR*****/
//-------------------------------
$pdf->SetFont('Arial','',6);

//CLAVE CARRERA
$pdf->SetXY(65,248);
$pdf->Cell(5,5,utf8_decode("CLAVE DE LA CARRERA: "),0,0,'L');
$pdf->SetXY(96,248);
$pdf->Cell(5,5,$_GET['AD7'],0,0,'L');

//CLAVE INSTITUCION
$pdf->SetXY(65,250);
$pdf->Cell(5,5,utf8_decode("CLAVE DE LA INSTITUCIÓN: "),0,0,'L');
$pdf->SetXY(96,250);
$pdf->Cell(5,5,$_GET['AD8'],0,0,'L');

//RODAC
$pdf->SetXY(75,262);
$pdf->Cell(5,5,$_GET['AD9'],0,0,'L');

//-------------------------------
$pdf->Output("Titulo atras","I");
//-------------------------------
?>