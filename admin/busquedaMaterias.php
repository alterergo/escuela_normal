<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
if(empty($id_usuario)){ header("Location: ../index.php"); }
//echo $id_usuario;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Alumnos</title>
</head>
<body onload="alumnos();">
<div id="contenedorAdmin" class="row">

<div id="contenido" class="grid_11">

<div id="myDiv"></div>
<img src="../images/deshacer.png" class="icono" title="Deshacer" onClick="javascript:location.reload()">
<table cellpadding="15" class="table_cons">
    <thead>
        <th>#</th>	
        <th>Clave</th>
        <th>Materia</th>
        <th>Semestre</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php 
	$i = 1;
	$q=$_POST["q"];
	$estatus = $_SESSION["estatusMat"];
	$consulta = "Select * from materias WHERE estatus=$estatus AND clave LIKE '%".$q."%' OR nombre like '%".$q."%' ORDER BY id_materia DESC ";
	$respuesta = mysql_query($consulta,$conexion);
	
	$totalRows = mysql_num_rows($respuesta);
	if($totalRows>=1){
	
	while($row = mysql_fetch_array($respuesta)){
	echo "<tr>";
	echo "<td>".$i."</td>";
	echo "<td>".$row["clave"]."</td>";
	echo "<td>".utf8_encode($row["nombre"])."</td>";
	echo "<td>".$row["id_semestre"]."</td>";
	echo "<td>
	<img src='../images/editar.png' title='Editar' onClick='editarMateria(".$row['id_materia'].")' class='icono'>
	<img src='../images/eliminar.png' title='Eliminar' onclick='eliminarMateria(".$row['id_materia'].")' class='icono'>
	</td>";
	echo "</tr>";
	$i++;
	}
	}else{
		echo "Sin Resultados";
	}
?>
</tbody>
</table>

</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->


</body>
</html>