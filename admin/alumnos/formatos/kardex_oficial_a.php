<?php
require('fpdf/fpdf.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$id_alumno=$_GET['id'];
$semestre_get=$_GET['S'];
//$fecha=date("d-m-Y");	
//$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	$curp=$row['curp'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	/*
	function Header()
	{		 
		 
		 //$this->Image('kardex_espa.jpg',0,0,216);
		 //$this->Image('kardex_espa_atras.jpg',0,0,216);
		 
		 //$this->Image('kardex_mate.jpg',0,0,216);
		 //$this->Image('kardex_mate_atras.jpg',0,0,216);
		 
		 //$this->Image('kardex_ingles.jpg',0,0,216);
		 //$this->Image('kardex_ingles_atras.jpg',0,0,216); 
		 //$this->Image('kardex_hist.jpg',0,0,216);
		 //$this->Image('kardex_hist_atras.jpg',0,0,216);
		 //$this->Image('kardex_tele.jpg',0,0,216);
		 //$this->Image('kardex_tele_atras.jpg',0,0,216);
		
	}
	
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf=new PDF('P','cm','Legal');
$pdf->AddPage();
$pdf->SetFont('Arial','',10); //Times, Helvetica, Arial, Courier

//$pdf->SetXY(0.1,0.1);
//$pdf->Cell(21.4,33,"".$idEspecialidad."",1,0,'L');

#----- LISTA DE MATERIAS -----# 
		$selectMateria = "SELECT * FROM materias WHERE id_semestre = '$semestre_get' AND id_especialidad = '$idEspecialidad' AND estatus='1' ORDER BY clave ASC "; 
		$resultMat = mysql_query($selectMateria,$conexion);
		
		while($resultMat_=mysql_fetch_assoc($resultMat)){
			$id_materia=$resultMat_['id_materia'];

			$queryCalif = "SELECT AVG(calificacion) FROM calif_parc where id_alumno = '$id_alumno' and id_materia = '$id_materia' ";
				
				$resultCalif = mysql_query($queryCalif,$conexion);
				while($calif = mysql_fetch_assoc($resultCalif)){										
					$calificacion[]=substr($calif['AVG(calificacion)'],0,3);	
				}
			
			// HORAS DEL CURSO							
				$sel=mysql_query("SELECT SUM(total_horas) AS tot_horas FROM horas_clase WHERE id_materia='$id_materia' ",$conexion);
							$cont_asis='';
							while($cons=mysql_fetch_assoc($sel)):
								$total_horas_clase=$cons['tot_horas'];
									
					#--- consulta de los porcentajes de ASISTENCIA
					$queryPorcenaje = " SELECT porcentaje, SUM(asistencia) AS asisten FROM pase_lista WHERE id_alumno = '$id_alumno' AND id_materia = '$id_materia'  ";					
					$resultQuery = mysql_query($queryPorcenaje,$conexion);						
						
						while($row = mysql_fetch_array($resultQuery)){
							$asist=$row["asisten"];
							$porcentaje=$row["porcentaje"];
							
							if($asist!=0):
								$porcen_asis[]=substr((($asist*100)/$total_horas_clase),0,3);																
							else:
								$porcen_asis[]=$porcentaje;
							endif;
							
							}
							endwhile;
		}
////////////////////////////// MATEMATICAS //////////////////////////////
if($idEspecialidad==5):
	
	//// 5 SEMESTRE ////
	if($semestre_get=5):
	
	// 101
	$pdf->SetXY(8.6,4);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,4);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,4.6);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,4.6);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,5.2);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,5.2);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,5.7);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,5.7);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,6.5);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,6.5);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,7.1);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,7.1);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	// 107
	$pdf->SetXY(8.6,7.7);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,7.7);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 6 SEMESTRE
	elseif($semestre_get==6):
	//108
	$pdf->SetXY(8.6,10.3);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.3);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,10.9);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.9);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,11.5);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,11.5);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,12);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,12);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,12.6);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,12.6);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,13.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,13.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//114
	$pdf->SetXY(8.6,14);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,14);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,14.6);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,14.6);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	// 7 SEMESTRE
	elseif($semestre_get==3):
	//201
	$pdf->SetXY(8.6,17.2);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,17.2);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,17.8);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,17.8);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
		
	
	// 8 SEMESTRE
	elseif($semestre_get==8):
	//203
	$pdf->SetXY(8.6,20.5);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,20.5);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//204
	$pdf->SetXY(8.6,21.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,21.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	
	endif;
	
	
	
	////////////////////////////// TELESECUNDARIA //////////////////////////////
	elseif($idEspecialidad==7):
	
	//// 5 SEMESTRE ////
	if($semestre_get==5):
	
	// 101
	$pdf->SetXY(8.6,3.1);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,3.1);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,3.6);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,3.6);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,4.2);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,4.2);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,4.7);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,4.7);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,5.4);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,5.4);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,6.1);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,6.1);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	// 107
	$pdf->SetXY(8.6,6.7);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,6.7);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 6 SEMESTRE
	elseif($semestre_get==6):
	//108
	$pdf->SetXY(8.6,9.3);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.3);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,9.9);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.9);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,10.5);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.5);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,11.1);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,11.1);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,11.6);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,11.6);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,12.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,12.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//114
	$pdf->SetXY(8.6,13);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,13);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 7 SEMESTRE
	elseif($semestre_get==7):
	//201
	$pdf->SetXY(8.6,15.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,15.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,16.2);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.2);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	
	
	//  SEMESTRE
	elseif($semestre_get==8):
	//208
	$pdf->SetXY(8.6,18.9);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,18.9);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,19.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,19.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	endif;
	
	
	////////////////////////////// ESPAÑOL //////////////////////////////
	elseif($idEspecialidad==6):
	
	//// 5 SEMESTRE ////
	if($semestre_get==5):
	
	// 101
	$pdf->SetXY(8.1,3.6);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.3,3.6);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.1,4.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.3,4.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.1,4.6);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.3,4.6);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.1,5.1);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.3,5.1);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.1,5.9);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.3,5.9);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.1,6.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.3,6.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	// 107
	$pdf->SetXY(8.1,7.1);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.3,7.1);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 6 SEMESTRE
	elseif($semestre_get==6):
	//108
	$pdf->SetXY(8.1,9.5);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.3,9.5);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.1,10.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.3,10.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.1,10.6);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.3,10.6);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.1,11.1);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.3,11.1);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.1,11.8);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.3,11.8);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.1,12.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.3,12.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//114
	$pdf->SetXY(8.1,13);
	$pdf->Cell(50,3,"5.8".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.3,13);
	$pdf->Cell(50,3,"314".$porcen_asis[6]."",0,0,'L');
	
	
	// 7 SEMESTRE
	elseif($semestre_get==7):
	//201
	$pdf->SetXY(8.1,15.6);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.3,15.6);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.1,16.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.3,16.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	
	
	// 8 SEMESTRE
	elseif($semestre_get==8):
	//208
	$pdf->SetXY(8.1,18.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.3,18.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.1,19.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.3,19.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	endif;
	
	
	////////////////////////////// INGLES //////////////////////////////
	elseif($idEspecialidad==8):
	
	//// 5 SEMESTRE ////
	if($semestre_get==5):
	
	// 101
	$pdf->SetXY(8.6,2.8);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,2.8);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,3.3);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,3.3);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,3.9);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,3.9);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,4.5);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,4.5);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,5.2);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,5.2);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,5.9);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,5.9);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	// 107
	$pdf->SetXY(8.6,6.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,6.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 6 SEMESTRE
	elseif($semestre_get==6):
	//108
	$pdf->SetXY(8.6,9.1);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.1);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,9.7);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.7);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,10.2);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.2);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,10.8);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.8);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,11.5);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,11.5);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,12.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,12.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//114
	$pdf->SetXY(8.6,12.8);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,12.8);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 7 SEMESTRE
	elseif($semestre_get==7):
	//201
	$pdf->SetXY(8.6,15.5);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,15.5);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,16.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	
	
	//  SEMESTRE
	elseif($semestre_get==8):
	//208
	$pdf->SetXY(8.6,18.8);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,18.8);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,19.4);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,19.4);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	endif;


//////////////////////// HISTORIA ////////////////////
elseif($idEspecialidad==9):

	//// 5 SEMESTRE ////
		if($semestre_get==5):
		
		// 101
		$pdf->SetXY(8.7,1.9);
		$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
		
		$pdf->SetXY(9.9,1.9);
		$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
		
		// 102
		$pdf->SetXY(8.7,2.5);
		$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
		
		$pdf->SetXY(9.9,2.5);
		$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
		
		// 103
		$pdf->SetXY(8.7,3);
		$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
		
		$pdf->SetXY(9.9,3);
		$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
		
		// 104
		$pdf->SetXY(8.7,3.6);
		$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
		
		$pdf->SetXY(9.9,3.6);
		$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
		
		// 105
		$pdf->SetXY(8.7,4.3);
		$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
		
		$pdf->SetXY(9.9,4.3);
		$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
		
		// 106
		$pdf->SetXY(8.7,5);
		$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
		
		$pdf->SetXY(9.9,5);
		$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
		
		// 107
		$pdf->SetXY(8.7,5.6);
		$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
		
		$pdf->SetXY(9.9,5.6);
		$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
		
		
		// 6 SEMESTRE
		elseif($semestre_get==6):
		//108
		$pdf->SetXY(8.7,8.2);
		$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
		
		$pdf->SetXY(9.9,8.2);
		$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
		
		//109
		$pdf->SetXY(8.7,8.8);
		$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
		
		$pdf->SetXY(9.9,8.8);
		$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
		
		//110
		$pdf->SetXY(8.7,9.4);
		$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
		
		$pdf->SetXY(9.9,9.4);
		$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
		
		//111
		$pdf->SetXY(8.7,9.9);
		$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
		
		$pdf->SetXY(9.9,9.9);
		$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
		
		//112
		$pdf->SetXY(8.7,10.6);
		$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
		
		$pdf->SetXY(9.9,10.6);
		$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
		
		//113
		$pdf->SetXY(8.7,11.4);
		$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
		
		$pdf->SetXY(9.9,11.4);
		$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
		
		//114
		$pdf->SetXY(8.7,12);
		$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
		
		$pdf->SetXY(9.9,12);
		$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
		
		
		// 7 SEMESTRE
		elseif($semestre_get==7):
		//201
		$pdf->SetXY(8.7,14.6);
		$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
		
		$pdf->SetXY(9.9,14.6);
		$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
		
		//202
		$pdf->SetXY(8.7,15.2);
		$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
		
		$pdf->SetXY(9.9,15.2);
		$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
		
		
		
		//  SEMESTRE
		elseif($semestre_get==8):
		//208
		$pdf->SetXY(8.7,17.9);
		$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
		
		$pdf->SetXY(9.9,17.9);
		$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
		
		// 106
		$pdf->SetXY(8.7,18.5);
		$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
		
		$pdf->SetXY(9.9,18.5);
		$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
		
		endif;

endif;

		//}//while materias

$pdf->Output("Kardex Oficial","I");

?>