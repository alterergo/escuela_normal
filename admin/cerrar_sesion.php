<?php
	session_start();
	session_unset();
	session_destroy();
	session_regenerate_id(true);
?>
<script type="text/javascript" language="javascript">
	location.href="../index.php";
</script>