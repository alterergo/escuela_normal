<?php
include '../../includes/conexion.php';
session_start();

$idAlumno = $_POST["idAlumno"];

?>
<script language="javascript">
function mostrar(tip){ //alert(tip);
	var boleta=document.getElementById('bole');
	var simple=document.getElementById('constancia_simple');
	var constancia=document.getElementById('constancia');
	var kardex_o=document.getElementById('kardex_of');

	if ( tip=='kardex' || tip=='historial' || tip=='titulo' || tip=='certificado' ){
		boleta.style.display='none';
		simple.style.display='none';
		constancia.style.display='none';
		kardex_o.style.display='none';
	}
	else if(tip=='boleta'){
		boleta.style.display='';
		simple.style.display='none';
		constancia.style.display='none';
		kardex_o.style.display='none';
	}

	else if (tip == "constancia_simple"){
		boleta.style.display='none';
		simple.style.display='';
		constancia.style.display='none';
		kardex_o.style.display='none';
	}


	else if(tip=='kardex_oficial'){
		boleta.style.display='none';
		constancia.style.display='none';
		simple.style.display='none';
		kardex_o.style.display='';
	}

	else {
		boleta.style.display='none';
		constancia.style.display='';
		simple.style.display='none';
		kardex_o.style.display='none';
	}

}

</script>
<form>
<fieldset>
<legend>Seleccione Formato</legend>
<div id="mensajes" class="mensajes"></div>

<input type="radio" name="formato" value="boleta" id="boleta"  onclick="mostrar(this.value);"> Boleta <br/>
<div id="bole" style="display:none">
Semestre: <select name="grado" id="grado">
<option value="1">1°</option>
<option value="2">2°</option>
<option value="3">3°</option>
<option value="4">4°</option>
<option value="5">5°</option>
<option value="6">6°</option>
<option value="7">7°</option>
<option value="8">8°</option>
<option value="9">9°</option>
<option value="10">10°</option>
<option value="11">11°</option>
<option value="12">12°</option>
</select><br />
Expedido a:<input type="text" name="expe" id="expe" size="50" title="Ej: 20 de Enero de 2013" placeholder="Ej: 20 de Enero de 2013" />
<br>
Ciclo Escolar:<input type="text" name="cicl" id="cicl" size="50" placeholder="2015-2016">
</div>
<input type="radio" name="formato" value="kardex" id="kardex" onclick="mostrar(this.value);"> Kardex <br/>

<input type="radio" name="formato" value="constancia_simple"  onclick="mostrar(this.value);"> Constancia Simple <br/>
<div id="constancia_simple" style="display:none">Periodo del: <input type="text" name="periodo" id="periodo" size="50" title="Ej: 20 de Agosto del 2012 al 05 de Julio del 2013" autofocus placeholder="Ej: 20 de Agosto del 2012 al 05 de Julio del 2013" /><br />
Expedido a:<input type="text" name="expedido" id="expedido" size="50" title="Ej: los once días del mes de Enero del dos mil trece" placeholder='Ej: los once días del mes de Enero del dos mil trece' /></div>

<input type="radio" name="formato" value="constancia"  onclick="mostrar(this.value);"> Constancia de Estudios <br />
<div id="constancia" style="display:none">Expedido a:<input type="text" name="expedido2" id="expedido2" size="50" title="Ej: los once días del mes de Enero del dos mil trece" placeholder="Ej: los once días del mes de Enero del dos mil trece" autofocus/></div>

<input type="radio" name="formato" value="titulo" onclick="mostrar(this.value);"> Titulo<br />

<input type="radio" name="formato" value="kardex_oficial" onclick="mostrar(this.value);"> Kardex Oficial
<div id="kardex_of" style="display:none">Semestre a Imprimir
<select name="sem_kardex" id="sem_kardex">
<option value="1">1°</option>
<option value="2">2°</option>
<option value="3">3°</option>
<option value="4">4°</option>
<option value="5">5°</option>
<option value="6">6°</option>
<option value="7">7°</option>
<option value="8">8°</option>
<option value="9">9°</option>
<option value="10">10°</option>
</select>
</div><br />

<input type="radio" name="formato" value="certificado" onclick="mostrar(this.value);"> Certificado <br />

<input type="radio" name="formato" value="historial"  onclick="mostrar(this.value);"> Historial
<input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $idAlumno; ?>" />
</fieldset>
</form>
