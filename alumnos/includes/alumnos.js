(function(){

	$("#accesos").click(function(){
		$("#dialog-accesos").dialog("open");
	});
	
	$( "#dialog-accesos" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );
					
					if($("#pass").val()==""){
						$("#pass").focus();
						$("#errPass").html("Campo Contrase&ntilde;a").fadeIn(900).delay(2000).fadeOut(900);
						return;
					}
					
					if($("#nvoPass").val()==""){
						$("#nvoPass").focus();
						$("#errPass").html("Nueva Contrase&ntildea Requerida").fadeIn(900).delay(2000).fadeOut(900);
						return;
					}
					
					
					$.post("editarPass.php",
						$("#editarAccesos").serialize(),
						function(data){
						//alert(data);
							
							if(data == 1){
								//alert("Especialidad Modificada");
									$("#result").html("Contrase&ntilde;a Modificada");
									//document.location.reload();
								setTimeout("document.location.reload()", 2000);
								}else{
									("#result").html("Error al modificar").fadeIn(900).delay(2000).fadeOut(900);
								}
						});
					
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

	
})();