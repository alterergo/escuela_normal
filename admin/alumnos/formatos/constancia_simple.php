<?
include "../../../includes/conexion.php";

$idAlumno = $_GET["idAlumno"];
$periodo=$_GET['periodo'];
$expedido=$_GET['expedido'];

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.sexo,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t6.ciclo_escolar
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6
				WHERE
					t1.id_alumno = '$idAlumno'
				AND
					t6.en_curso = '1'
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				";
$result = mysql_query($selectAlumnos,$conexion);

while($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$sexo=$row['sexo'];
	$id_semestre = $row["id_semestre"];
	$semestre = $row["semestre"];
	$grado = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = utf8_encode($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

switch($grado){
	case 1:
		$grado = "PRIMER";
		break;
	case 2:
		$grado = "SEGUNDO";
		break;
	case 3: 
		$grado = "TERCER";
		break;
	case 4: 
		$grado = "CUARTO";
		break;
	case 5: 
		$grado = "QUINTO";
		break;	
}

$ingr=mysql_query("SELECT c1.plan_estudios, c2.anios FROM ingreso AS c1, plan_estudios AS c2 WHERE c1.id_alumno='$idAlumno' AND c1.plan_estudios=c2.id_plan",$conexion);
while($dat=mysql_fetch_assoc($ingr)):
	$plan_estudios=$dat['plan_estudios'];
	$anios=$dat['anios'];
endwhile;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Constancia de Estudios</title>

<style type="text/css">
body{	
	font-size:20px;
	text-align:justify;

}
.mayu{
	text-transform:uppercase;
}

</style>
</head>

<body>
<table cellpadding="0" cellspacing="0" border="0" width="816" height="1056">
<tr align="center" valign="top"  >
    <td height="17%" width="15%" style="padding-top:15px"><img src="../../../images/sep.jpg" width="160" /></td>
    <td width="70%" class="mayu" style="font-size:12px; padding-top:15px" >subsecretarÍa de educaciÓn superior <br /> direcciÓn general de formaciÓn y desarrollo de docentes <br />direcciÓn de formaciÓn de docentes<br /><u>esculea normal superior federalizada del estado de puebla<br />clave: 21dnl0006l</u> </td>
    <td width="15%" style="padding-top:15px" align="left"><img src="../../../images/logo.png" width="100" /></td>
</tr>

<tr >
	<td height="7%" colspan="3" align="right" class="mayu" valign="bottom" style="padding-right:20px; padding-top:30px"><strong>asunto: constancia de estudios.</strong></td>
</tr>

<tr >
	<td height="7%" colspan="3" class="mayu" style="font-style:italic; padding-left:70px; padding-bottom:35px"><strong>a quien corresponda:<br />presente</strong></td>
</tr>  

<tr>
	<td  height="10%" colspan="3" style="padding-left:70px; padding-right:20px; padding-top:25px; padding-bottom:25px">El que suscribe, Director de la escuela Normal Superior Federalizada del Estado de Puebla, <span style="font-style:italic"><strong>HACE CONSTAR</strong></span> que existen documentos en el archivo Escolar de esta Institución que comprueban que <? if ($sexo=='H'){ ?>el estudiante: <? } else{ ?> la estudiante: <? }?> </td>
</tr>

<tr >
	<td height="15%" colspan="3" style="font-size:24px; padding-top:20px; padding-bottom:20px " align="center"><strong><u>C. <? echo $nombre_completo?></u></strong></td>
</tr>  

<tr>
	<td colspan="3" height="44%" valign="top" style="padding-left:70px; padding-right:20px; padding-top:20px">Se encuentra <? if ($sexo=='H'){ ?>inscrito <? } else{ ?> inscrita <? }?> al <strong><? echo $grado?> SEMESTRE</strong>, de la Licenciatura en Educaci&oacute;n Secundaria con Especialidad en <u><strong style="text-transform:uppercase"><? echo $nombreEspecialidad?></strong></u> Plan de <? echo $anios ?> años, del periodo comprendido del <? echo $periodo ?>, del ciclo escolar <? echo $ciclo_escolar ?>. <br /><br />
 	A petici&oacute;n <? if ($sexo=='H'){ ?>del interesado <? } else{ ?> de la interesada <? }?>y para los fines legales que convengan se extiende la presente en el Municipio de San Juan B. Cuautlancicngo, Puebla, a <? echo $expedido?>. <br /><br /><br /><br /><br />
    <strong>ATENTAMENTE</strong> <br />
    "Ejercer la libertad para trascender en el tiempo"<br /><br /><br /><br />
    <strong>MTRO. GERARDO PAUL ARVIZU SERAPIO <BR />
    DIRECTOR</strong><br /><br /><br />
    <span style="font-size:14px"> c.c.p: Archivo<br /><br />GPAS/ebo* 007</span><br /><br />
    
    <u>
    <? for($i=1;$i<=145;$i++):
    echo "&nbsp;";
	endfor; ?></u><br />
    <span style="font-size:10px">Calle Azteca Norte, Col. Reserva Territorial Quetzalc&oacute;atl, C.P. 72150 Tel. 01 222 283 6505, E-mail: ensfep130@gmail.com</span></td>
</tr>      


</table>

</body>
</html>