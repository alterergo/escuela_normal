 function validar(e,id)
 {
   var clave=id;
   var alumno="alumno_"+clave;
   //alert(alumno);
   tecla = (document.all) ? e.keyCode : e.which;
   patron = /[12345678/]/;
   te = String.fromCharCode(tecla);
   return patron.test(te);
   }



 $(document).ready(function(){/* == INICIA FUNCION ==*/

$( "#tabs" ).tabs();

/********** tooltip ************/
 $( ".icono" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});

$( ".tooltip" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});
//$.spro.jpopit("No Icon on the left of me.", {noIcon: true});

/***************************************************/
/************** FORMULARIO DEL ALUMNO **************/
/***************************************************/
//OCULTA EL CALPO DEL SEMESTRE DE ENTRADA
//$('#validar_alumno').fadeOut('slow');


/*********************************************************/
/********************* VALIDAR EMAIL *********************/
/*********************************************************/
function validar_email(valor)
    {
        // creamos nuestra regla con expresiones regulares.
        var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        // utilizamos test para comprobar si el parametro valor cumple la regla
        if(filter.test(valor))
            return true;
        else
            return false;
    }


$( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 800,
            width: 950,
            modal: true,
            buttons: {
                "Crear": function() {
                    $(".ui-dialog-buttonset").css('display','none');

					if($("#nombre").val() == ""){
						//alert("El campo Nombre es Requerido");
						//$("#errNom").html("");
						$(".ui-dialog-buttonset").css('display','');
						alert("El campo Nombre es Requerido");
						$("#nombre").focus();
						return;
					}


					if($("#apellido_paterno").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El Apellido Paterno es Requerido");
						//$("#errApp").html("El Apellido Paterno es Requerido");
						$("#apellido_paterno").focus();
							return;
					}


					if($("#apellido_materno").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El Apellido Materno es Requerido");
						//$("#errApm").html("El Apellido Materno es Requerido");
						$("#apellido_materno").focus();
							return;
					}

					if($("#edad").val() == "" || isNaN($("#edad").val())){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Edad es Requerida");
						//$("#errEdd").html("La Edad es Requerida");
						$("#edad").focus();
							return;
					}

					if($("#sexo").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El campo Sexo es Requerido");
						//$("#errSexo").html("El sexo es Requerido");
						$("#sexo").focus();
							return;
					}

					if($("#calle").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El campo Calle es Requerido");
						//$("#errCalle").html("El campo Calle es Requerido");
						$("#calle").focus();
							return;
					}

					if($("#numero_interior").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El Numero Interior es Requerido");
						//$("#errNumInt").html("El Numero Interior es Requerido");
						$("#numero_interior").focus();
						return;
					}

					if($("#colonia").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Colonia es Requerida");
						//$("#errCol").html("La Colonia es Requerida");
						$("#colonia").focus();
							return;
					}

					if($("#codigo_postal").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El Codigo Postal es Requerido");
						//$("#errC_P").html("El Codigo Postal es Requerido");
						$("#codigo_postal").focus();
							return;
					}else if(isNaN($("#codigo_postal").val())) {
						$(".ui-dialog-buttonset").css('display','');
						alert("Codigo postal incorrecto");
						//$("#errC_P").html("Debe introducir un número");
						$("#codigo_postal").focus();
							return;
					}

					if($("#correo").val() == ''){
						$(".ui-dialog-buttonset").css('display','');
						alert("Ingrese un correo");
						//$("#errEmail").html("Ingrese un correo");
							return;
					}else if(validar_email($("#correo").val()))
					{
						$("#errEmail").html("");
					}else
					{
						$(".ui-dialog-buttonset").css('display','');
						alert("El correo no es valido");
						//$("#errEmail").html("El correo no es valido");
							return;
					}

					if($("#lugar_nacimiento").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("El Lugar de Nacimiento es Requerido");
						//$("#errLugNac").html("El Lugar de Nacimiento es Requerido");
						$("#lugar_nacimiento").focus();
							return;
					} else { $("#errLugNac").html(""); }

					if($("#dia_nacimiento").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Fecha de Nacimiento es Requerida");
						//$("#errFecNac").html("La Fecha de Nacimiento es Requerida");
						$("#dia_nacimiento").focus();
							return;
					}

					if($("#mes_nacimiento").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Fecha de Nacimiento es Requerida");
						//$("#errFecNac").html("La Fecha de Nacimiento es Requerida");
						$("#mes_nacimiento").focus();
							return;
					}

					if($("#anio_nacimiento").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Fecha de Nacimiento es Requerida");
						//$("#errFecNac").html("La Fecha de Nacimiento es Requerida");
						$("#anio_nacimiento").focus();
							return;
					}

					if($("#matricula").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Matricula es Requerida");
						//$("#errMat").html("La matricula es Requerida");
						$("#matricula").focus();
							return;
					}

					if($("#id_ciclo").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Matricula es Requerida");
						//$("#errCiclo").html("El ciclo escolar es Requerido");
						$("#id_ciclo").focus();
							return;
					}

					if($("#edo_civil").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Matricula es Requerida");
						//$("#errCivil").html("Seleccione estado civil");
						$("#edo_civil").focus();
							return;
					}else { $("#errCivil").html(""); }

					if($("#idEspecialidad").val() == ""){
						$(".ui-dialog-buttonset").css('display','');
						alert("La Matricula es Requerida");
						//$("#errEspe").html("La especialidad es Requerida");
						$("#idEspecialidad").focus();
						return;
					}else { $("#errEspe").html(""); }



					//$("#aviso").html("Registrando...");
					//grayOut(true,{'opacity':'25', 'bgcolor':'#999999' });

					// VALIDAR SI EXISTE EL USUARIO
					$.ajax({
						url: 'alumnos/validarAlmExiste.php',
						type: 'POST',
						async: true,
						data: 'matricula='+$("#matricula").val(),
						dataType: 'html',
						success: function(data){
							//alert(data);
							if(data == 'activo'){
								alert("La matricula ya existe");
								//$("#matricula").focus();
								//$("#errMat").html("La matricula ya Existe");
								$(".ui-dialog-buttonset").css('display','');
								return;
							}else if(data == 'temporal'){
								alert("La matricula tiene Baja Temporal. Revise la lista de Bajas");
								$("#matricula").focus();
								//$("#errMat").html("La matricula tiene Baja Temporal. Revise la lista de Bajas");
								$(".ui-dialog-buttonset").css('display','');
								return;
							}else if(data == 'definitiva'){
							alert("La matricula tiene Baja Definitiva. Revise la lista de Bajas");
								$("#matricula").focus();
								//$("#errMat").html("La matricula tiene Baja Definitiva. Revise la lista de Bajas");
								$(".ui-dialog-buttonset").css('display','');
								return;
							}else{

								// SI TODOS LOS CAMPOS SON CORRECTOS CREA AL ALUMNO
								$.post("alumnos/crearAlumno.php",
								$("#agregarAlumno").serialize(),
								function(data){
									//alert(data);
									if(data == 1 ){
										//$( "#dialog-semestre" ).dialog( "open" );
										alert("Alumno Resgistrado");
										//$("#aviso").html("Alumno Registrado");
										//document.location.reload();
										}else {
										alert("Ocurrio un error al registrar al alumno");
										$(".ui-dialog-buttonset").css('display','');
										}

									//$("#agregandoAlumno").html(data);
								});
							}
						},
						error: function(data){
							alert("Error ");
							return;
						}
					});
					//fin validacion

					/*
					$.post("alumnos/crearAlumno.php",
					$("#agregarAlumno").serialize(),
					function(data){

						if(data == 1 ){
							alert("Alumno Resgistrado");
							$( "#dialog-semestre" ).dialog( "open" );
							//document.location.reload();

							}else{
							alert("Ocurrio un error al registrar al lumno");
							}

						//$("#agregandoAlumno").html(data);
					});
					*/
					//alert($("#fecha_nacimiento").val());

                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });

        $( "#nuevoAlumno" ).click(function() {
                $( "#dialog-form" ).dialog( "open" );
            });


/***********************************************************************************/
/************************ FUNCION SEMESTRES DE ESPECIALIDAD ***********************/
/***********************************************************************************/
/*
("#idEspecialidad").change(function(){
        var op = $("#idEspecialidad option:selected").val();
        //$('#s_e').html(op);
	$("#s_e").load("sem_espe.php",{id:op});

});

*/
/***********************************************************************************/
/************************ FORMULARIO DEL SEMESTRE DEL ALUNMO ***********************/
/***********************************************************************************/
$( "#dialog-semestre" ).dialog({
            autoOpen: false,
            height: 300,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );
					$.post("alumnos/actualizarSemestre.php",$("#semestreIngreso").serialize(),function(data){ /*alert(data);*/ document.location.reload(); });

                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });

/***********************************************************************************/
/************************ FORMULARIO DE INFORMACION DEL ALUNMO *********************/
/***********************************************************************************/
$( "#dialog-info" ).dialog({
            autoOpen: false,
            height: 700,
            width: 900,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );


					if($("#nomb_edt").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#err_nomb_edt").html("El campo Nombre es Requerido");
						$("#nomb_edt").focus();
						return;

					}else { $("#err_nomb_edt").html(""); }


					if($("#app_edt").val() == ""){
						//alert("El Apellido Paterno es Requerido");
						$("#err_app_edt").html("El Apellido Paterno es Requerido");
						$("#app_edt").focus();
						return;
					}else { $("#err_app_edt").html(""); }


					if($("#apm_edt").val() == ""){
						//alert("El Apellido Materno es Requerido");
						$("#err_apm_edt").html("El Apellido Materno es Requerido");
						$("#apm_edt").focus();
						return;
					}else { $("#err_apm_edt").html(""); }

					if($("#edad_edt").val() == "" || isNaN($("#edad").val())){
						//alert("La Edad es Requerida");
						$("#err_edad_edt").html("La Edad es Requerida");
						$("#edad_edt").focus();
						return;
					}else { $("#err_edad_edt").html(""); }

					if($("#sexo_edt").val() == ""){
						//alert("El campo Sexo es Requerido");
						$("#err_sexo_edt").html("El sexo es Requerido");
						$("#sexo_edt").focus();
						return;
					} else { $("#err_sexo_edt").html(""); }

					if($("#calle_edt").val() == ""){
						//alert("El campo Calle es Requerido");
						$("#err_calle_edt").html("El campo Calle es Requerido");
						$("#calle_edt").focus();
						return;
					} else { $("#err_calle_edt").html(""); }


					if($("#num_int_edt").val() == ""){
						//alert("El Numero Interior es Requerido");
						$("#err_num_int_edt").html("El Numero Interior es Requerido");
						$("#num_int_edt").focus();
						return;
					}else if(isNaN($("#num_int_edt").val())) {
						$("#err_num_int_edt").html("Debe introducir un número");
						$("#num_int_edt").focus();
						return;
					}else { $("#err_num_int_edt").html(""); }


					if($("#colonia_edt").val() == ""){
						//alert("La Colonia es Requerida");
						$("#err_colonia_edt").html("La Colonia es Requerida");
						$("#colonia_edt").focus();
						return;
					}else { $("#err_colonia_edt").html(""); }


					if($("#cod_post_edt").val() == ""){
						//alert("El Codigo Postal es Requerido");
						$("#err_cod_post_edt").html("El Codigo Postal es Requerido");
						$("#cod_post_edt").focus();
						return;
					}else if(isNaN($("#cod_post_edt").val())) {
						$("#err_cod_post_edt").html("Debe introducir un número");
						$("#cod_post_edt").focus();
						return;
					}else { $("#err_cod_post_edt").html(""); }



					if($("#email").val() == ''){
						$("#err_email").html("Ingrese un Correo");
						return;
					}else if(validar_email($("#email").val()))
					{
						$("#err_email").html("");
					}else
					{
						$("#err_email").html("El correo no es valido");
						return;
					}

					if($("#lugar_nacimiento_edt").val() == ""){
						//alert("El Lugar de Nacimiento es Requerido");
						$("#err_lugar_nacimiento_edt").html("El Lugar de Nacimiento es Requerido");
						$("#lugar_nacimiento_edt").focus();
						return;
					} else { $("#err_lugar_nacimiento_edt").html(""); }


					if($("#fecha_nacimiento_edt").val() == ""){
						//alert("La Fecha de Nacimiento es Requerida");
						$("#err_fecha_nacimiento_edt").html("La Fecha de Nacimiento es Requerida");
						$("#fecha_nacimiento_edt").focus();
						return;
					}else { $("#err_fecha_nacimiento_edt").html(""); }


					if($("#matricula_id").val() == ""){
						//alert("La Matricula es Requerida");
						$("#err_matricula_id").html("La matricula es Requerida");
						$("#matricula_id").focus();
						return;
					}else { $("#err_matricula_id").html(""); }

					$.post("alumnos/editarAlumno.php",$("#updAlumno").serialize(),function(data){
						if(data == 1){
						alert("Información Guardada");

                                                                                    $( "#dialog-info" ).dialog( "close" );

						//document.location.reload();
						}else{ alert("Error al actualizar"); }
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });


/*************************************************************************************/
/*********************** FORMULARIO DE REINSCRIPCION *********************************/
/************************************************************************************/
$( "#dialog-reinscripcion" ).dialog({
            autoOpen: false,
            height: 500,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {

					$.post("alumnos/reiscripcion.php",
					$("#semestreInscripcion").serialize(),
					function(data){
						//alert(data);
						if(data==1){
						$("#msg_reins").html("Alumno Reinscrito").fadeIn(1000).delay(2000).fadeOut(1000, function(){
                                                    $( "#dialog-reinscripcion" ).dialog( "close" );
                                                });
						//setTimeout("document.location.reload()", 2000);
						}else{
						$("#msg_reins").html("Error al Reinscribir").fadeIn(1000).delay(1000).fadeOut(1000);
						//document.location.reload();
						}
					});
					//$( this ).dialog( "close" );

                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });


/*************************************************************************/
/************************ CALENDARIO *************************************/
/************************************************************************/
/*
$( "#fecha_nacimiento" ).datepicker({changeMonth: true, changeYear: true });
$( "#fecha_nacimiento" ).datepicker( "option", "dateFormat", "yy-mm-dd" );

var monthNamesShort = $( "#fecha_nacimiento" ).datepicker( "option", "monthNamesShort" );
$( "#fecha_nacimiento" ).datepicker( "option", "monthNamesShort", [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ] );

var dayNames = $( "#fecha_nacimiento" ).datepicker( "option", "dayNames" );
$( "#fecha_nacimiento" ).datepicker( "option", "dayNames", [ "Domingo", "Lunes", "Martes", "Miercoles", "Jeuves", "Viernes", "Sabado" ] );

var dayNamesMin = $( "#fecha_nacimiento" ).datepicker( "option", "dayNamesMin" );
$( "#fecha_nacimiento" ).datepicker( "option", "dayNamesMin", [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ] );
*/

/***********************************************************/
/********* MOSTRAR Y OCULTAR EN CASO DE TRASLADO************/
/***********************************************************/
$("#traslado").click(function(){ $("#validar_alumno").fadeIn('slow'); });

$("#nuevo").click(function(){ $("#validar_alumno").fadeOut('slow'); });


/*********************************************************************************************************************************/
/*********************************************** FORMULARIO DE ESPECIALIDADES *****************************************************/
/*********************************************************************************************************************************/

// ABRIR CATALOGO DE ESPECIALIDADES
$("#nuevoAlumno").click(function(){
	$( "#dialog-especialidades" ).dialog( "open" );

});

// CATALOGO DE ESPECIALIDADES
$( "#dialog-especialidades" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );

					/*if($("#plan_h").val()==""){
						$("#errEsp").html("<div class='error'>Selecciones un Plan de Estudios</div>").slideDown(900).delay(1300).slideUp(1500);
						//setTimeout("location.href='./'", 2200);
						return;
					}*/

					var especialidad = $("#nombreEspeciaidad").val();
					var plan = $("#plan_h").val();
					var grupoEspecialidad = parseInt($("#grupoEspecialidad:checked").val());


					if(especialidad==""){
						$("#nombreEspeciaidad").focus();
						$("#errEsp").html("<div class='error'>EL campo nombre es requerido</div>").fadeIn(900).delay(1000).fadeOut(900);
						//setTimeout("location.href='./'", 2200);
						return;
					}

					$.ajax({
						url: 'especialidades/validarEspExiste.php',
						type: 'POST',
						async: true,
						data: 'especialidad='+especialidad+'plan='+plan,
						dataType: 'html',
						success: function(data){
						//alert(data);
							if(data == 'Activo'){
								//alert("La Especialidad ya Existe");
								$("#nombreEspeciaidad").focus();
								$("#errEsp").html("La Especialidad ya Existe").fadeIn(900).delay(1000).fadeOut(900);
								return;
							}else if(data == 'Inactivo'){
								//alert("La Especialidad se encuentra en la Lista de Bajas");
								$("#nombreEspeciaidad").focus();
								$("#errEsp").html("La Especialidad se encuentra en la Lista de Bajas");
								return;
							}else if(data == 0){

							$.post("especialidades/crearEspecialidad.php",
								{nomEspecialidad: especialidad,plan_estudios:plan,grupoEspecialidad: grupoEspecialidad},
								function(data){
									if(data == 1){
									//alert("Especialidad Agregada");
									$("#msg_espe").html("Especialidad Registrada");
									setTimeout("document.location.reload()",2000);
									}else{
									alert("Error al registrar");
									}
							});



							}
						},
						error: function(data){
							alert("Error General");
						}
					});

					/*
					$.post("especialidades/crearEspecialidad.php",
						{nomEspecialidad: especialidad},
						function(data){
							if(data == 1){
								alert("Especialidad Agregada");
								document.location.reload();
								}else{
								alert("Error al registrar");
								}
						});
					*/


                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/*
==========================================================================================
NOTA: 	ACCIONES DE EDITAR Y ELIMINAR ESPECIALIDAD SE ENCUENTRAN EN FUNCIONES JAVASCRIPT
		EN LA PARTE DE ABAJO.
==========================================================================================
*/


/**********************************************************************************************/
/****************************** EDICION DE ESPECIALIDAD ***************************************/
/**********************************************************************************************/
$( "#dialog-editarEsp" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );


					var idEsp = $("#idEsp").val();
					var grupoEspecialidadEdt = parseInt($("#grupoEspecialidadEdt:checked").val());

					if($("#nomEditar").val()==""){
						/*$("#nomEditar").focus();
						$("#errNomEdt").html("El nuevo Nombre es requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;*/
						var nomEditar = $("#nombreOrg").val();
					}else{
						var nomEditar = $("#nomEditar").val();
					}

					$.post("especialidades/editarEspecialidad.php",
						{idEsp: idEsp ,nomEditar: nomEditar,grupoEspecialidadEdt: grupoEspecialidadEdt},
						function(data){
						//alert(data);
							if(data == 1){
								//alert("Especialidad Modificada");
								//document.location.reload();
								$("#mensajes").html("Especialidad Modificada");
								setTimeout("document.location.reload()", 2000);
								}else{
								alert("Error al modificar");
								}
						});


                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/********************************************************************************/
/****************************** AGREGAR MATERIAS ********************************/
/********************************************************************************/
// FORMULARIO DE MATERIAS
$( "#dialog-materias" ).dialog({
            autoOpen: false,
            height: 450,
            width: 400,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    //$( this ).dialog( "close" );

					if($("#claveMateria").val()==""){
						$("#claveMateria").focus();
						$("#errCveMat").html("El campo Clave es requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}

					if($("#nombreMateria").val()==""){
						$("#errNomMat").focus();
						$("#errNomMat").html("El campo nombre es requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}

					var idMat = $("#claveMateria").val();
					var nom = $("#nombreMateria").val();
					var id_espe=$("#idEspecialidad").val();
					$.ajax({
						url: 'materias/validarMaterias.php',
						type: 'POST',
						async: true,
						data: {'idMat':idMat,'nom':nom, 'id_espe':id_espe},
						dataType: 'html',
						success: function(data){
						//alert(data);
							if(data == 1){
								$("#errCveMat").html("Ya existe una materia con esa clave").fadeIn(900).delay(1000).fadeOut(900);
								$("#claveMateria").val("");
								//document.location.reload();
							}else{

								$.post("materias/crearMateria.php",
								$("#materias").serialize(),
								function(data){
								//alert(data);
									if(data == 1){
										$("#msg_materia").html("Materia Registrada").fadeIn(1000).delay(1000).fadeOut(1000);
										setTimeout("document.location.reload()", 2000);
										}else{
										$("#msg_materia").html("Error al registrar").fadeIn(1000).delay(1000).fadeOut(1000);
										}
								});


							}
						},
						error: function(data){
							alert("Error General");
						}
					});

                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

//ABRIR FORMULARIO DE MATERIAS
$("#nuevaMateria").click(function(){
	$( "#dialog-materias" ).dialog( "open" );

});


/***************************************/
/******** links de regreso *************/
/***************************************/
//FLECHA DE REGRESO A ESPECIALIDADES
$("#regresarEsp").click(function(){
	document.location.href="especialidades_admin.php";
	});

//FLECHA DE REGRESO A SEMESTRES
$("#regresarSem").click(function(){
	var id = $("#id_especialidad").val()
	document.location.href="semestre_admin.php?idEsp="+id;
	});

/*********************************************************************************************************************************/
/*********************************************** FORMULARIO DE SEMESTRES *********************************************************/
/*********************************************************************************************************************************/

// ABRIR SEMESTRES
$("#nuevoSemestre").click(function(){
	$( "#dialog-semestres" ).dialog( "open" );

});

// FORMULARIO SEMESTRE
$( "#dialog-semestres" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {

					if($("#nombreSemestre").val()==""){
						$("#nombreSemestre").focus();
						$("#errNombreSemestre").html("Seleccione el semestre").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}


					var nombreSem = $("#nombreSemestre").val();
					var id_espe=$("#id_espe_hidd").val();
					$.ajax({
						url: 'semestres/validarSemestre.php',
						type: 'POST',
						async: true,
						data: {'nombre':nombreSem,'id_espe':id_espe},
						dataType: 'html',
						success: function(data){
						//alert(data);
							if(data == 1){
								$("#errNombreSemestre").html("El semestre Ya Existe").fadeIn(900).delay(1000).fadeOut(1000);
								$("#nombreSemestre").val("");
								$("#nombreSemestre").focus();
								//document.location.reload();
							}else{
								//alert("Error al hacer la consulta");
								$.post("semestres/crearSemestres.php",
								$("#semestre").serialize(),
								function(data){
								//alert(data);
									if(data == 1){
										$("#msg_semestre").html("Semestre Registrado").fadeIn(1000).delay(1000).fadeOut(1000);
										//alert("Semestre Agregado");
										setTimeout("document.location.reload()", 2000);

										}else{
										$("#msg_semestre").html("Error al registrar").fadeIn(1000).delay(1000).fadeOut(1000);
										}
								});
							}
						},
						error: function(data){
							alert("Error General");
						}
					});

					/*

					*/
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


//EDITAR SEMESTRE
$( "#dialog-editarSemestres" ).dialog({
            autoOpen: false,
            height: 400,
            width: 400,
            modal: true,
            buttons: {
                "Aceptar": function() {

					if($("#semestreEitar").val()==""){
						$("#semestreEitar").focus();
						$("#errSemEdt").html("El campo de Nombre es Requerido ").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					if($("#gradoEitar").val()==""){
						$("#gradoEitar").focus();
						$("#errGdoEdt").html("El campo de Grado es Requerido ").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}

					$.post("semestres/editarSemestres.php",
						$("#editarSemestre").serialize(),
						function(data){
						//alert(data);
							if(data == 1){
								//alert("Semestre Modificado");
								$("#mensajes").html("Semestre Modificado");
								setTimeout("document.location.reload()",2000);

								}else{
								alert("Error al modificar el semestre");
								}
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/**********************************************************************************/
/******************************** MATERIAS ****************************************/
/**********************************************************************************/
$( "#dialog-editarMateria" ).dialog({
            autoOpen: false,
            height: 500,
            width: 400,
            modal: true,
            buttons: {
                "Aceptar": function() {
					var clave = $("#materiaClaveEdt").val();
					var nombreMateria = $("#materiaNomEdt").val();
					var idMateria = $("#idMateria").val();
					//alert(a+' '+b);

					if($("#materiaClaveEdt").val()==""){
						$("#materiaClaveEdt").focus();
						$("#errCve").html("El campo Clave es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					if($("#materiaNomEdt").val()==""){
						$("#materiaNomEdt").focus();
						$("#errNom").html("El campo Nombre es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}


					$.post("materias/editarMateria.php",
						{ nuevaClave: clave, nuevoNombre: nombreMateria, idMateria: idMateria },
						function(data){
						//alert(data);
							if(data == 1){
								//alert("Materia Modificada");
								$("#mensajes").html("Materia Modificada");
								setTimeout("document.location.reload()",2000);
								}else{
								alert("Error al modificar la Materia");
								}
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/************************************************************************************************/
/************************************* CICLOS ESCOLARES *****************************************/
/************************************************************************************************/

// ABRIR CICLO ESCOLAR
$("#nuevoCiclo").click(function(){
	$( "#dialog-cicloEscolar" ).dialog( "open" );

});

// FORMULARIO CICLO ESCOLAR
$( "#dialog-cicloEscolar" ).dialog({
            autoOpen: false,
            height: 500,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {

					if($("#nombreCiclo").val()==""){
						$("#nombreCiclo").focus();
						$("#errCiclo").html("El campo Nombre es Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					var nombreCiclo = $("#nombreCiclo").val();
					$.ajax({
						url: 'ciclos_escolares/validarCiclo.php',
						type: 'POST',
						async: true,
						data: 'nombre='+nombreCiclo,
						dataType: 'html',
						success: function(data){
						//alert(data);
							if(data == 1){
								$("#errCiclo").html("El ciclo escolar ya Existe").fadeIn(900).delay(1000).fadeOut(900);
								$("#nombreCiclo").val("");
								//document.location.reload();
							}else{
								$.post("ciclos_escolares/crearCiclo.php",
								{ nombreCiclo: nombreCiclo },
								function(data){
								//alert(data);
									if(data == 1){
										//alert("ciclo agregado");
										$("#resultCiclo").html("ciclo agregado").fadeIn(900).delay(1000).fadeOut(900);
										document.location.reload();
										}else{
										alert("Error al registrar el Ciclo");
										}
								});

							}
						},
						error: function(data){
							alert("Error General");
						}
					});

                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

// FORMULARIO EDICION CICLO ESCOLAR
$( "#dialog-editarCiclo" ).dialog({
            autoOpen: false,
            height: 500,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {

					if($("#nuevoNombreCiclo").val() == ""){
						$("#nuevoNombreCiclo").focus();
						$("#errCicloEdt").html("El campo Nombre es Requerido").fadeIn(800).delay(1000).fadeOut(800);
						return;
					}

					$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data == 1){
								//alert("ciclo modificado");
								$("#mensajes").html("ciclo modificado");
								setTimeout("document.location.reload()",2000);
								}else{
								alert("Error al modificar el Ciclo");
								}
						});
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/*********************************************************************************************/
/*********************** FORMULARIO SEMESTRE ACTUAL DEL ALUMNO *******************************/
/*********************************************************************************************/
$( "#dialog-semActual" ).dialog({
            autoOpen: false,
            height: 570,
            width: 680,
            modal: true,
            buttons: {
                "Aceptar": function() {
					$( this ).dialog( "close" );
					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


/****************************************************/
/******************* BUSCADOR ***********************/
/****************************************************/
/*
$('input#search').domsearch('table#tablaAlumnos');
$('input#searchEsp').domsearch('table#tablaEspecialidades');
$('input#searchSem').domsearch('table#tablaSemestres');
$('input#searchMat').domsearch('table#tablaMaterias');
$('input#searchCiclo').domsearch('table#tablaCiclos');
*/
//Placeholder en todos los navegadores
$('input[placeholder]').placeholder();

/*********************************************************************************************/
/***************************** FORMULARIO DE BAJA DEL ALUMNO *********************************/
/*********************************************************************************************/
$( "#dialog-bajaAlumno" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
					//$( this ).dialog( "close" );
					var idAlumno = $("#idAlumno").val();
					var tipoBaja = $("#tipoBaja").val();

					if(tipoBaja == "temporal"){
					var respTemp = confirm("¿Desea dar de Baja Temporal?");
					if(respTemp){
						$.post("alumnos/bajaTemporal.php",
							{idAlumno: idAlumno },
							function(data){
								if(data=1){
								//alert("Alumno dado de baja Temporal");
								//document.location.reload();
								$("#mensaje").html("Alumno dado de baja Temporal");
								setTimeout("location.href='./'", 2300);
								}else{ alert("Error al eliminar al alumno"); }
							});
						}else{	location.reload(); }
					}else if(tipoBaja == "definitiva"){
						//alert("Definitiva");
						var respuesta = confirm("Desea eliminar al Alumno de forma Definitiva?");
						if(respuesta){
							$.post("alumnos/eliminarAlumno.php",
							{idAlumno: idAlumno },
							function(data){
								if(data==1){
								alert("Alumno dado de baja Definitiva");
								document.location.reload();
								}else{ alert("Error al eliminar al alumno"); }
							});

						}
						else{
							$( this ).dialog( "close" );
						}
					}else{ $( this ).dialog( "close" ); }

					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/*
#-------------------------------------------------------------------------------------------#
#------------------------- Restauraciones de archivos eliminados ---------------------------#
#-------------------------------------------------------------------------------------------#
*/
/*********************************************************************************************/
/*********************** FORMULARIO RESTAURAR REGISTROS ELIMINADOS ***************************/
/*********************************************************************************************/
$("#registros").click( function(){
	$("#registrosElimindaos").load("alumnos/alumnosBaja.php");
	$( "#dialog-registrosEliminados" ).dialog( "open" );

});

$( "#dialog-registrosEliminados" ).dialog({
            autoOpen: false,
            height: 500,
            width: 800,
            modal: true,
            buttons: {
                "Aceptar": function() {
					$( this ).dialog( "close" );
					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/**********************************************/
/********* FORMULARIO DE ESPECIALIDADES *******/
/**********************************************/
$("#registrosEsp").click( function(){
	$("#frmRestaurar").load("especialidades/frmRestaurarEsp.php");
	$( "#dialog-restaurar" ).dialog( "open" );

});

$( "#dialog-restaurar" ).dialog({
            autoOpen: false,
            height: 500,
            width: 700,
            modal: true,
            buttons: {
                "Aceptar": function() {
					$( this ).dialog( "close" );
					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/**********************************************/
/********* FORMULARIO DE SEMESTRES *******/
/**********************************************/
$("#registrosSem").click( function(){
	$("#frmRestaurarSem").load("semestres/frmRestaurarSem.php");
	$( "#dialog-restaurarSem" ).dialog( "open" );

});

$( "#dialog-restaurarSem" ).dialog({
            autoOpen: false,
            height: 500,
            width: 700,
            modal: true,
            buttons: {
                "Aceptar": function() {

					$( this ).dialog( "close" );

                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


/**********************************************/
/********* FORMULARIO DE MATERIAS *******/
/**********************************************/
$("#registrosMat").click( function(){
	$("#frmRestaurarMaterias").load("materias/frmRestaurarMat.php");
	$( "#dialog-restaurarMat").dialog( "open" );

});

$( "#dialog-restaurarMat" ).dialog({
            autoOpen: false,
            height: 500,
            width: 700,
            modal: true,
            buttons: {
                "Aceptar": function() {
					//document.location.reload();
					$( this ).dialog( "close" );
					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});

/**********************************************/
/********* FORMULARIO DE CICLOS ESCOLARES *******/
/**********************************************/
$("#registrosCiclo").click( function(){
	$("#frmRestaurarCiclos").load("ciclos_escolares/frmRestaurarCiclos.php");
	$( "#dialog-restaurarCiclo").dialog( "open" );

});

$( "#dialog-restaurarCiclo" ).dialog({
            autoOpen: false,
            height: 500,
            width: 700,
            modal: true,
            buttons: {
                "Aceptar": function() {
					//document.location.reload();
					$( this ).dialog( "close" );
					/*$.post("ciclos_escolares/editarCiclo.php",
						$("#formEditarCiclo").serialize(),
						function(data){
						//alert(data);
							if(data = 1){
								alert("ciclo modificado");
								document.location.reload();
								}else{
								alert("Error al modificar el Ciclo");
								}
						});*/
                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


/***********************************************************************************************/
/************************* FORMULARIO DE CAMBIO DE ESPECIALIDAD ********************************/
/***********************************************************************************************/
$( "#dialog-cambioEspecialidad" ).dialog({
            autoOpen: false,
            height: 500,
            width: 900,
            modal: true,
            buttons: {
                "Aceptar": function() {
					//document.location.reload();
					//$( this ).dialog( "close" );
					var id = $("#idAlumno").val();
					var esp = $("#especialidad").val();
					//alert(id+" "+esp);

					if($("#especialidad").val()==""){
						$("#mensajes").html("Campo Requerido")
									  .fadeIn(900)
									  .delay(1000)
									  .fadeOut(900);
						return;
					}

					var confirmar = confirm("Desea cambia de Especialidad?");
					if(confirmar){
						$.post("alumnos/cambiarEspecialidad.php",
						{idAlumno: id, especialidad: esp},
						function(data){
						//alert(data);
							if(data = 1){
								$("#mensajes").empty();
								$("#mensajes").html("Especialidad Actualizada");
                                                                $( "#dialog-cambioEspecialidad" ).dialog( "close" );
								//setTimeout("document.location.reload()", 2200);
								}else{
									alert("Error al actualizar la Especialidad");
									document.location.reloa();
								}
						});
					}else{
						return;
					}

                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


/********************************************************************************************/
/************************** 		F O R M A T O S		 ************************************/
/********************************************************************************************/
$( "#dialog-formatos" ).dialog({
            autoOpen: false,
            height: 450,
            width: 550,
            modal: true,
            buttons: {
                "Generar": function() {
					//document.location.reload();
					//$(this).dialog("close");

					var formato = $("input:radio[name=formato]:checked").val();
					var idAlumno = $("#idAlumno").val();
					var periodo = $("#periodo").val();
					var expedido = $("#expedido").val();
					var expedido2 = $("#expedido2").val();
					var expe = $("#expe").val();
					var grado=$("#grado").val();
					var sem_kardex=$("#sem_kardex").val();
          var cicl=$("#cicl").val();


					if(formato == ""){
						$("#mensajes").html("Campo Requerido").fadeIn(900).delay(1000).fadeOut(900);
						return;
					}
					//alert(formato);
					if(formato=="boleta"){
						if(expe==''){
							$("#mensajes").html("Ingrese una fecha de expedición").fadeIn(1000).delay(1000).fadeOut(1000);
						} else{
            //alert(cicl);
						window.open('alumnos/formatos/boleta.php?idAlumno='+idAlumno+'&cicl='+cicl+'&expe='+expe+'&grado='+grado,'Boleta','width=840,height=1000,scrollbars=1');
						}

					} else if(formato=="kardex"){
						//$.post("alumnos/formatos/kardex.php",{idAlumno: idAlumno});
						window.open('alumnos/formatos/kardex.php?idAlumno='+idAlumno,'Kardex','width=840,height=1000,scrollbars=1');

					} else if(formato=="constancia_simple"){
						//$.post("alumnos/formatos/kardex.php",{idAlumno: idAlumno});
							if (periodo=="" || expedido==""){
								$("#mensajes").html("Ingrese los campos").fadeIn(1000).delay(1000).fadeOut(1000);
							} else {
							window.open('alumnos/formatos/constancia_simple.php?idAlumno='+idAlumno+'&periodo='+periodo+'&expedido='+expedido,'Constancia Simple','width=840,height=1000,scrollbars=1');
							}

					} else if(formato=="constancia"){
						//$.post("alumnos/formatos/kardex.php",{idAlumno: idAlumno});
							if (expedido2==""){
								$("#mensajes").html("Ingrese fecha de expedición").fadeIn(1000).delay(1000).fadeOut(1000);
							} else {
							window.open('alumnos/formatos/constancia.php?idAlumno='+idAlumno+'&expedido='+expedido2,'Constancia Simple','width=840,height=1000,scrollbars=1');
							}

					} else if(formato=="historial"){
						//$.post("alumnos/formatos/kardex.php",{idAlumno: idAlumno});
						document.location='alumnos/historial.php?id='+idAlumno;

					} else if(formato=='titulo'){
						document.location='alumnos/titulo.php?id='+idAlumno;

					} else if(formato=='kardex_oficial'){
						if(sem_kardex==1 || sem_kardex==2 || sem_kardex==3 || sem_kardex==4){
						window.open('alumnos/formatos/kardex_oficial.php?id='+idAlumno+'&S='+sem_kardex,'Constancia Simple','width=840,height=1000,scrollbars=1');
						}else{
						window.open('alumnos/formatos/kardex_oficial_a.php?id='+idAlumno+'&S='+sem_kardex,'Constancia Simple','width=840,height=1000,scrollbars=1');
							}
					} else if(formato=='certificado'){
						document.location='alumnos/certificado.php?id='+idAlumno;

					}


                },
                Cancelar: function() {
					//document.location.reload();
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});


});/*======================== FIN DE LA FUNCION DOCUMENT READY ===============================*/


/********************************************************/
/******************* ESTILO A MENU **********************/
/*******************************************************/
function alumnos(){
	var elemento = document.getElementById("alumno");
	elemento.className = "negritas";
}

function materias(){
	var elemento = document.getElementById("materias");
	elemento.className = "negritas";
}

function semestres(){
	var elemento = document.getElementById("semestres");
	elemento.className = "negritas";
}

function especialidades(){
	var elemento = document.getElementById("especialidades");
	elemento.className = "negritas";
}

function ciclo_escolar(){
	var elemento = document.getElementById("ciclo_escolar");
	elemento.className = "negritas";
}

function docentes(){
	var elemento = document.getElementById("docentes");
	elemento.className = "negritas";

}

/*******************************************************************************/
/************************** ACCIONES TABLA ALUMNOS *****************************/
/*******************************************************************************/

function inscripcion_reinscripcion(id){
	//alert(id);
	$.post("alumnos/reinscripcionAlumno.php",
	{idAlumno: id},
	function(data){
	/*alert(data);*/
	$("#inscripcionAlumno").html(data);
	$( "#dialog-reinscripcion" ).dialog( "open" ); });
}


/*********************************************************/
/******************	FUNCIONES DE FORMATOS ****************/
/*********************************************************/
function documentos(id){
	//alert(id);
	$("#contFormatos").load("alumnos/frmFormatos.php",{idAlumno: id});
	$("#dialog-formatos").dialog("open");

}

function informacion(id){
	//alert(id);
	$.post("alumnos/informacionAlumno.php",
	{idAlumno: id},
	function(data){
	/*alert(data);*/
	$("#infoAlumno").html(data);
	$( "#dialog-info" ).dialog( "open" ); });

}

function cambio_especialidad(id){
	//alert(id);
	$("#frmCambioEspecialidad").load(
		"alumnos/frmCambioEspecialidad.php",
		{idAlumno: id},
		function(data){
			$( "#dialog-cambioEspecialidad" ).dialog( "open" );
		});
}

function semestre(id) {
	//alert(id);
	$.post("alumnos/semestreActual.php",
	{idAlumno: id},
		function(data){
			//alert("Semestre Actual: "+data);
			$( "#dialog-semActual" ).dialog( "open" );
			$("#aux").html(data);
		}
	);

}

function eliminar_alumno(id){
	//alert(id);
	$("#bajaAlumno").load("alumnos/frmEliminarAlumno.php",{idAlumno: id},function(data){ $( "#dialog-bajaAlumno" ).dialog( "open" ); });

	/*$.post("alumnos/frmEliminarAlumno.php",
		{idAlumno: id },
		function(data){
			//alert(data);
			$( "#dialog-bajaAlumno" ).dialog( "open" );
		});*/

	/*
	var respuesta = confirm("Desea eliminar al Alumno?")
	if (respuesta){
		$.post("alumnos/eliminarAlumno.php",
		{idAlumno: id },
		function(data){ alert(data); });
		window.location = "index.php";
	}
	else{
		document.locatiion.reload();
	}*/
}


/***********************************************************/
/*************** ACCIONES TABLA Especialidades *************/
/***********************************************************/

function editarEspecialidades(id){
	$("#editarEsp").load("especialidades/formEdicion.php",
	{idEsp: id},
	function(data){ /*alert(data);*/ $( "#dialog-editarEsp" ).dialog( "open" );
	});
}

function eliminarEspecialidades(id){
	//alert(id);
	var respuesta = confirm("Desea eliminar esta Especialidad");
	if(respuesta){
		//alert("Se eliminara");
		$.post("especialidades/eliminarEspecialidad.php",
		{idEsp: id},
		function(data){
			/*alert("Especilidad Eliminada"); document.location.reload();*/
			$("#myDiv").html("<div class='grid_12 mensajes'>Especialidad Eliminda</div>");
			setTimeout("document.location.reload()", 2200);
		});
	}else{
		document.location.reload();
	}
}

function semestres(id,plan){
		//alert(id);
		//$.post("materias_admin.php",{idEsp: id});
		location.href="semestre_admin.php?idEsp="+id;
}

/************************************************************/
/***************** ACCIONES TABLA MATERIAS *****************/
/************************************************************/

function editarMateria(id){
	//alert(id);
	$("#formEditarMateria").load("materias/formEditarMateria.php",{idMateria: id},function(data){ $( "#dialog-editarMateria" ).dialog( "open" ); });
}

function eliminarMateria(id){
	//alert(id);
	var respuesta = confirm("Desea eliminar esta materia?");
	if(respuesta){

		$.post("materias/eliminarMaterias.php",
		{idMatEliminar: id},
		function(data){
			//alert(data);
			if(data == 1){
				//alert("Materia Eliminada");
				//document.location.reload();
				$("#myDiv").html("<div class='grid_12 mensajes'>Materia Eliminada</div>");
				setTimeout("document.location.reload()",2000);
				}
		});

		}else{ document.location.reload(); }
}

/************************************************************/
/***************** ACCIONES TABLA SEMESTRES *****************/
/************************************************************/

function materias(id_sem,id_esp){ document.location.href="materias_admin.php?idSem="+id_sem+"&idEsp="+id_esp}

function editarSemestre(id){
	//alert(id);
	$("#formEdtSemestre").load("semestres/formEditarSemestre.php",
		{idSemestre: id},
		function(data){
			$( "#dialog-editarSemestres" ).dialog( "open" ); }
		);
}

function eliminarSemestre(id){
	//alert(id);
	var respuesta = confirm("Desea eliminar este semestre?");
	if(respuesta){
		$.post("semestres/eliminarSemestre.php",
		{idSemestre: id},
		function(data){
			//alert(data);
			if(data==1){
				//alert("Semestre eliminado");
				$("#myDiv").html("<div class='grid_12 mensajes'>Semestre Eliminado</div>");
				setTimeout("document.location.reload()",2000);

			}else{
				alert("Error al eliminar el semestre");
			}

		});
	}else{
		document.location.reload();
	}

}

/************************************************************/
/**************** ACCIONES TABLA CICLO_ESCOLAR **************/
/************************************************************/

function editarCiclo(id){
	//alert(id);
	$("#cargarFormCiclo").load(
		"ciclos_escolares/formEditarCiclo.php",
		{ idCiclo: id },
		function(data){ /*alert(data);*/  $( "#dialog-editarCiclo" ).dialog( "open" );
	});
}

function eliminarCiclo(id){
	//alert(id);
	var respuesta = confirm("Desea eliminar este ciclo?");
	if(respuesta){
		$.post("ciclos_escolares/eliminarCiclo.php",
		{idCiclo: id},
		function(data){
			//alert(data);
			if(data == 1){
				//alert("Ciclo Eliminado");
				$("#myDiv").html("<div class='grid_12 mensajes'>Ciclo Eliminado</div>");
				setTimeout("document.location.reload()",2000);
				}else{
				alert("Error al eliminar el ciclo");
				}
			});

	}else{
		document.location.reload();
	}
}

/*
===================== RESTAURACION ============================
*/
/*******************************************************/
/******* FUNCION PARA RESTAURAR ESPECIALIDAD ***********/
/*******************************************************/
function restaurarEsp(id){
	//alert(id);
	var acept = confirm("Desea restaurar esta Especialidad?");
	if(acept){
		$.ajax({
		  url: 'especialidades/restaurarEsp.php',
		  type: 'POST',
		  async: true,
		  data: 'idEsp='+id,
		  dataType: 'html',
		  success: function(data){
			if(data == 1){
				//alert("Especialidad Restaurada");
				$("#myDiv").html("<div class='mensajes grid_12'>Especialidad Restaurada</div>");
				setTimeout("document.location.href='especialidades_admin.php'", 2000);
			}else{
				alert("Error al restaurar la Especialidad");
			}
		  },
		  error: function(data){
			alert("Error. Intentelo màs tarde");
		  }
		});
	}else{
		return;
		document.location.href="especialidades_admin.php";
	}

}

/*******************************************************/
/******* FUNCION PARA RESTAURAR SEMESTRES ************/
/*******************************************************/
function restaurarSem(id){
	//alert(id);
	var acept = confirm("Desea restaurar este Semestre?");
	if(acept){
	$.ajax({
	  url: 'semestres/restaurarSem.php',
	  type: 'POST',
	  async: true,
	  data: 'idSem='+id,
	  dataType: 'html',
	  success: function(data){
	  //alert(data);
		if(data == 1){
			//alert("Semestre Restaurado");
			$("#myDiv").html("<div class='mensajes grid_12'>Semestre Restaurado</div>");
			setTimeout("document.location.href='semestre_admin.php'",2000);
		}else{
			alert("Error al restaurar el Semestre");
		}
	  },
	  error: function(data){
		alert("Error restaurar el Semestre");
	  }
	});
	}else{
		document.location.reload();

	}
}

/*******************************************************/
/******* FUNCION PARA RESTAURAR MATERIAS ************/
/*******************************************************/
function restaurarMat(id){
	//alert(id);
	var acept = confirm("Desea Restaurar esta Materia?");
	if(acept){
	$.ajax({
	  url: 'materias/restaurarMat.php',
	  type: 'POST',
	  async: true,
	  data: 'idMat='+id,
	  dataType: 'html',
	  success: function(data){
	  //alert(data);
		if(data == 1){
			//alert("Materia Restaurada");
			//document.location.reload();
			$("#myDiv").html("<div class='mensajes grid_12'>Materia Restaurada</div>");
			setTimeout("document.location.reload()",2000);
		}else{
			alert("Error al restaurar la Materia");
		}
	  },
	  error: function(data){
		alert("Error restaurar la Materia");
	  }
	});
	}else{
		docuemt.location.href="materias_admin.php";
	}

}

/*******************************************************/
/******* FUNCION PARA RESTAURAR CICLOS ESCOLARES ************/
/*******************************************************/
function restaurarCiclo(id){
	var acept = confirm("Desea Restaurar este Ciclo?");
	if(acept){
	//alert(id);
	$.ajax({
	  url: 'ciclos_escolares/restaurarCiclos.php',
	  type: 'POST',
	  async: true,
	  data: 'idCiclo='+id,
	  dataType: 'html',
	  success: function(data){
	  //alert(data);
		if(data == 1){
			//alert("Ciclo Escolar Restaurado");
			$("#myDiv").html("<div class='mensajes grid_12'>Ciclo Escolar Restaurado</div>");
			setTimeout("document.location.reload()",2000);
		}else{
			alert("Error al restaurar el Ciclo Escolar");
		}
	  },
	  error: function(data){
		alert("Error restaurar el ciclo Escolar");
	  }
	});
	}else{
		//document.location.reload();
		return;
	}

}

/**************************************************************************** modificaciones 14/01/12 **************************************************************************/

/**********************************************************/
/***************** 	BAJA TENMPORAL DEL ALUMNO ************/
/**********************************************************/
function bajaTemporal(id){

	var respuesta = confirm("Desea Restaurar al alumno?");
	if(respuesta){
	//alert(id);
	$.post("alumnos/restaurarAlumno.php",
	{idAlumno: id},
	function(data){
		if(data==1){
			//alert("Alumno Restaurado");
			//location.reload();
			$("#myDiv").html("<div class='grid_12 mensajes'>Alumno Restaurado </div>");
			setTimeout("location.href='./'", 2200);
			//document.location.href='index.php';
		}else{
			alert("Ocurrio un error. Intentelo nuevamente.");
		}
	});
	}else{
		location.reload();
	}
}

/****************************************************************************************************************************/
/**************************************************************** BUSQUEDA **************************************************/
/****************************************************************************************************************************/
//BUSCADOR DE ALUMNOS
function buscarAlumno(){
	var xmlhttp;
	var n=document.getElementById('search').value;


	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaAlumnos").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />';
			document.getElementById("tablaAlumnos").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }

	xmlhttp.open("POST","busquedaAlumno.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);

}

//BUSCADOR ESPECIALIDAD
function buscarEspecialidad(){
	var xmlhttp;
	var n=document.getElementById('searchEsp').value;
	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaEspecialidades").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />';
			document.getElementById("tablaEspecialidades").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	xmlhttp.open("POST","busquedaEspecialidad.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
}

//BUSCADOR SEMESTRE
function buscarSemestre(){
	var xmlhttp;
	var n=document.getElementById('searchSem').value;
	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaSemestres").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />';
			document.getElementById("tablaSemestres").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	xmlhttp.open("POST","busquedaSemestre.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
}

//BUSCADOR MATERIA
function buscarMateria(){
	var xmlhttp;
	var n=document.getElementById('searchMat').value;
	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaMaterias").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />';
			document.getElementById("tablaMaterias").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	xmlhttp.open("POST","busquedaMaterias.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
}

//BUSCADOR Ciclo
function buscarCiclo(){
	var xmlhttp;
	var n=document.getElementById('searchCiclo').value;
	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaCiclos").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />';
			document.getElementById("tablaCiclos").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	xmlhttp.open("POST","busquedaCiclos.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
}

/****************************************************************/
/****************** CAMBIO DE SEMESTRE **************************/
/****************************************************************/

function cambiarCicloCurso(val,name){
	//alert(val+" "+name);
	var acept = confirm("Desea Cambiar este Ciclo por defecto?");
	if(acept){
		var idCiclo = $("#idCiclo").val();
		var valor = val;
		var nombre = name;
		//alert(idCiclo+" "+valor+" "+nombre);

		$.ajax({
			url: 'ciclos_escolares/cambiarCicloCurso.php',
			type: 'POST',
			async: true,
			data: {idCiclo : idCiclo, valor: valor, nombre: nombre},
			dataType: 'html',
			success: function(data){
				//alert(data);
				if(data == 1){
						$("#mensajes").html("Se actualizó el Ciclo Escolar.").fadeIn(900).delay(1000).fadeOut(900);
						//document.location.reload();
				}else{
						$("#mensajes").html("Ocurrio un error. Intente Nuevamente").fadeIn(900).delay(1000).fadeOut(900);
				}
			},
			error: function(data){
				alert("Error General");
			}
		});

	}else{
		document.location.reload();
	}
}

/******************************************************************************************************/
/***************************************** ACTIVAR PARCIAL  *******************************************/
/******************************************************************************************************/

function activarParcial(id){
	//alert(id);
	//$("#frmParcial").load("ciclos_escolares/frmActivarParcial.php",{idParcial: id});
	var confirmar = confirm("Desea Cambiar el Periodo Parcial");
	if(confirmar){
		$.ajax({
			url: 'ciclos_escolares/cambiarParcial.php',
			type: 'POST',
			async: true,
			data: 'idParcial='+id,
			dataType: 'html',
			success: function(data){
			//alert(data);
				if(data == 1){
					//alert("Consulta hecha");
					$("#myDiv").html("<div class='mensajes'>Parcial Actualizado</div>").fadeIn(900).delay(1000).fadeOut(900);
					$("#tablaParciales").load("ciclo_escolar_admin.php #tablaParciales");
					//document.location.reload();
				}else{
					alert("Error al hacer la consulta");
				}
			},
			error: function(data){
				alert("Error General");
			}
		});
	}else{
		return;
	}

}

/**************************************************************************************************/
/***********************************	AGREGAR HISTORIAL	***************************************/
/**************************************************************************************************/
function agregarFoto(id){


}



////////////////////DESHABILITA PANTALLA PARA CARGA////////////////////
function grayOut(vis, options) {
	var options = options || {};
	var zindex = options.zindex || 50;
	var opacity = options.opacity || 70;
	var opaque = (opacity / 100);
	var bgcolor = options.bgcolor || '#000000';
	var dark=document.getElementById('darkenScreenObject');  if (!dark) {
	var tbody = document.getElementsByTagName("body")[0];
	var tnode = document.createElement('div');           // Create the layer.
	tnode.style.position='absolute';                 // Position absolutely
	tnode.style.top='0px';                           // In the top
	tnode.style.left='0px';                          // Left corner of the page
	tnode.style.overflow='hidden';                   // Try to avoid making scroll bars
	tnode.style.display='none';                      // Start out Hidden
	tnode.id='darkenScreenObject';                   // Name it so we can find it later
	tbody.appendChild(tnode);                            // Add it to the web page
	dark=document.getElementById('darkenScreenObject');  // Get the object.
	}
	if (vis) {
		if( document.body && ( document.body.scrollWidth || document.body.scrollHeight ) ) {
			var pageWidth = document.body.scrollWidth+'px';
			var pageHeight = document.body.scrollHeight+'px';
		} else if( document.body.offsetWidth ) {
			var pageWidth = document.body.offsetWidth+'px';
			var pageHeight = document.body.offsetHeight+'px';
		} else {
			var pageWidth='100%';
			var pageHeight='100%';
		}
		dark.style.opacity=opaque;
		dark.style.MozOpacity=opaque;
		dark.style.filter='alpha(opacity='+opacity+')';
		dark.style.zIndex=zindex;
		dark.style.backgroundColor=bgcolor;
		dark.style.width= pageWidth;
		dark.style.height= pageHeight;
		dark.style.display='block';
	} else {
		dark.style.display='none';
	}
}
///////////////////////////////////////////////////////////////////////
