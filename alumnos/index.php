<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Escuela Normal</title>
<link rel="stylesheet" href="../css/style.css"></link>
</head>
<body>

<div id="contenedorLogin" class="row margenSuperior">
	<div id="respuestaAjax"></div>
    <div id="logo">        
        <img src="../images/escuela.png"/>
    </div>
    <div id="iniciarSesion">
        <h2>Alumnos</h2>
        
        <label for="user"><div>Usuario</div></label>
        <input type="text" id="user" name="user" placeholder="Usuario" autofocus class="textf"/>
        <br/>
        <label for="pass"><div>Contraseña</div></label>
        <input type="password" id="pass" name="pass" placeholder="Contraseña" class="textf"/>
        <br/>
        <input type="submit" id="enviar" name="enviar" value="Iniciar"/>
    </div>        
    
</div>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="includes/validacion.js"></script>
</script>
</body>
</html>