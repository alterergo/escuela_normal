<?
include "../../../includes/conexion.php";

$idAlumno = $_GET["idAlumno"];
$expedido=$_GET['expedido'];

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.sexo,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t6.ciclo_escolar
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6
				WHERE
					t1.id_alumno = '$idAlumno'
				AND
					t6.en_curso = '1'
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t5.id_alumno = t1.id_alumno
				
				AND
					t4.idEspecialidad = t5.id_especialidad	
				";
$result = mysql_query($selectAlumnos,$conexion);

while($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$sexo=$row['sexo'];
	$id_semestre = $row["id_semestre"];
	$semestre = $row["semestre"];
	$grado = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = utf8_encode($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

switch($grado){
	case 1:
		$grado = "PRIMER";
		break;
	case 2:
		$grado = "SEGUNDO";
		break;
	case 3: 
		$grado = "TERCER";
		break;
	case 4: 
		$grado = "CUARTO";
		break;
	case 5: 
		$grado = "QUINTO";
		break;	
}

$ingr=mysql_query("SELECT c1.plan_estudios, c2.anios FROM ingreso AS c1, plan_estudios AS c2 WHERE c1.id_alumno='$idAlumno' AND c1.plan_estudios=c2.id_plan",$conexion);
while($dat=mysql_fetch_assoc($ingr)):
	$plan_estudios=$dat['plan_estudios'];
	$anios=$dat['anios'];
endwhile;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Constancia de Estudios</title>

<style type="text/css">
body{	
	font-size:12px;
	text-align:justify;

}
.mayu{
	text-transform:uppercase;
}
.materias{
	font-size:10px;
}

</style>
</head>

<body>
<table cellpadding="0" cellspacing="0" border="0" width="816" height="1056">
<tr align="center" valign="top"  >
    <td height="10%" width="15%" style="padding-top:15px"><img src="../../../images/sep.jpg" width="130" /></td>
    <td width="70%" class="mayu" style="font-size:10px; padding-top:15px" >subsecretarÍa de educaciÓn superior <br /> direcciÓn general de formaciÓn y desarrollo de docentes <br />direcciÓn de formaciÓn de docentes<br /><u>escuela normal superior federalizada del estado de puebla<br />clave: 21dnl0006l</u> </td>
    <td width="15%" style="padding-top:15px" align="left"><img src="../../../images/logo.png" width="80" /></td>
</tr>

<tr >
	<td height="5%" colspan="3" align="right" class="mayu" valign="bottom" style=""><strong>asunto: constancia de estudios.</strong></td>
</tr>

<tr >
	<td height="3%" colspan="3" class="mayu" style="font-style:italic; padding-left:70px; "><strong>a quien corresponda:<br />presente</strong></td>
</tr>  

<tr>
	<td  height="4%" colspan="3" style="padding-left:70px; padding-right:20px; ">El que suscribe, Director de la escuela Normal Superior Federalizada del Estado de Puebla con clave 21DNL0006L, <span style="font-style:italic"><strong>HACE CONSTAR</strong></span> que existen documentos en el archivo Escolar de esta Institución que comprueban que <? if ($sexo=='H'){ ?>el estudiante: <? } else{ ?> la estudiante: <? }?> </td>
</tr>

<tr >
	<td height="4%" colspan="3" style="font-size:16px; " align="center"><strong><u>C. <? echo $nombre_completo?></u></strong></td>
</tr>  

<tr>
	<td colspan="3" height="5%" valign="top" style="padding-left:70px; padding-right:20px; ">Se encontró <? if ($sexo=='H'){ ?>inscrito <? } else{ ?> inscrita <? }?> en la Licenciatura en Educaci&oacute;n Secundaria con Especialidad en <u><strong style="text-transform:uppercase"><? echo $nombreEspecialidad?></strong></u> Plan de <? echo $anios ?> años; obteniendo las siguientes calificaciones correspondientes al: 
    </td>
</tr>

<tr>
	<td colspan="3" height="64%" valign="top">
    <?
	$materias=mysql_query("SELECT * FROM calif_parc AS t1, materias AS t2, ciclo_escolar AS t3 WHERE t1.id_alumno='$idAlumno'
	AND t1.id_materia = t2.id_materia
	AND t1.ciclo_escolar = t3.id_ciclo ORDER BY t2.clave ASC",$conexion);
	if($mat=mysql_fetch_assoc($materias)):	
	$ciclo=$mat['ciclo_escolar'];	
	endif;
	?>
    <div width="100%" align="center" ><u>CICLO <? echo $ciclo?></u></div>
	<table cellpadding="0" cellspacing="0" border="1" width="50%" align="left" class="materias" style="border:thin solid" >
	<? 	
	$id_semes_ant="1";
	$id_ciclo_ant="";
	$materias=mysql_query("SELECT AVG(t1.calificacion), t1.id_materia, t2.clave, t2.nombre, t2.id_materia, t3.ciclo_escolar, t3.id_ciclo, t2.id_semestre 
	FROM calif_parc AS t1, materias AS t2, ciclo_escolar AS t3 WHERE 	
	t1.id_alumno='$idAlumno'
	AND t1.id_materia = t2.id_materia
	AND t1.ciclo_escolar = t3.id_ciclo 
	AND t2.estatus=1
	GROUP BY t2.clave
	ORDER BY t2.clave ASC ",$conexion); //
	while($mat=mysql_fetch_assoc($materias)):
	$calificacion=substr($mat['AVG(t1.calificacion)'],0,3);
	$nombre_materia=utf8_encode($mat['nombre']);
	//$id_materia=$mat['id_materia']; 
	$ciclo=$mat['ciclo_escolar'];
	$id_ciclo=$mat['id_ciclo']; 
	$id_semes=$mat['id_semestre']; //echo $id_semes;
	
	
		
	?>
    
        <!-- semestre igual -->        
    	
        <? 
		if( $id_ciclo==$id_ciclo_ant || $id_ciclo_ant==""):				
			if ($id_semes==$id_semes_ant ):  ?>	
				<tr>
				<td width="92%"  style="padding-left:6px; padding-right:4px;"><? echo $nombre_materia?></td>
				<td width="8%" align="center"><? echo $calificacion?></td>                        
				</tr>                                    						
			<? 
			$id_semes_ant=$id_semes; 			
			$id_ciclo_ant=$id_ciclo; 
			
			else: //cambio de semestre ?>
			</table>
			<table cellpadding="0" cellspacing="0" border="1" width="50%" align="right" class="materias" style="border:thin solid">				
				<tr>
				<td width="92%"  style="padding-left:6px; padding-right:4px;"><? echo $nombre_materia?></td>
				<td width="8%" align="center"><? echo $calificacion?></td>                        
				</tr>                                   
			
			<? $id_ciclo_ant=$id_ciclo; 
			    $id_semes_ant=$id_semes; 
				
			endif; 
		
		else:
			///ciclo nuevo
			 ?>	             	
                </table>&nbsp;
                <div width="100%" align="center" style="height:2px" >&nbsp;</div>                
                <div width="100%" align="center"  ><u>CICLO <? echo $ciclo?></u></div>
                <table cellpadding="0" cellspacing="0" border="1" width="50%" align="left" class="materias" style="border:thin solid">
				<tr>
				<td width="92%"  style="padding-left:6px; padding-right:4px;"><? echo $nombre_materia?></td>
				<td width="8%" align="center"><? echo $calificacion?></td>                        
				</tr>     
              <? 
			  	$id_semes_ant=$id_semes;
				$id_ciclo_ant=$id_ciclo;
		endif;
			
	
 endwhile; ?>
				
                </table>
                
    </td>
</tr>    

<tr>
	<td colspan="3" height="5%">    
 	A petici&oacute;n <? if ($sexo=='H'){ ?>del interesado <? } else{ ?> de la interesada <? }?>y para los fines legales que convengan se extiende la presente en el Municipio de San Juan B. Cuautlancicngo, Puebla, a <? echo $expedido?>. <br /><br />
    <div align="center">
    <strong>ATENTAMENTE</strong> <br />
    "Ejercer la libertad para trascender en el tiempo"<br /><br />
    <strong>MTRO. GERARDO PAUL ARVIZU SERAPIO <BR />
    DIRECTOR</strong></div>
    <br /><br />
    <span style="font-size:10px"> c.c.p: Archivo<br /><br />GPAS/fcc*</span>
        
    </td>
</tr>
<tr><td colspan="3" align="left"><br/><br/><br/><br /><br/><br/><br />
		<span style="font-size:10px">Calle Azteca Norte, Col. Reserva Territorial Quetzalc&oacute;atl, C.P. 72150 Tel. 01 222 283 6505, E-mail: ensfep130@gmail.comspan>
    </td></tr>       


</table>

</body>
</html>