<?

include '../../includes/conexion.php';
session_start();

if(empty($_POST["ciclo"])){
	$ciclo_escolar_actual = $_SESSION['cicloEscolar'];
}else{
	$ciclo_escolar_actual = $_POST["ciclo"];
	$_SESSION['cicloEscolar'] = $ciclo_escolar_actual;
}


//$ciclo_escolar_actual = 1;
//echo $ciclo_escolar_actual;
/*
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../index.php");
endif;
*/

/////////////// Obtener CICLO ESCOLAR/////////////////////////////////

$c_e_act = mysql_query("SELECT * FROM ciclo_escolar WHERE id_ciclo = '$ciclo_escolar_actual' ",$conexion);
while($ci_es_ac = mysql_fetch_assoc($c_e_act)):
		$cicloEscolar = $ci_es_ac['ciclo_escolar'];
endwhile;

////////////////////////////////////////////////////////////////////////////////////


// DATOS

	////saco especialidades
	$bajas=mysql_query("SELECT id_especialidad FROM bajas WHERE ciclo_escolar='$ciclo_escolar_actual' GROUP BY id_especialidad",$conexion);
	while($bjs=mysql_fetch_array($bajas)):
	$id_espe=$bjs['id_especialidad'];
		
	
	/// saco nombre de especialidad
	$nom_espe=mysql_query("SELECT nombreEspecialidad FROM catalogoespecialidades  WHERE idEspecialidad='$id_espe'",$conexion);
	if($nom_espe_=mysql_fetch_assoc($nom_espe)):
		$nombreEspec=($nom_espe_["nombreEspecialidad"]);		
	endif;
	
	///saco semestres
		$sem=mysql_query("SELECT semestre_baja FROM bajas WHERE id_especialidad='$id_espe' AND ciclo_escolar='$ciclo_escolar_actual' GROUP BY semestre_baja",$conexion);
		while($semes=mysql_fetch_assoc($sem)):
			$id_semestre=$semes['semestre_baja'];
	
	///saco # de inscritos por especialidad y semestre
	$num_alum_espe=mysql_query("SELECT COUNT(*) alu FROM ingreso WHERE id_especialidad='$id_espe' AND ciclo_escolar='$ciclo_escolar_actual' AND semestre_ingreso='$id_semestre' ",$conexion);
	if($num_alum_espe_=mysql_fetch_assoc($num_alum_espe)):
		$total_alumnos_espe=$num_alum_espe_['alu'];
	endif;
	
	///saco totales					
			$total_espe=mysql_query("SELECT COUNT(semestre_baja) dato FROM bajas WHERE semestre_baja='$id_semestre' AND id_especialidad='$id_espe'",$conexion);
			if($to=mysql_fetch_assoc($total_espe)):
				$total=$to['dato'];
			endif;
			
			
			
			$ttotall=($total*100)/$total_alumnos_espe;
			
			
			$sem_espe[]="SEM. ".$id_semestre." ".$nombreEspec;
			$porcentajes[]=number_format($ttotall,1,'.','');
			
			
		endwhile;
	
	endwhile;


/*************** COMIENZA GRAFICA **********************/

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_bar.php');

// Callback function for Y-scale to get 1000 separator on labels
function separator1000($aVal) {
    return $aVal;
}

function separator1000_usd($aVal) {
    return '$'.number_format($aVal);
}


$datay=$porcentajes; 


// Create the graph and setup the basic parameters
$graph = new Graph(600,500,'auto');
$graph->img->SetMargin(80,30,30,190);
$graph->SetScale('textint');
$graph->SetShadow();
$graph->SetFrame(true); // No border around the graph

// Add some grace to the top so that the scale doesn't
// end exactly at the max value.
// The grace value is the percetage of additional scale
// value we add. Specifying 50 means that we add 50% of the
// max value
$graph->yaxis->scale->SetGrace(20);
$graph->yaxis->SetLabelFormatCallback('separator1000');

// Setup X-axis labels
$graph->xaxis->SetTickLabels($sem_espe);
$graph->xaxis->SetLabelAngle(90);
$graph->xaxis->SetFont(FF_FONT0);



// Setup graph title ands fonts
$graph->title->Set('Grafica de Bajas del Ciclo Escolar '.$cicloEscolar);
$graph->title->SetFont(FF_FONT2,FS_BOLD);
$graph->xaxis->title->Set('');//Ciclo escolar XXXX-XXXX
$graph->xaxis->title->SetFont(FF_FONT2,FS_BOLD);



// Create a bar pot
$bplot = new BarPlot($datay);
$bplot->SetFillColor('orange');
$bplot->SetWidth(0.5);
$bplot->SetShadow();



///// numeros arriba de las barras ///////
// Setup the values that are displayed on top of each bar
$bplot->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
$bplot->value->SetFont(FF_ARIAL,FS_BOLD);
$bplot->value->SetAngle(45);
$bplot->value->SetFormatCallback('separator1000');

// Black color for positive values and darkred for negative values
$bplot->value->SetColor('black','darkred');
$graph->Add($bplot);

// Finally stroke the graph
$graph->Stroke();

?>

