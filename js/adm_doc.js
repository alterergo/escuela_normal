// JavaScript Document
/******** tooltip **********/
$( ".icono" ).tooltip({
       show: null,
       position: {
       my: "left top",
       at: "left bottom"
       },
       open: function( event, ui ) {
       ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
       }
});

/*********************************************************/
/********************* VALIDAR EMAIL *********************/
/*********************************************************/
function validar_email(valor)
    {
        // creamos nuestra regla con expresiones regulares.
        var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        // utilizamos test para comprobar si el parametro valor cumple la regla
        if(filter.test(valor))
            return true;
        else
            return false;
    }
	
	
$( "#dialog-form1" ).dialog({
            autoOpen: false,
            height: 700,
            width: 900,
            modal: true,
            buttons: {
                "Crear": function() {
                    
					if($("#nombre").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#errNom").html("El campo Nombre es Requerido");
						$("#nombre").focus();
						
						
					}else { $("#errNom").html(""); } 
					
					
					if($("#apellido_paterno").val() == ""){
						//alert("El Apellido Paterno es Requerido");
						$("#errApp").html("El Apellido Paterno es Requerido");
						$("#apellido_paterno").focus();
						
					}else { $("#errApp").html(""); } 
					
					
					if($("#apellido_materno").val() == ""){
						//alert("El Apellido Materno es Requerido");
						$("#errApm").html("El Apellido Materno es Requerido");
						$("#apellido_materno").focus();
						
					}else { $("#errApm").html(""); } 
					
										
					if($("#sexo").val() == ""){
						//alert("El campo Sexo es Requerido");
						$("#errSexo").html("El sexo es Requerido");
						$("#sexo").focus();
						
					} else { $("#errSexo").html(""); }
										
						
					if($("#correo").val() == ''){
						$("#errEmail").html("Ingrese un correo");
						
					}else if(validar_email($("#correo").val()))
					{
						$("#errEmail").html("");
					}else
					{
						$("#errEmail").html("El correo no es valido");
						
					}
 					
					if($("#tel").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errTel").html("El telefono es Requerido");
						$("#tel").focus();
						
					}else { $("#errTel").html(""); }
					
					if($("#direcc").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errDirecc").html("La dirección es Requerida");
						$("#direcc").focus();
						
					}else { $("#errDirecc").html(""); }
					
					if($("#rfc").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errRfc").html("El RFC es Requerido");
						$("#rfc").focus();
						
					}else { $("#errRfc").html(""); }
					
					if($("#curp").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errCurp").html("El Curp es Requerido");
						$("#curp").focus();
						
					}else { $("#errCurp").html(""); }
					
					if($("#grad_acad").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errGrad_acad").html("El grado academico es Requerido");
						$("#grad_acad").focus();
						
					}else { $("#errGrad_acad").html(""); }
					
					if($("#usuario").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errUsu").html("El usuario es Requerido");
						$("#usuario").focus();
						
					}else { $("#errUsu").html(""); }
					
					
					if($("#pass").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errPass").html("La contraseña es Requerida");
						$("#pass").focus();
						return;
					}else { $("#errPass").html(""); }
										
										
					
					// VALIDAR SI EXISTE EL DOCENTE 
					$.ajax({
						url: ' validar.php',
						type: 'POST',
						async: true,
						data: 'usuario='+$("#usuario").val(),
						dataType: 'html',
						success: function(data){
							//alert(data);
							if(data == 'activo'){
								//alert("La matricula ya existe");
								$("#usuario").focus();
								$("#errUsu").html("El usuario ya Existe");
								return;
							}else if(data == 'temporal'){
								//alert("temporal");
								$("#usuario").focus();
								$("#errUsu").html("El usuario tiene Baja Temporal. Revise la lista de Bajas");
								return;
							}else if(data == 'definitiva'){
								$("#usuario").focus();
								$("#errUsu").html("El usuario tiene Baja Definitiva. Revise la lista de Bajas");
								return;
							}else{
								
								// SI TODOS LOS CAMPOS SON CORRECTOS CREA AL DOCENTE
								$.post("crearDocente.php",
								$("#agregarDocente").serialize(),
								function(data){
									if(data == 1 ){
										
										//alert("Docente Resgistrado");
										$("#nvo_doc").html("Registro exitoso!!!");
										setTimeout("location.href='./'", 2300);	
										//$( "#asignar-materia" ).dialog( "open" );
										//document.location.reload();
										}else {
										//alert("Ocurrio un error al registrar al Docente");		
										$("#nvo_doc").html("Error al registrar al Docente");
										} 
									//$("#agregandoAlumno").html(data); 
								});
							}
						},
						error: function(data){
							alert("Error ");
							return;
						}
					});
					//fin validacion
										
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				$(this).closest('form').find("input[type=text], textarea").val("");
            }
        });
		
       	//MUESTRA PANTALLA DE NUEVO DOCENTE
 $( "#nuevoDocente" ).click(function() {
                $( "#dialog-form1" ).dialog( "open" );
            });
/********************************************************************************************************************/

/**********************INFORMACION ADMIN DOCENTES**************************************************/


//MUESTRA PANTALLA DE INFO ADMIN DOCENTE
 $( "#info_adminDocente" ).click(function() {
 $( "#dialog-infoadmin" ).dialog( "open" );
      });
/*********************************************************/
$( "#dialog-infoadmin" ).dialog({
            autoOpen: false,
            height: 650,
            width: 900,
            modal: true,
            buttons: {
                "Actualizar": function() {
                    
					if($("#nombre_edit").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#errNom_edit").html("El campo Nombre es Requerido").fadeIn(1000).delay(1000).fadeOut(1000);
						$("#nombre_edit").focus();
						return;
						
					}else { $("#errNom_edit").html(""); } 
					
					
					if($("#apellido_paterno_edit").val() == ""){
						//alert("El Apellido Paterno es Requerido");
						$("#errApp_edit").html("El Apellido Paterno es Requerido").fadeIn(1000).delay(1000).fadeOut(1000);
						$("#apellido_paterno_edit").focus();
						return;
					}else { $("#errApp_edit").html(""); } 
					
					
					if($("#apellido_materno_edit").val() == ""){
						//alert("El Apellido Materno es Requerido");
						$("#errApm_edit").html("El Apellido Materno es Requerido").fadeIn(1000).delay(1000).fadeOut(1000);
						$("#apellido_materno_edit").focus();
						return;
					}else { $("#errApm_edit").html(""); } 
					
										
					if($("#sexo_edit").val() == ""){
						//alert("El campo Sexo es Requerido");
						$("#errSexo_edit").html("El sexo es Requerido").fadeIn(1000).delay(1000).fadeOut(1000);
						$("#sexo_edit").focus();
						return;
					} else { $("#errSexo_edit").html(""); }
										
						
					if($("#correo_edit").val() == ''){
						$("#errEmail_edit").html("Ingrese un correo").fadeIn(1000).delay(1000).fadeOut(1000);
						return;
					}else if(validar_email($("#correo_edit").val()))
					{
						$("#errEmail_edit").html("");
					}else
					{
						$("#errEmail_edit").html("El correo no es valido").fadeIn(1000).delay(1000).fadeOut(1000);
						return;
					}
 																				                
					
					
					// SI TODOS LOS CAMPOS SON CORRECTOS ACTUALIZA AL DOCENTE
								$.post("edit_adminDocente.php",
								$("#infoadminDocente").serialize(),
								function(data){
									if(data == 1){
										
										//alert("Docente Resgistrado");
										$("#updt_msg_adm_doc").html("Actualización exitosa!!!");
										setTimeout("location.href='./'", 2300);	
										//document.location.reload();
										}else if(data == 0){
										//alert("Error al actualizar, Intentelo mas tarde");		
										$("#updt_msg_adm_doc").html("Error al actualizar, Intentelo mas tarde!!!");
										} 
									//$("#agregandoAlumno").html(data); 
								});
										
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				$(this).closest('form').find("input[type=text], textarea").val("");
            }
        });
			  

/***********************************************************************************/
/********************** FORMULARIO DE INFORMACION DE DOCENTES (VALIDACIONES)***********/

/*********************************************************/
$(document).ready(function(){
$( "#dialog-info" ).dialog({
            autoOpen: false,
            height: 700,
            width: 900,
            modal: true,
            buttons: {
                "Actualizar": function() {
                    
					if($("#nomb_edt").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#errNom_edt").html("El campo Nombre es Requerido");
						$("#nomb_edt").focus();
						return;
						
					}else { $("#errNom_edt").html(""); } 
					
					
					if($("#app_edt").val() == ""){
						//alert("El Apellido Paterno es Requerido");
						$("#errApp_edt").html("El Apellido Paterno es Requerido");
						$("#app_edt").focus();
						return;
					}else { $("#errApp_edt").html(""); } 
					
					
					if($("#apm_edt").val() == ""){
						//alert("El Apellido Materno es Requerido");
						$("#errApm_edt").html("El Apellido Materno es Requerido");
						$("#apm_edt").focus();
						return;
					}else { $("#errApm_edt").html(""); } 
					
					/*					
					if($("#sexo_edt").val() == ""){
						//alert("El campo Sexo es Requerido");
						$("#errSexo_edit").html("El sexo es Requerido");
						$("#sexo_edt").focus();
						
					} else { $("#errSexo_edit").html(""); }
					*/					
						
					if($("#correo_edt").val() == ''){
						$("#errCorreo_edt").html("Ingrese un correo");
						return;
					}else if(validar_email($("#correo_edt").val()))
					{
						$("#errCorreo_edt").html("");
					}else
					{
						$("#errEmail_edt").html("El correo no es valido");
						return;
					}
 					
					if($("#tel_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errTel_edt").html("El telefono es Requerido");
						$("#tel_edt").focus();
						return;
					}else { $("#errTel_edt").html(""); }
					
					if($("#direcc_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errDirecc_edt").html("La dirección es Requerida");
						$("#direcc_edt").focus();
						return;
					}else { $("#errDirecc_edt").html(""); }
					
					if($("#rfc_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errRfc_edt").html("El RFC es Requerido");
						$("#rfc_edt").focus();
						return;
					}else { $("#errRfc_edt").html(""); }
					
					if($("#curp_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errCurp_edt").html("El Curp es Requerido");
						$("#curp_edt").focus();
						return;
					}else { $("#errCurp_edt").html(""); }
					
					if($("#grad_acad_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errGrad_acad_edt").html("El grado academico es Requerido");
						$("#grad_acad_edt").focus();
						return;
					}else { $("#errGrad_acad_edt").html(""); }					
					
                		
										
					
					
					// SI TODOS LOS CAMPOS SON CORRECTOS ACTUALIZA AL DOCENTE
								$.post("edit_docentee.php",
								$("#updDocente").serialize(),
								function(data){
									//alert(data);
									if(data==1){
										
										//alert("Actualización exitosa");
										$("#updt_msg").html("Actualización exitosa").fadeIn(1000).delay(700).fadeOut(1000);
										//setTimeout("location.href='./'", 2300);
                                                                                $( "#dialog-info" ).dialog( "close" );
										//document.location.reload();
										}else {
											//alert(data);
										//alert("Error al actualizar, Intentelo mas tarde");
										$("#updt_msg").html("Error al actualizar, Intentelo nuevamente");	
										} 
									//$("#agregandoAlumno").html(data); 
								});
										
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				$(this).closest('form').find("input[type=text], textarea").val("");
            }
        });
});


/***********************************************************************************/
/********************** FORMULARIO DE ASIGNACION DE MATERIAS A DOCENTES (VALIDACIONES)***********/

/*********************************************************/

$(document).ready(function(){//1
$( "#materias-Docente" ).dialog({//2
            autoOpen: false,
            height: 650,
            width: 900,
            modal: true,
            buttons: {//3
                "Asignar": function() {//4
                    
					if($("#plan_estudios").val()==""){
						$("#errPlan").html("Seleccione el plan de estudios").fadeIn(1000).delay(1000).fadeOut(1000);
						$("#plan_estudios").focus();
						//return;
					}
					if($("#espe").val()==""){
						$("#errEspec").html("Seleccione la especialidad").fadeIn(1000).delay(1000).fadeOut(1000);
						//$("#plan_estudios").focus();
						//return;
					}					
					
					if($("#claveMat").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#errMater").html("Seleccione una clave").fadeIn(1000).delay(700).fadeOut(1000);
						//$("#clave").focus();
						return;
						
					}
																												
					if($("#plan_estudios").val()!="" && $("#espe").val()!="" && $("#claveMat").val() != ""){
					
					// SI TODOS LOS CAMPOS SON CORRECTOS ASIGNA MATERIA
								$.post("materias_doc_insert.php",
								$("#matDocente").serialize(),
								function(data){//5
									if(data==1 ){
										
										//alert("Actualización exitosa");
										$("#asig_msg").html("Asignación exitosa");
										//setTimeout("location.href='./'", 2300);	
										//document.location.reload();
										}else {
											
										//alert("Error al actualizar, Intentelo mas tarde");
										$("#asig_msg").html("Error al asignar, Intentelo nuevamente");	
										} 
									//$("#agregandoAlumno").html(data); 
								});//5
					}
					
                },//4
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            },//3
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				//document.location.reload();
				$(this).closest('form').find("input[type=text], textarea").val("");
            }

        });//2
});//1


/*
$(document).ready(function(){
$( "#materias-Docente" ).dialog({
            autoOpen: false,
            height: 500,
            width: 650,
            modal: true,
            buttons: {
                "Asignar": function() {
                    
					//var asignacion = $("#cambiarDocente").val();
					
					if($('input[name="tutores"]:radio').is(':checked')){
						//alert("Seleccionado");
						//si esta seleccionado el radio es por que se hara un cambio de tutor
						// y se envia el id de ciclo, la materia, el id del maestro que se va arrastrando, y el id del tutor que se va a reemplazar
						var ciclo = $("#ciclo_escolar").val();
						var idMateria = $("#idMateria").val();
						var nvoDocente = $("#id_doc").val();						
						var docenteAnterior = $("#tutorEdt").val();
						
						$.post("reemplazarTutor.php",
						{ciclo: ciclo, idMateria: idMateria,nvoDocente: nvoDocente,docenteAnterior: docenteAnterior },
						function(data){
							if(data==1){
							$("#asig_msg").html("Asignación exitosa").fadeIn(1000).delay(700).fadeOut(1000);
							setTimeout(function(){asignar_materias(idDocente);},3000);
							}
							else{
								$("#asig_msg").html("No se hizo la Asignación");
							}
							//alert(data); 
							
						});
						
					}else{
						//alert("No Seleccionado");
						//si no esta seleccionado se envian solo los datos de ciclo escolar, plan de estudios, y el id del docente o tutor
						var ciclo = $("#ciclo_escolar").val();
						var idMateria = $("#idMateria").val();
						var idDocente = $("#id_doc").val();
						
						$.post("insertTutor.php",
						{ciclo: ciclo,idMateria: idMateria,idDocente: idDocente},
						function(data){
							//alert(data); 
							$("#asig_msg").html("Asignación exitosa").fadeIn(1000).delay(700).fadeOut(1000);
							setTimeout(function(){asignar_materias(idDocente);},3000);
						
						});
						
					
						
					}
					
					
				
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				//document.location.reload();
				$(this).closest('form').find("input[type=text], textarea").val("");
            }

        });
});

*/
/***********************************************************************************/
/*****************************FUNCION MOSTRAR MATERIAS*********************************/
/***********************************************************************************/
$(document).ready(function(){
		$("#plan_estudios").change(function(){
			var plan = $("#plan_estudios option:selected").val();
		 	//alert(plan);
			
			$("#tr_especialidad").load("especialidad.php",{id_plan:plan});
			
        		
		});
});


function grupo_claves(){
	var espe = $("#espe option:selected").val();
	//var plan = $("#id_plan_hidd").val();
	$("#tr_clave").load("claves.php",{espe:espe});

}


function cambiarMat(){
	var op = $("#claveMat option:selected").val();
	var grupo=$("#grupo option:selected").val();
	//$('#materia').html(op);
	$("#datos_materia").load("datos_materia.php",{id:op, grupo:grupo});
}
/***********************************************************************************/
/********************** MATERIAS ASIGNADAS A DOCENTES  **************************/
/*********************************************************/
$(document).ready(function(){
$( "#materias-asignadas" ).dialog({
            autoOpen: false,
            height: 800,
            width: 900,
            modal: true,
            buttons: {
                
                Salir: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				$(this).closest('form').find("input[type=text], textarea").val("");
            }
        });
});

/*********************************************************************************************/
/***************************** FORMULARIO DE BAJA DEL DOCENTE *********************************/
/*********************************************************************************************/
$( "#dialog-bajaDocente" ).dialog({
            autoOpen: false,
            height: 400,
            width: 500,
            modal: true,
            buttons: {
                "Aceptar": function() {
					//$( this ).dialog( "close" );
					var idDocente = $("#idDocente").val();
					var tipoBaja = $("#tipoBaja").val();
					
					if(tipoBaja == "temporal"){
					var respTemp = confirm("¿Desea dar de Baja Temporal?");
					if(respTemp){
						$.post("bajaTemporal.php",
							{idDocente: idDocente }, 
							function(data){ 
								if(data==1){
								//alert("Alumno dado de baja Temporal");
								//document.location.reload();
								$("#mensaje").html("Docente dado de baja Temporal");
								setTimeout("location.href='./'", 2300);	
								}else{ $("#mensaje").html("No se pudo dar de baja al Docente"); }
							});
						}else{	location.reload(); }	
					}else if(tipoBaja == "definitiva"){
						//alert("Definitiva");
						var respuesta = confirm("Desea eliminar al Docente de forma Definitiva?");
						if(respuesta){
							$.post("bajaDefinitiva.php",
							{idDocente: idDocente }, 
							function(data){ 
								if(data==1){
								$("#mensaje").html("Docente dado de baja Definitiva");
								setTimeout("location.href='./'", 2300);	
								}else{ $("#mensaje").html("No se pudo dar de baja al Docente"); }
							});
							
						}
						else{
							$( this ).dialog( "close" );
						}
					}else{ $( this ).dialog( "close" ); }
					
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
});
/*********************************************************************************************************************************/
/*********************************************** OPCIONES DOCENTES *********************************************************/
/*********************************************************************************************************************************/
function informacion_doc(id){
	//alert(id);
	$.post("info_doc.php",
	{idDocente: id}, 
	function(data){ 
	/*alert(data);*/ 
	$("#infoDocente").html(data); 
	$( "#dialog-info" ).dialog( "open" ); });
	
}


function eliminar_doc(id){ 
	//alert(id);
	$("#bajaDocente").load("eliminar_doc.php",
	{idDocente: id},
	function(data){ $( "#dialog-bajaDocente" ).dialog( "open" ); });
}


function asignar_materias(id){
	//alert(id);
	$.post("materias_doc.php",
	{idDocente: id}, 
	function(data){ 
	/*alert(data);*/ 
	$("#materiasDocente").html(data); 
	$( "#materias-Docente" ).dialog( "open" ); });
	
}

function materias_asignadas(id){
	//alert(id);
	$.post("materias_asig.php",
	{idDocente: id}, 
	function(data){ 
	/*alert(data);*/ 
	$("#materiasAsignadas").html(data); 
	$( "#materias-asignadas" ).dialog( "open" ); });
	
}

/**********************************************************/
/***************** 	RESTAURAR DOCENTE TEMPORAL  ************/
/**********************************************************/
function restaurarTemporal(id){

	var respuesta = confirm("Desea Restaurar al Docente?");
	if(respuesta){
	//alert(id);
	$.post("restaurarTemporal.php",
	{idDocente: id},
	function(data){ 
		if(data==1){
			//alert("Alumno Restaurado");
			//location.reload();
			$("#mensaje_restau").html("Docente Restaurado");
			setTimeout("location.href='./'", 2200);	
			//document.location.href='index.php';
		}else{
			alert("Ocurrio un error. Intentelo nuevamente.");
		}	
	});
	}else{
		location.reload();
	}
}


/*****************************************************************************************************************************/
/******************************************************** SUBIR ARCHIVOS *****************************************************/
/*****************************************************************************************************************************/

function subirArchivos(clave){
	//alert(clave);
	//$.post("subirArchivos.php",{clave:clave});
	$("#materias").fadeOut(500);
	$("#dialog-subirArchivos").load("subirArchivos.php",{clave:clave});
	//$( "#dialog-subirArchivos" ).dialog( "open" );
	
}

function descargarExcel(clave,nombre){
	//alert(clave+" "+nombre);
	var id_materia = clave;
	var id_usuario = nombre;
	document.location='hoja_lista.php?id_materia='+id_materia+'&id_usuario='+id_usuario;
	
}


/*****************************************************************************************************************************/
/******************************************************** PASE DE LISTA *****************************************************/
/*****************************************************************************************************************************/
function pase_lista(mat,grupo){ //alert(mat);
	
	$("#contenido").empty();
	$("#contenido").load("pase_lista.php",{id_mat:mat, grupo:grupo});
}

function envia_lista(id_m,grupo){ //alert(id_m);

	if($("#dia").val() == ""){						
	$("#pas").html("Elija la fecha").slideDown(2000).delay(2000).slideUp(3000);
	$("#dia").focus();						
	return;					
	}else {  

				$.post("envio_lista.php",
				$("#envio_pase").serialize(),
				function (data){
					if(data==1){
						$('#pas').html('Pase de lista exitoso').fadeIn(2000).delay(1000).fadeOut(2000);
													
						setTimeout(function(){pase_lista(id_m,grupo);},3000);
					}else{
						$("#pas").html("Ocurrió un error, intentelo mas tarde");
						//$("#contenido").empty();
						//$("#contenido").load("pase_lista.php",{id_mat:id_m});
					}
				
				}); 	
		  }
}


/*****************************************************************************************************************************/
/***************************************************EDITAR PASE DE LISTA *****************************************************/
/*****************************************************************************************************************************/
function pase_lista_edit(mat,grupo){ //alert(mat);
	
	$("#contenido").empty();
	$("#contenido").load("pase_lista_edt.php",{id_mat:mat, grupo:grupo});
}

function modif_lista(id_m){ //alert(mat);
	
	if($("#dia_updt").val() == ""){						
	$("#dia_err").html("Seleccione una fecha");
	$("#dia_updt").focus();						

	}else if($("#alumno_pase").val() == ""){
		$("#dia_err").html("")						
	$("#al_err").html("Seleccione un alumno");
	$("#alumno_pase").focus();						
	
	}else if($("#pase_edit").val() == ""){		
		$("#al_err").html("");				
	$("#val_err").html("Escriba un valor");
	$("#pase_edit").focus();						
	
	} else{
		$("#val_err").html("");	
		
				$.post("envio_lista_edit.php",
				$("#envio_pase_updt").serialize(),
				function (data){
					if(data==1){
						$('#pas_updt').html('Modificación exitosa').slideDown(2000).delay(1000).slideUp(2000);
													
						//setTimeout(function(){pase_lista(id_m);},3000);
						setTimeout("location.href='./'",3000);
					}else{
						$("#pas_updt").html("Ocurrió un error, intentelo mas tarde").slideDown(2000).delay(1000).slideUp(2000);
						//$("#contenido").empty();
						//$("#contenido").load("pase_lista.php",{id_mat:id_m});
					}
				
				}); 	
		  }
}


/*****************************************************************************************************************************/
/******************************************************** INSERTAR PARCIAL *****************************************************/
/*****************************************************************************************************************************/
function calificaciones(mat,grupo){ //alert(mat);
	
	$("#contenido").empty();
	$("#contenido").load("calificaciones.php",{id_mat:mat, grupo:grupo});
}

function envia_calif(id_m){ //alert(id_m);

				$.post("envio_calific.php",
				$("#envio_calif").serialize(),
				function (data){
					if(data==1){
						$('#pas').html('Calificaciones almacenadas').fadeIn(2000).delay(1000).fadeOut(2000);
													
						setTimeout(function() {regresa();},3000);
					}else{
						$("#pas").html("Ocurrió un error, intentelo mas tarde");
						//$("#contenido").empty();
						//$("#contenido").load("pase_lista.php",{id_mat:id_m});
					}
				
				}); 	
		  
}

/*****************modifica calificaciones ************************/
function modif_calif_env(id_m){ //alert(id_m);

				$.post("modif_calific.php",
				$("#modif_calif").serialize(),
				function (data){
					if(data==1){
						$('#updtOk').html('Calificaciones almacenadas').fadeIn(2000).delay(1000).fadeOut(2000);
													
						setTimeout(function() {regresa();},3000);
					}else{
						$("#updtOk").html("Ocurrió un error, intentelo mas tarde");
						//$("#contenido").empty();
						//$("#contenido").load("pase_lista.php",{id_mat:id_m});
					}
				
				}); 	
		  
}


function regresa(){
document.location='./'	
}

/*****************************************************************************************************************************/
/******************************************************** HORAS DE CLASE *****************************************************/
/*****************************************************************************************************************************/
// $( "#horas_clase" ).click(function()  {
function horas_clase(id_mat,grupo){	
	$("#contenido").empty();
	$("#contenido").load("horas.php",{id_mat:id_mat, grupo:grupo});
}
////////dialog

function envia_horas(id_mat,grupo){
	var camp_horas=$("#horas").val();
	
	if (camp_horas==""){
		$("#yes").html("Ingrese las horas!!").slideDown(2000).delay(1000).slideUp(2000);
	}
	else {
	$.post("envio_hora.php",
	$("#envio_horas").serialize(),
	function (data){
			if(data==1){
				$("#yes").html("Horas registradas").fadeIn(1000).delay(1000).fadeOut(2000);
				setTimeout("location.href='./'",3100);			
			} else{
				$("#yes").html("Ocurrio un error, Intentelo nuevamente");
			}
	});
	}
}


/**********************INFORMACION DOCENTES (sesion docente)**************************************************/


//MUESTRA PANTALLA DE INFO ADMIN DOCENTE
 $( "#info_Docente1" ).click(function() {
 $( "#info_docente" ).dialog( "open" );
      });
/*********************************************************/
$( "#info_docente" ).dialog({
            autoOpen: false,
            height: 650,
            width: 900,
            modal: true,
            buttons: {
                "Actualizar": function() {
                    
					if($("#nomb_edt").val() == ""){
						//alert("El campo Nombre es Requerido");
						$("#errNom_edt").html("El campo Nombre es Requerido");
						$("#nomb_edt").focus();
						
						
					}else { $("#errNom_edt").html(""); } 
					
					
					if($("#app_edt").val() == ""){
						//alert("El Apellido Paterno es Requerido");
						$("#errApp_edt").html("El Apellido Paterno es Requerido");
						$("#app_edt").focus();
						
					}else { $("#errApp_edt").html(""); } 
					
					
					if($("#apm_edt").val() == ""){
						//alert("El Apellido Materno es Requerido");
						$("#errApm_edt").html("El Apellido Materno es Requerido");
						$("#apm_edt").focus();
						
					}else { $("#errApm_edt").html(""); } 
															
						
					if($("#correo_edt").val() == ''){
						$("#errCorreo_edt").html("Ingrese un correo");
						
					}else if(validar_email($("#correo_edt").val()))
					{
						$("#errCorreo_edt").html("");
					}else
					{
						$("#errEmail_edt").html("El correo no es valido");
						
					}
 					
					if($("#tel_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errTel_edt").html("El telefono es Requerido");
						$("#tel_edt").focus();
						
					}else { $("#errTel_edt").html(""); }
					
					if($("#direcc_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errDirecc_edt").html("La dirección es Requerida");
						$("#direcc_edt").focus();
						
					}else { $("#errDirecc_edt").html(""); }
					
					if($("#rfc_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errRfc_edt").html("El RFC es Requerido");
						$("#rfc_edt").focus();
						
					}else { $("#errRfc_edt").html(""); }
					
					if($("#curp_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errCurp_edt").html("El Curp es Requerido");
						$("#curp_edt").focus();
						
					}else { $("#errCurp_edt").html(""); }
					
					if($("#grad_acad_edt").val() == ""){
						//alert("La Matricula es Requerida");
						$("#errGrad_acad_edt").html("El grado academico es Requerido");
						$("#grad_acad_edt").focus();
						return;
					}else { $("#errGrad_acad_edt").html(""); }
 													
										
					
					
					// SI TODOS LOS CAMPOS SON CORRECTOS ACTUALIZA AL DOCENTE
								$.post("edit_docente.php",
								$("#updDocente_sess").serialize(),
								function(data){
									if(data == 1){
										
										//alert("Docente Resgistrado");
										$("#updt_msg").html("Actualización exitosa!!!");
										setTimeout("location.href='./'", 2300);	
										//document.location.reload();
										}else if(data == 0){
										//alert("Error al actualizar, Intentelo mas tarde");		
										$("#updt_msg").html("Error al actualizar, Intentelo mas tarde!!!");
										} 
									//$("#agregandoAlumno").html(data); 
								});
										
					
                },
                Cancelar: function() {
                    $( this ).dialog( "close" );
                }
            },
            close: function() {				
                allFields.val("").removeClass( "ui-state-error" );
				$(this).closest('form').find("input[type=text], textarea").val("");
            }
        });
			  
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// ELIMINAR MATERIA A DOCENTE //////////////////////////////////////

function eliminar_md(id_mm){

	var respuesta = confirm("Desea Eliminar la materia?");
	if(respuesta){
	//alert(id);
	$.post("eliminar_mat.php",
	{id_mm: id_mm},
	function(data){ 
		if(data==1){
			//alert("Alumno Restaurado");
			//location.reload();
			$("#elim_ok").html("Materia eliminada").fadeIn(1000).delay(700).fadeOut(1000);
			//setTimeout("location.href='./'", 2200);	
			//document.location.href='index.php';
		}else{
			$("#elim_ok").html("No se pudo eliminar la Materia").fadeIn(1000).delay(700).fadeOut(1000);
		}	
	});
	}else{
		location.reload();
	}
}


/****************************************************************************************************************************/
/**************************************************************** BUSQUEDA **************************************************/
/****************************************************************************************************************************/
//BUSCADOR DE DOCENTES
function buscarDocente(){
	var xmlhttp;
	var n=document.getElementById('search').value;
	

	if(n==''){
	 document.getElementById("myDiv").innerHTML="";
	 document.getElementById("tablaDocentes").style.display='';
	 document.getElementById("paginate").style.display='';
	 return;
	}
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
		}else{ 
			document.getElementById("myDiv").innerHTML='<img src="../images/load.gif" width="50" height="50" />'; 
			document.getElementById("tablaDocentes").style.display='none';
			document.getElementById("paginate").style.display='none';
			}
	  }
	
	xmlhttp.open("POST","busquedaDocente.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q="+n);
	
}