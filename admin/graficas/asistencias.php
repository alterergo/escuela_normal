<div style="border: #000 solid 1px; width: 816px; height: 1056px; ">
<table width='100%'>
<tr>
	<td align='center' width='80%'><b>ESCUELA NORMAL SUPERIOR FEDERALIZADA DEL ESTADO DE PUEBLA</b></td>
	<td width='20%'><img src="../../images/logo.png" width='100' /></td>
</tr>
</table>

<?php
	include '../../includes/conexion.php';

	$cicloEscolarPost = $_GET["ciclo"];
	$planEstudiosPost = $_GET["plan"];
	//$cicloEscolarPost = 1;
	//$planEstudiosPost = 1;
	
	
	
	/*
	*	Se obtiene el ciclo escolar
	*/
	$c_e_act = mysql_query("SELECT * FROM ciclo_escolar WHERE id_ciclo = '$cicloEscolarPost' ",$conexion);
	while($ci_es_ac = mysql_fetch_assoc($c_e_act)):
			$cicloEscolar = $ci_es_ac['ciclo_escolar'];
	endwhile;
?>
<!-- INICIA TABLA DE DATOS -->
<table border='1' cellpadding='0' cellspacing='0' align='center'>
<tr>
	<th> GRADO Y ESPECIALIDAD </th>
	<th> PROMEDIO DE<BR/> ASISTENCIAS </th>
</tr>

<?php
	/*
	* Obtenemos el id de cada especialidad
	*/
	$gradoo = '';
	$idEspec = mysql_query(" SELECT * FROM catalogoespecialidades WHERE estatus = 1 AND plan_estudios = '$planEstudiosPost' ORDER BY nombreEspecialidad ASC ",$conexion);
	while($resultEspec = mysql_fetch_assoc($idEspec)){ //W1
		$idEspecialidad = $resultEspec["idEspecialidad"];
		$nombreEspecialidad = ($resultEspec["nombreEspecialidad"]);
		//echo "Espe: ".$idEspecialidad;
		//Obtenemos los semestres de la especialidad leida
		$contadorMaterias = 0;
		$semestre = mysql_query(" SELECT * FROM sem_espe AS t1, semestre AS t2 WHERE t1.id_especialidad='$idEspecialidad' AND t1.id_semestre=t2.id_semestre ORDER BY t2.id_semestre ASC ",$conexion);		
			while($resultSemestre = mysql_fetch_assoc($semestre)){ //w2
			$idSemestre = $resultSemestre["id_semestre"];
			//echo "S: ".$idSemestre;
			$nombreSemestre = $resultSemestre["semestre"];
			$grado = $resultSemestre["grado"];
			//echo "G: ".$grado.'****';
			if($gradoo == ""){
				
				$sumatoriaPromediosMateria = 0;
				$materias = mysql_query(" SELECT * FROM materias WHERE id_especialidad = '$idEspecialidad' AND id_semestre = '$idSemestre' AND estatus = '1' ",$conexion);
				while($resultMaterias = mysql_fetch_assoc($materias)){//W3
					$idMateria = $resultMaterias["id_materia"];
					//echo "<br/>M: ".$idMateria;
						//Obtenemos Id del alumno 
						$contadorAlumnos = 0;						
						$calificacionFinal = 0;
						$sumaPromediosAsistencia = 0;
						$total_horasClase=0;
						$promedioTotalAsistenciasGrupo=0;
						
						// obtengo el total de horas del curso (P1 +P2 + P3)
						$horasClase=mysql_query("SELECT * FROM horas_clase WHERE id_materia='$idMateria' AND id_ciclo_escolar='$cicloEscolarPost'",$conexion);
						while($horasClase_=mysql_fetch_assoc($horasClase)):
							$total_horas=$horasClase_["total_horas"]; 
							$total_horasClase=$total_horasClase+$total_horas;
							//echo "horas".$total_horasClase;
							$total_horas="";
						endwhile;
						
						$asis=mysql_query("SELECT * FROM pase_lista WHERE id_materia='$idMateria' AND ciclo_escolar='$cicloEscolarPost' GROUP BY id_alumno",$conexion);						
						while($asis_ = mysql_fetch_assoc($asis)){//W4
							$idAlumno = $asis_["id_alumno"];
							//echo "A: ". $idAlumno."<br/>";
							//$i = 0;
							
							//Obtenemos Asistencias de cada alumno por materia
							$asistenciaDia = "";
							$totalAsistencias = 0;
							$promedioAsistencia=0;
							$asistencias = mysql_query(" SELECT * FROM pase_lista WHERE id_alumno = '$idAlumno' AND ciclo_escolar = '$cicloEscolarPost' AND id_materia = '$idMateria' ",$conexion);							
							while($asistencias_ = mysql_fetch_assoc($asistencias)){//W5
								$asistenciaDia = $asistencias_["asistencia"];
									if($asistenciaDia!="" && $asistenciaDia!='/' && $asistenciaDia!='0'):
										$totalAsistencias = $totalAsistencias + $asistenciaDia;
									endif;
									//echo "totalAsistencias: ".$totalAsistencias;
								//$i++;
							} //W5
							//Obtenemos promedio de asistencias de materia por alumno							
							$promedioAsistencia=($totalAsistencias*100)/$total_horasClase; 
							//echo "PROMEDIO ASISTENCIA ALUMNO MAT: ".$promedioAsistencia;
							
							//Obtenemos sumatoria de todos los promedios de asistencia de cada alumno por materia
							$sumaPromediosAsistencia = number_format($sumaPromediosAsistencia + $promedioAsistencia,2); 
							//echo "SUMA PROMEDIOS Asistencia: ".$sumaPromediosAsistencia;
							$contadorAlumnos++;
							//echo "total alumno: ".$contadorAlumnos;
						}//W4
				
				$contadorMaterias++;
				//Obtenemos el promedio de asistencias de la materia de todos los alumnos y se divide entre el # de alumnos
				$promedioTotalAsistenciasGrupo = $sumaPromediosAsistencia/$contadorAlumnos; 
				//echo "PROMEDIO GRUPO ASIST: ".number_format($promedioTotalAsistenciasGrupo, 1);
				
				// sumo promedios
				$sumatoriaPromediosMateria = number_format($sumatoriaPromediosMateria + $promedioTotalAsistenciasGrupo,2);
				//echo "Sumatoria Promedios Materia: ".$sumatoriaPromediosMateria;
				//echo "Contador Materias".$contadorMaterias;
				//echo "<br/>";
				}//w3
				
				//$promedioTotal es el promedio que necesitamos en la tabla (del semestre 1)
				$promedioTotalS1 = number_format($sumatoriaPromediosMateria / $contadorMaterias,1); 
				//echo "PROMEDIO TOTAL(1) : ".$promedioTotalS1;
				$gradoo = $grado;
				//echo "Promedio Total Vacio: ".$promedioTotal;
				$primeraVez = 1;
				
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}else{//Inicia validacion de que gradoo no es vacio
			//Comparamos que el grado consultado es igual a la bandera de gradoo(Coparar si seguimos en el mismo grado)

			if($grado==$gradoo){
				$contadorMaterias = 0;
				$promedioTotalS2 = 0;
				$sumatoriaPromediosMateria = 0;
				$materias = mysql_query(" SELECT * FROM materias WHERE id_especialidad = '$idEspecialidad' AND id_semestre = '$idSemestre' AND estatus = '1' ",$conexion);
				while($resultMaterias = mysql_fetch_assoc($materias)){//W3
					$idMateria = $resultMaterias["id_materia"];
					//echo "<br/>M: ".$idMateria;
						//Obtenemos Id del alumno 
						$contadorAlumnos = 0;						
						$calificacionFinal = 0;
						$sumaPromediosAsistencia = 0;
						$total_horasClase=0;
						$promedioTotalAsistenciasGrupo=0;
						
						// obtengo el total de horas del curso (P1 +P2 + P3)
						$horasClase=mysql_query("SELECT * FROM horas_clase WHERE id_materia='$idMateria' AND id_ciclo_escolar='$cicloEscolarPost'",$conexion);
						while($horasClase_=mysql_fetch_assoc($horasClase)):
							$total_horas=$horasClase_["total_horas"]; 
							$total_horasClase=$total_horasClase+$total_horas;
							//echo "horas".$total_horasClase;
							$total_horas="";
						endwhile;
						
						$asis=mysql_query("SELECT * FROM pase_lista WHERE id_materia='$idMateria' AND ciclo_escolar='$cicloEscolarPost' GROUP BY id_alumno",$conexion);						
						while($asis_ = mysql_fetch_assoc($asis)){//W4
							$idAlumno = $asis_["id_alumno"];
							//echo "A: ". $idAlumno."<br/>";
							//$i = 0;
							
							//Obtenemos Asistencias de cada alumno por materia
							$asistenciaDia = "";
							$totalAsistencias = 0;
							$promedioAsistencia=0;
							$asistencias = mysql_query(" SELECT * FROM pase_lista WHERE id_alumno = '$idAlumno' AND ciclo_escolar = '$cicloEscolarPost' AND id_materia = '$idMateria' ",$conexion);							
							while($asistencias_ = mysql_fetch_assoc($asistencias)){//W5
								$asistenciaDia = $asistencias_["asistencia"];
									if($asistenciaDia!="" && $asistenciaDia!='/' && $asistenciaDia!='0'):
										$totalAsistencias = $totalAsistencias + $asistenciaDia;
									endif;
									//echo "totalAsistencias: ".$totalAsistencias;
								//$i++;
							} //W5
							//Obtenemos promedio de asistencias de materia por alumno							
							$promedioAsistencia=($totalAsistencias*100)/$total_horasClase; 
							//echo "PROMEDIO ASISTENCIA ALUMNO MAT: ".$promedioAsistencia;
							
							//Obtenemos sumatoria de todos los promedios de asistencia de cada alumno por materia
							$sumaPromediosAsistencia = number_format($sumaPromediosAsistencia + $promedioAsistencia,2); 
							//echo "SUMA PROMEDIOS Asistencia: ".$sumaPromediosAsistencia;
							$contadorAlumnos++;
							//echo "total alumno: ".$contadorAlumnos;
						}//W4
				
				$contadorMaterias++;
				//Obtenemos el promedio de asistencias de la materia de todos los alumnos y se divide entre el # de alumnos
				$promedioTotalAsistenciasGrupo = $sumaPromediosAsistencia/$contadorAlumnos; 
				//echo "PROMEDIO GRUPO ASIST: ".number_format($promedioTotalAsistenciasGrupo, 1);
				
				// sumo promedios
				$sumatoriaPromediosMateria = number_format($sumatoriaPromediosMateria + $promedioTotalAsistenciasGrupo,2);
				//echo "Sumatoria Promedios Materia: ".$sumatoriaPromediosMateria;
				//echo "Contador Materias".$contadorMaterias;
				//echo "<br/>";
				}//w3
				
				//$promedioTotal es el promedio que necesitamos en la tabla (del semestre 1)
				$promedioTotalS2 = number_format($sumatoriaPromediosMateria / $contadorMaterias,1);				
				//echo "PROMEDIO TOTAL: ".$promedioTotalS2;
				
				$promedioGrafica = ($promedioTotalS2 + $promedioTotalS1)/2;
				//echo " PROMEDIO FINAL GRAFICA (2): ".$promedioGrafica;
				if($promedioGrafica != 0){
?>
				<tr>
					<td><?php echo $grado."o ".$nombreEspecialidad; ?></td>
					<td><?php echo $promedioGrafica; ?></td>
					
				</tr>
<?php
				
				$promedioGrafica = 0;
				}
			}else{			
				
				$sumatoriaPromediosMateria = 0;
				$promedioTotalS1 = 0;
				$contadorMaterias = 0;
				$sumatoriaPromediosMateria = 0;
				$materias = mysql_query(" SELECT * FROM materias WHERE id_especialidad = '$idEspecialidad' AND id_semestre = '$idSemestre' AND estatus = '1' ",$conexion);
				while($resultMaterias = mysql_fetch_assoc($materias)){//W3
					$idMateria = $resultMaterias["id_materia"];
					//echo "<br/>M: ".$idMateria;
						//Obtenemos Id del alumno 
						$contadorAlumnos = 0;						
						$calificacionFinal = 0;
						$sumaPromediosAsistencia = 0;
						$total_horasClase=0;
						$promedioTotalAsistenciasGrupo=0;
						
						// obtengo el total de horas del curso (P1 +P2 + P3)
						$horasClase=mysql_query("SELECT * FROM horas_clase WHERE id_materia='$idMateria' AND id_ciclo_escolar='$cicloEscolarPost'",$conexion);
						while($horasClase_=mysql_fetch_assoc($horasClase)):
							$total_horas=$horasClase_["total_horas"]; 
							$total_horasClase=$total_horasClase+$total_horas;
							//echo "horas".$total_horasClase;
							$total_horas="";
						endwhile;
						
						$asis=mysql_query("SELECT * FROM pase_lista WHERE id_materia='$idMateria' AND ciclo_escolar='$cicloEscolarPost' GROUP BY id_alumno",$conexion);						
						while($asis_ = mysql_fetch_assoc($asis)){//W4
							$idAlumno = $asis_["id_alumno"];
							//echo "A: ". $idAlumno."<br/>";
							//$i = 0;
							
							//Obtenemos Asistencias de cada alumno por materia
							$asistenciaDia = "";
							$totalAsistencias = 0;
							$promedioAsistencia=0;
							$asistencias = mysql_query(" SELECT * FROM pase_lista WHERE id_alumno = '$idAlumno' AND ciclo_escolar = '$cicloEscolarPost' AND id_materia = '$idMateria' ",$conexion);							
							while($asistencias_ = mysql_fetch_assoc($asistencias)){//W5
								$asistenciaDia = $asistencias_["asistencia"];
									if($asistenciaDia!="" && $asistenciaDia!='/' && $asistenciaDia!='0'):
										$totalAsistencias = $totalAsistencias + $asistenciaDia;
									endif;
									//echo "totalAsistencias: ".$totalAsistencias;
								//$i++;
							} //W5
							//Obtenemos promedio de asistencias de materia por alumno							
							$promedioAsistencia=($totalAsistencias*100)/$total_horasClase; 
							//echo "PROMEDIO ASISTENCIA ALUMNO MAT: ".$promedioAsistencia;
							
							//Obtenemos sumatoria de todos los promedios de asistencia de cada alumno por materia
							$sumaPromediosAsistencia = number_format($sumaPromediosAsistencia + $promedioAsistencia,2); 
							//echo "SUMA PROMEDIOS Asistencia: ".$sumaPromediosAsistencia;
							$contadorAlumnos++;
							//echo "total alumno: ".$contadorAlumnos;
						}//W4
				
				$contadorMaterias++;
				//Obtenemos el promedio de asistencias de la materia de todos los alumnos y se divide entre el # de alumnos
				$promedioTotalAsistenciasGrupo = $sumaPromediosAsistencia/$contadorAlumnos; 
				//echo "PROMEDIO GRUPO ASIST: ".number_format($promedioTotalAsistenciasGrupo, 1);
				
				// sumo promedios
				$sumatoriaPromediosMateria = number_format($sumatoriaPromediosMateria + $promedioTotalAsistenciasGrupo,2);
				//echo "Sumatoria Promedios Materia: ".$sumatoriaPromediosMateria;
				//echo "Contador Materias".$contadorMaterias;
				//echo "<br/>";
				}//w3
				
				//$promedioTotal es el promedio que necesitamos en la tabla (del semestre 1)
				$promedioTotalS1 = number_format($sumatoriaPromediosMateria / $contadorMaterias,1);
				//echo "PROMEDIO TOTAL(3): ".$promedioTotalS1;
				$gradoo = $grado;
				//echo "Promedio Total Vacio: ".$promedioTotal;
				
			}//Termina else
			
		}//Termina else de comprobacion que gradoo no es vacio
			
			
			
		}//w2
		
	}//w1
	
	
?>
</table>


<br/><br/>

<div style="text-align: center; ">
		<img src="asistencias_grafica.php" />
	</div>

</div>