<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../index.php");
endif;

//echo $id_usuario;

if(empty($_POST["estatus"])){
	$_SESSION["estatusCiclo"]=1;
}else{
	$_SESSION["estatusCiclo"]=$_POST["estatus"];
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ciclo Escolar</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>
</head>
<body onload="ciclo_escolar();">
<div id="contenedorAdminCiclo" class="row">

<div id="headerAdmin" class="grid_11">
	<?php include 'menu.php'; ?>
</div>
<div id="myDiv" class="grid_11"></div>
<div id="contenido" class="grid_11">
<div id="tabs">
 <ul>
<li><a href="#tabs-1">Ciclos Escolares</a></li>
<li><a href="#tabs-2">Parciales</a></li>
<li><a href="#tabs-3">Plan Estudios</a></li>
</ul>
<!-- tab-1 -->
<div id="tabs-1">
<? if($tipo_usuario==5 || $tipo_usuario==4): ?>
	<div class="grid_3" id="nuevoCiclo" style="cursor:pointer"><img src="../images/agregar.png" class="icono" ><span style="color:#0066CC;">Nuevo Ciclo</span></div>
    <? endif; ?>
<div class="margen_4 search" id="buscador">
	<div id="contEstatus" class="contEstatus">
		<form method="POST" name="enviarEstatus">
		<label>Estatus</label>
		<select class="ui-widget-content" id="estatus" name="estatus" onChange="document.enviarEstatus.submit()">
			<!--option value="">Seleccione</option-->
			<option value="1" <?php if($_POST["estatus"] == 1){ ?> selected="selected" <?php }?> >Activos</option>
			<option value="2" <?php if($_POST["estatus"] == 2){ ?> selected="selected" <?php }?> >Inactivos</option>
		</select>
		</form>
	</div>
	<div id="contBusqueda" class="contBusqueda">
	Buscar <input type="text" id="searchCiclo" name="searchCiclo" onKeyUp="buscarCiclo()" placeholder="Ciclo Escolar" class="ui-widget-content "/> 
	<img src="../images/lupa.png" id="lupa" class="lupa">
	</div>
	
</div>
<!--div class="margen_7"><span id="registrosCiclo" class="restaurar">Lista de Bajas</span></div-->
<div id="myDiv" class="tooltip"></div>
<table id="tablaCiclos" cellpadding="15" class="table_cons">
    <thead>
        <th>#</th>	
        <th>Ciclo Escolar</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php 
	require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#	
	
	if(empty($_POST["estatus"])){
		$estatus = 1;
	}else{
		$estatus = $_POST["estatus"];
	}
	
	$query = "SELECT * FROM ciclo_escolar WHERE activo = ".$estatus;
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = isset($_GET['page']);
	$cantidad = 10;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#
	$i = 1;
	$consulta = "Select * from ciclo_escolar WHERE activo = $estatus LIMIT $desde, $cantidad";
	$respuesta = mysql_query($consulta,$conexion);
	while($row = mysql_fetch_array($respuesta)){
	
	if($row["en_curso"]==1){ $en_curso = "<img src='../images/ok.png' title='En curso' class='icono'>"; }else{ $en_curso = " ";}
	
	echo "<tr>";
	echo "<td>".$i."</td>";
	echo "<td>".utf8_encode($row["ciclo_escolar"])."</td>";
	echo "<td>";
	if($estatus == 1){
		if($tipo_usuario==5 || $tipo_usuario==4):
	echo "<img src='../images/editar.png' title='Editar' onClick='editarCiclo(".$row['id_ciclo'].")' class='icono'>";
	//<img src='../images/eliminar.png' title='Eliminar' onclick='eliminarCiclo(".$row['id_ciclo'].")' class='icono'>
		endif;
		
	echo "$en_curso";
		
	
	}elseif($estatus == 2){
		if($tipo_usuario==5 || $tipo_usuario==4):
		echo "<img src='../images/restaurar.png' title='Restaurar' onclick='restaurarCiclo(".$row['id_ciclo'].")' class='icono'>";
		endif;
	}
	echo "</td>";
	echo "</tr>";
	$i++;
	}
?>
</tbody>
</table>
<?php
	#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div id='paginate' class='paginacion'>";
	$url = "ciclo_escolar_admin.php?";
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#---------------------------------------------------------------------#
?>

</div>
<!-- TERMINA TAB-1 -->

<!-- tab-2 -->
<div id="tabs-2"> 
<table id="tablaParciales" cellpadding="15" class="table_cons">
    <thead>
        <th>#</th>	
        <th>Ciclo Escolar</th>
        <th>Activar</th>
    </thead>
<tbody>
<?php
	$consultaParciales = " SELECT * FROM parciales ";
	$result = mysql_query($consultaParciales,$conexion);
	while($row = mysql_fetch_array($result)){
	echo "<tr>";
	echo "<td>".$row["id_parcial"]."</td>";
	echo "<td>".$row["nombre_parcial"]."</td>";
	echo "<td>";
	
	if($tipo_usuario==5 || $tipo_usuario==4):
		echo "<img src='../images/editar.png' title='Activar Parcial' class='icono' onClick='activarParcial(".$row["id_parcial"].")' >";
	endif;
	
	if($row["estatus"] == 1){
		echo "<img src='../images/ok.png' class='icono' title='Activo'>";
	}
	
	echo "</td>";
	echo "</tr>";
	
	}
	
?>
</tbody>
</table>
</div>
<!-- TERMINA TAB-2 -->

<!-- tab-3 -->
<div id="tabs-3">
<? if($tipo_usuario==5 || $tipo_usuario==4): ?>
    <div>
    <a href="#" id="nuevoPlan"><img src="../images/agregar.png" class="icono"> <span style="color:#0066CC;">Nuevo Plan</span></a>
    </div>
<? endif; ?>

<table id="tablaPlan" cellpadding="15" class="table_cons">
    <thead>
        <th>#</th>	
        <th>Plan de Estudios</th>
        <th>Años</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php
	$num=1;
	$p_e=mysql_query("SELECT * FROM plan_estudios ",$conexion); 
		while($sel_plan=mysql_fetch_assoc($p_e)):
			$id_plan=$sel_plan['id_plan'];
			$plan=$sel_plan['plan_estudios'];
			$anios_plan=$sel_plan['anios'];
			
			echo "<tr><td>".$num."</td>";		
			echo "<td>".$plan."</td>";
			echo "<td>".$anios_plan." años</td>";
			
			echo "<td>";
				if($tipo_usuario==5 || $tipo_usuario==4):
					echo "<img src='../images/editar.png' title='Editar Plan' class='icono' onClick='editarPlan(".$id_plan.")' >";			
			endif;
			echo "</td></tr>";		
			$num++;
		endwhile;
	
?>
</tbody>
</table>
</div>
<!-- TERMINA TAB-3 -->

</div><!-- TERMINAN TABS -->

</div><!-- TERMINA CONTENIDO -->

<!--------------------------------- FORMULARIOS ------------------------------------------->
<div id="dialog-cicloEscolar" title="Nuevo Ciclo">
<form>
<fieldset>
<legend>Nuevo Ciclo</legend>

<div class="grid_4"><label for="nombreCiclo">Nombre del Ciclo</label></div>
<div class="grid_4"><input type="text" id="nombreCiclo" name="nombreCiclo" class="text ui-widget-content ui-corner-all" title="Ej: 2012-2013" /><br/><span class="error" id="errCiclo"></span></div>

</fieldset>
<div id="resultCiclo" class="mensajes"></div>
</form>
</div>

<div id="dialog-nuevoPlan" title="Nuevo Plan Escolar">
<form>
<fieldset>
<legend>Nuevo Plan</legend>

<div class="grid_4"><label for="nombrePlan">Nombre del Plan</label></div>
<div class="grid_4"><input type="text" id="nombrePlan" name="nombrePlan" class="text ui-widget-content ui-corner-all" /><br/><span class="error" id="errPlan"></span></div>

<div class="grid_4"><label for="aniosPlan">Años del Plan</label></div>
<div class="grid_4"><input type="text" id="aniosPlan" name="aniosPlan" class="text ui-widget-content ui-corner-all" /><br/><span class="error" id="errAnios"></span></div>

</fieldset>
<div id="resultPlan" class="mensajes"></div>
</form>
</div>

<!------------------- DIV CONTENEDOR DEL FORM DE EDICION ---------------------------------->
<div id="dialog-editarCiclo" title="Editar Ciclo"> <div id="cargarFormCiclo"></div> </div>

<!------------------- DIV CONTENEDOR DEL FORM DE EDICION DE PLAN -------------------------->
<div id="editarPlan" title="Editar Plan de Estudios"> <div id="edit_Plan"></div> </div>

<!-------------------------- DIC CONTENEDOR RESTAURACION DE CICLOS ----------------------------->
<div id="dialog-restaurarCiclo" title="Restaurar Ciclos"> <div id="frmRestaurarCiclos"></div> </div>

<!------------------------ CONTENEDOR DE CAMBIO DE PARCIALES -------------------------------->
<div id="dialog-cambiarParcial" title="Cambio de Parcial"> <div id="frmParcial"></div> </div>


<!------------------------------- TERMINAN FORMULARIOS ------------------------------------>

</div>

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/planesEstudio.js"></script>
</body>
</html>