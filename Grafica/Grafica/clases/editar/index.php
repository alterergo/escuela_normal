<?php
session_start();
$idciclo=$_SESSION['ciclo'];
$idmateria=$_SESSION['idmateria'];
$concentrado=$_SESSION['concentrado'];
$promedios=$_SESSION['promedios'];
echo '<br>';
echo "el ciclo es: ",$idciclo;
echo '<br>';
echo "la materiaes:",$idmateria;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="esp" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<title>Modificar Calificaciones</title>
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<script type="text/javascript">
	function quitarCampo(idtr)
	{
		var eliminar = document.getElementById(idtr);
		var contenedor= document.getElementById("Contenido");
		contenedor.removeChild(eliminar);
	}
	</script>
</head>
<body>
<div class="container">
	<div class="table-responsive">
		<form action="guardar.php" method="post">
		<table class="table">
			<thead class="success">
				<tr>
					<td>Nombre</td>
					<td>Primer Parcial</td>
					<td>Segundo Parcia</td>
					<td>Tercer Parcial</td>
					<td>Semestral</td>
					<td>Grupo</td>
					<td></td>
				</tr>
			</thead>
			<tbody id="Contenido">
				<?php
				$grupo=$_SESSION['grupo'];
				$posicion=0;
				foreach ($concentrado as $key => $value) 
				{
					
					echo '<tr id="'.$posicion.'">';
					echo '<td>'.$concentrado[$posicion][1].'<input type="hidden" name="alumno_'.$posicion.'[]" value="'.$concentrado[$posicion][0].'"></td>';
					echo '<td><input type="text"class="form-control" name="alumno_'.$posicion.'[]" value="'.$concentrado[$posicion][2].'" required></td>';
					echo '<td><input type="text"class="form-control" name="alumno_'.$posicion.'[]" value="'.$concentrado[$posicion][3].'" required></td>';
					echo '<td><input type="text"class="form-control" name="alumno_'.$posicion.'[]" value="'.$concentrado[$posicion][4].'" required></td>';
					echo '<td><input type="text"class="form-control" name="alumno_'.$posicion.'[]" value="'.$concentrado[$posicion][5].'" required></td>';
					echo '<td><input type="text"class="form-control" name="alumno_'.$posicion.'[]" value="'.$grupo.'" required></td>';
					echo '<td><a class="btn btn-warning" href="JavaScript:quitarCampo('.$posicion.');" >Eliminar</a></td>';	
					echo '</tr>';
					$posicion=$posicion+1;

				}
				?>
			</tbody>
		</table>
		<input class="btn btn-primary" type="submit" value="Guardar">
		</form>
	</div>
</div>
<br>
</body>
</html>