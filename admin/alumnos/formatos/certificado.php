<?php
require('fpdf/fpdf.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;


$id_alumno=$_GET['id'];
//$id_alumno=231;

$dia=strtoupper(utf8_encode($_GET['dia']));
$mes=strtoupper(utf8_encode($_GET['mes']));
$anio=strtoupper(utf8_encode($_GET['anio']));
$estudios=strtoupper(utf8_encode($_GET['est']));
/*
$dia="VEINIDOS";
$mes="SEPTIEMBRE";
$anio="TRECE";
*/


$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	$curp= strtoupper($row["curp"]);
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];    
	$plan = substr($plan, 17, 21);
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}   

class PDF extends FPDF
{
	
	function Header()
	{
		 //$this->Image('HP0154.jpg',0,0,216);		 
		
	}
	/*
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf=new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetFont('Arial','B',10); //Times, Helvetica, Arial, Courier

// TITULO Y ESCUELA
$pdf->SetXY(75,25);
$pdf->Cell(50,3,"CERTIFICADO DE TERMINACI".html_entity_decode("&Oacute;")."N DE ESTUDIOS",0,0,'L');

$pdf->SetXY(55,39); 
$pdf->Cell(50,3,"SECRETAR".html_entity_decode("&Iacute;")."A DE EDUCACI".html_entity_decode("&Oacute;"."N P".html_entity_decode("&Uacute;")."BLICA DEL ESTADO DE PUEBLA")."",0,0,'L');
$pdf->SetXY(30,44);
$pdf->Cell(50,3,"CAMINO REAL A CHOLULA S/N UNIDAD TERRITORIAL QUETZALCOATL, SAN JUAN B. CUAUTLANCINGO, PUEBLA",0,0,'L');
$pdf->SetXY(55,49);
$pdf->Cell(50,3,"ESCUELA NORMAL SUPERIOR FEDERALIZADA DEL ESTADO DE PUEBLA",0,0,'L');
$pdf->SetXY(105,59);
$pdf->Cell(50,3,"21DNL0006L",0,0,'L');

// NOMBRE
$pdf->SetXY(35,74);
$pdf->Cell(10,3,"".$nombre_completo."",0,0,'L');

// CURP
$pdf->SetXY(126,79);
$pdf->Cell(10,3,"".$curp."",0,0,'L');

// COMPLETOS/PARCIALES  (**********PEDIRLOS DESDE EL SISTEMA*******)
$pdf->SetXY(80,85);         
$pdf->Cell(10,3,$estudios,0,0,'L');

//ESPECIALIDAD
$pdf->SetFont('Arial','B',9);
$pdf->SetXY(110,85);
$pdf->Cell(50,4,"LICENCIADO EN EDUCACI".html_entity_decode("&Oacute;")."N SECUNDARIA",0,0,'L');
$pdf->SetXY(35,97);     
$pdf->Cell(50,4,"CON ESPECIALIDAD EN ".$nombreEspecialidad."",0,0,'L');
// MODALIDAD
$pdf->SetFont('Arial','B',10);
$pdf->SetXY(78,105);
$pdf->Cell(10,3,"ESCOLARIZADA",0,0,'L');

// PLAN
$pdf->SetXY(103,115);
$pdf->Cell(10,3,"".$plan."",0,0,'L');

// EXPEDIDO
$pdf->SetXY(50,145);
$pdf->Cell(50,4,"SAN JUAN B. CUAUTLANCINGO , PUEBLA",0,0,'L');


// FECHA
$pdf->SetXY(53,156);
$pdf->Cell(50,4,"".$dia."",0,0,'L');

$pdf->SetXY(130,156);
$pdf->Cell(50,4,"".$mes."",0,0,'L');

$pdf->SetXY(192,156);
$pdf->Cell(50,4,"".$anio."",0,0,'L');

$pdf->SetXY(140,175);
$pdf->Cell(50,1,"GERARDO PAUL ARVIZU SERAPIO",0,0,'L');

$pdf->SetXY(145,182);
$pdf->Cell(50,1,"DIRECTOR DE LA ESCUELA",0,0,'L');

$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();

//$pdf->SetXY(8,203);
//$pdf->SetX(8);
$pdf->SetY(208);
#----- LISTA DE MATERIAS SEM 1 -----# 
		$selectMateria = "SELECT * FROM materias WHERE id_semestre = '1' AND id_especialidad = '$idEspecialidad' AND estatus='1' ORDER BY clave ASC "; 
		$resultMat = mysql_query($selectMateria,$conexion);
		
		while($resultMat_=mysql_fetch_assoc($resultMat)){
			$id_materia=$resultMat_['id_materia'];
			$materia=($resultMat_['nombre']);
			
				
			
			$queryCalif = "SELECT AVG(calificacion),ciclo_escolar FROM calif_parc where id_alumno = '$id_alumno' and id_materia = '$id_materia' ";
				
				$resultCalif = mysql_query($queryCalif,$conexion);
				while($calif = mysql_fetch_assoc($resultCalif)){										
					$calificacion=substr($calif['AVG(calificacion)'],0,3);	
					$id_ciclo=$calif['ciclo_escolar'];
					
					if($calificacion==''){
						$calificacion=" / ";
                                        }else{
                                         $calificacion =  number_format($calificacion, 1, '.', '');        
                                         }
					
					$pdf->SetX(7);
					$tam=count(explode(" ", $materia));
					$mat_part=explode(" ", $materia);
					
					if($tam>7):
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(90,5,"".$mat_part[0]." ".$mat_part[1]." ".$mat_part[2]." ".$mat_part[3]." ".$mat_part[4]." ".$mat_part[5]." ".$mat_part[6]."",0,0,'L');
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,"".$calificacion."",0,0,'L');
					$pdf->Ln();
					$pdf->SetX(7);					
					$pdf->SetFont('Arial','',7);
						if($tam==8):
					$pdf->Cell(90,3,"".$mat_part[7]." ",0,0,'L');
						elseif($tam==9):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]."",0,0,'L');
						elseif($tam==10):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]."",0,0,'L');
						elseif($tam==11):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]." ".$mat_part[10]."",0,0,'L');
						elseif($tam==12):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]." ".$mat_part[10]." ".$mat_part[11]."",0,0,'L');
						elseif($tam==13):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]." ".$mat_part[10]." ".$mat_part[11]." ".$mat_part[12]."",0,0,'L');
						elseif($tam==14):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]." ".$mat_part[10]." ".$mat_part[11]." ".$mat_part[12]." ".$mat_part[13]."",0,0,'L');
						elseif($tam==15):
					$pdf->Cell(90,3,"".$mat_part[7]." ".$mat_part[8]." ".$mat_part[9]." ".$mat_part[10]." ".$mat_part[11]." ".$mat_part[12]." ".$mat_part[13]." ".$mat_part[14]."",0,0,'L');
					
						endif;
					$pdf->Ln();
					else:
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(90,5,"".$materia."",0,0,'L');
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,"".$calificacion."",0,0,'L');
					$pdf->Ln();
					endif;
					
						
					
				}
				$ciclo_semestre='';
				$c_e=mysql_query("SELECT ciclo_escolar FROM ciclo_escolar WHERE id_ciclo='$id_ciclo'",$conexion);
				if($c_e_=mysql_fetch_assoc($c_e)){
					$ciclo_semestre=$c_e_['ciclo_escolar'];
					}
				
						
		}	
		$pdf->SetFont('Arial','',8);		
                $pdf->SetXY(35,194);
		$pdf->Cell(50,4,"ASIGNATURAS ".$ciclo_semestre."",0,0,'L');
		$pdf->SetXY(30,202);
		$pdf->Cell(50,4,"PRIMER SEMESTRE ".$ciclo_semestre."",0,0,'L');
///////////////////////////////////////////////////////////////////////////////
$pdf->Ln();$pdf->Ln();
$pdf->SetY(208);
#----- LISTA DE MATERIAS SEM 2 -----# 
		$selectMateria1 = "SELECT * FROM materias WHERE id_semestre = '2' AND id_especialidad = '$idEspecialidad' AND estatus='1' ORDER BY clave ASC "; 
		$resultMat1 = mysql_query($selectMateria1,$conexion);
		
		while($resultMat_1=mysql_fetch_assoc($resultMat1)){
			$id_materia1=$resultMat_1['id_materia'];
			$materia1=($resultMat_1['nombre']);
			
				
			
			$queryCalif1 = "SELECT AVG(calificacion),ciclo_escolar FROM calif_parc where id_alumno = '$id_alumno' and id_materia = '$id_materia1' ";
				
				$resultCalif1 = mysql_query($queryCalif1,$conexion);
				while($calif1 = mysql_fetch_assoc($resultCalif1)){										
					$calificacion1=substr($calif1['AVG(calificacion)'],0,3);	
					$id_ciclo1=$calif1['ciclo_escolar'];
					
					if($calificacion1==''){
						$calificacion1=" / ";
                                        }else{
                                         $calificacion1 =  number_format($calificacion1, 1, '.', '');        
                                         }
					
					$pdf->SetX(112);
					$tam1=strlen($materia1);
					
					if($tam1>65):
					$p11=substr($materia1,0,65);
					$p21=substr($materia1,65,100);
					
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(93,5,"".$p11."",0,0,'L');
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,"".$calificacion1."",0,0,'L');
					$pdf->Ln();
					$pdf->SetX(7);
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(93,3,"".$p21."",0,0,'L');
					$pdf->Ln();
					else:
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(93,5,"".$materia1."",0,0,'L');
					$pdf->SetFont('Arial','',8);
					$pdf->Cell(10,5,"".$calificacion1."",0,0,'L');
					$pdf->Ln();
					endif;
					
						
					
				}
				$ciclo_semestre1='';
				$c_e1=mysql_query("SELECT ciclo_escolar FROM ciclo_escolar WHERE id_ciclo='$id_ciclo1'",$conexion);
				if($c_e_1=mysql_fetch_assoc($c_e1)){
					$ciclo_semestre1=$c_e_1['ciclo_escolar'];
					}
				
						
		}	
	
	$pdf->SetFont('Arial','',8);	
        $pdf->SetXY(137,194);
	$pdf->Cell(50,4,"ASIGNATURAS ".$ciclo_semestre."",0,0,'L');
	$pdf->SetXY(132,202);
	$pdf->Cell(50,4,"SEGUNDO SEMESTRE ".$ciclo_semestre1."",0,0,'L');
	
///////////////////////////////////////////////////////////////////////////////

/*

$pdf->SetXY(101,203);
	*/
///////////////////		

$pdf->Output("Certificado","I");

?>