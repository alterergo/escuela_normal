
<?php
include '../../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
if(empty($id_usuario)){ header("Location: ../../index.php"); }

?>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Baja Temporal</a></li>
		<li><a href="#tabs-2">Baja Definitiva</a></li>
	</ul>
    <div id="mensaje" class="mensajes" align="center"></div>
	<div id="tabs-1">
	<b>Temporal</b>
	<table width="700">
	<thead>
		<tr align="center">
			<th>Nombre</th>
			<th>Matricula</th>
			<th>Restaurar</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$consultaRows = "SELECT * FROM alumnos WHERE estatus = 'temporal' ORDER BY apellido_paterno ASC ";
		$respuestaRows= mysql_query($consultaRows,$conexion);
		
		$total=mysql_num_rows($respuestaRows);
		if($total<=0){ echo "<br /> Sin Resultados"; }
		
		while($row=mysql_fetch_array($respuestaRows)){
		?>
		<tr align="center">
			<td><?php echo $row["nombre"]." ".$row["apellido_paterno"]." ".$row["apellido_materno"]; ?></td>
			<td><?php echo $row["matricula"]; ?></td>
			<td><img src='../images/restaurar.png' onclick='bajaTemporal(<?php echo $row["id_alumno"]; ?>)' class='icono'></td>
			<br />
		</tr>
		<?php	
		}
		
		?>
	</tbody>
	</table>
	</div>
	<div id="tabs-2">
	<b>Definitiva</b>
	<table width="700">
	<thead>
		<tr align="center">
			<th>Nombre</th>
			<th>Matricula</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$consultaRows = "SELECT * FROM alumnos WHERE estatus = 'definitiva' ORDER BY apellido_paterno ASC ";
		$respuestaRow= mysql_query($consultaRows,$conexion);
		
		$totalRows = mysql_num_rows($respuestaRow);
		if($totalRows<=0){ echo "sin Resultados"; }
		
		while($row=mysql_fetch_array($respuestaRow)){
		?>
		<tr align="center">
			<td><?php echo $row["nombre"]." ".$row["apellido_paterno"]." ".$row["apellido_materno"]; ?></td>
			<td><?php echo $row["matricula"]; ?></td>
			<br />
		</tr>
		<?php
		}
		?>
	</tbody>
	</table>
	</div>
</div>


<script>
/************* FUNCION PARA TABS ******************/
$(function(){
	$( "#tabs" ).tabs();
});


/*********************************************************************/
/************* RESTAURAR ALUMNOS ELIMINADOS TEMPORALMENTE  ***********/
/*********************************************************************/
function bajaTemporal(id){

	var respuesta = confirm("¿Desea Restaurar al alumno?");
	if(respuesta){
	//alert(id);
	$.post("alumnos/restauraralumno.php",
	{idAlumno: id},
	function(data){ 
		if(data=1){
			//alert("Alumno Restaurado");
			$("#mensaje").html("Alumno Restaurado");
			setTimeout("location.href='./'", 2500);			
			//location.reload();
		}else{
			alert("Ocurrio un error. Intentelo nuevamente.");
		}	
	});
	}else{
		location.reload();
	}
}


</script>

