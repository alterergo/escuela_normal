<?

include '../../includes/conexion.php';
session_start();

if(empty($_POST["ciclo"])){
	$cicloEscolarPost = $_SESSION['cicloEscolar'];
}else{
	$cicloEscolarPost = $_POST["ciclo"];
	$_SESSION['cicloEscolar'] = $cicloEscolarPost;
}


//$ciclo_escolar_actual = 1;
//echo $ciclo_escolar_actual;
/*
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../index.php");
endif;
*/

//$cicloEscolarPost = $_POST["ciclo"];
	//$cicloEscolarPost = 1;


	/*
	*	Se obtiene el ciclo escolar
	*/
	$c_e_act = mysql_query("SELECT * FROM ciclo_escolar WHERE id_ciclo = '$cicloEscolarPost' ",$conexion);
	while($ci_es_ac = mysql_fetch_assoc($c_e_act)):
			$cicloEscolar = $ci_es_ac['ciclo_escolar'];
	endwhile;
	
	
	/*
	*  Alumnos Inscritos
	*/
	$alumnos = mysql_query(" SELECT * FROM ingreso WHERE ciclo_escolar = '$cicloEscolarPost' AND tipo_ingreso = 'nuevo' ",$conexion);
	$totalNuevos = mysql_num_rows($alumnos);
	
	/*
	* 	Alumnos REinscritos
	*/
	$alumnosReinscritos = mysql_query(" SELECT * FROM reinscripcion WHERE ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalReinscritos = mysql_num_rows($alumnosReinscritos);
	
	/*
	* 	Total de alummos del ciclo escolar
	*/
	$alumnosCiclo = mysql_query(" SELECT * FROM semestre_curso WHERE ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalCiclo = mysql_num_rows($alumnosCiclo);
	
	/*
	*	NUMERO DE ALUMNOS DE TRASLADO (ENTRADAS)
	*/
	$alumnosTraslado = mysql_query(" SELECT * FROM ingreso WHERE tipo_ingreso = 'traslado' AND ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalTraslado = mysql_num_rows($alumnosTraslado);
	
	/*
	*	NUMERO DE ALUMNOS DE TRASLADO (SALIDAS)
	*/
	$alumnosTraslado = mysql_query(" SELECT * FROM bajas WHERE motivo = 'traslado' AND ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalTrasladoBajas = mysql_num_rows($alumnosTraslado);
	
	/*
	* 	NÚMERO DE ALUMNOS DADOS DE BAJA(TEMPORALES)
	*/
	$bajaTemporal = mysql_query(" SELECT * FROM bajas WHERE tipo_baja = 'temporal' AND ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalBajaTemporal = mysql_num_rows($bajaTemporal);
	
	/*
	* 	NÚMERO DE ALUMNOS DADOS DE BAJA(DEFINITIVA)
	*/
	$bajaDefinitiva = mysql_query(" SELECT * FROM bajas WHERE tipo_baja = 'definitiva' AND ciclo_escolar = '$cicloEscolarPost' ",$conexion);
	$totalbajaDefinitiva = mysql_num_rows($bajaDefinitiva);

	
	
	/*
	*	Alumnos Regulares e Irregulares
	*/
	$regular = 0;
	$irregular = 0;
	$alumnosRegulares = mysql_query(" SELECT id_alumno FROM alumnos ",$conexion);
	while($resultAlumnos=mysql_fetch_assoc($alumnosRegulares)){
		$idAlumno = $resultAlumnos["id_alumno"];
		
			//Obtenemos id_materia 
		$calificacionParcial = mysql_query(" SELECT * FROM calif_parc WHERE id_alumno = '$idAlumno' AND ciclo_escolar = '$cicloEscolarPost' GROUP BY id_materia ",$conexion);
		while($resultParcial=mysql_fetch_assoc($calificacionParcial)){
		$idMateria = $resultParcial["id_materia"];
		
			$i = 0;
			$calificacionFinal = "";
			$promedioPorMateria = "";
			
			//Obtenemos Materias
			$calificacionMateria = mysql_query(" SELECT * FROM calif_parc WHERE id_alumno = '$idAlumno' AND ciclo_escolar = '$cicloEscolarPost' AND id_materia = '$idMateria' ",$conexion);
			while($resultMateria = mysql_fetch_assoc($calificacionMateria)){
				$calificacion = $resultMateria["calificacion"];
				$calificacionFinal = $calificacionFinal + $calificacion;
				$i++;
			} //W3
			
			$promedioPorMateria = $calificacionFinal/$i;			
			
			// Valida si el alumno tiene una o mas materias reprobadas
			$repro=0;
			if($promedioPorMateria<6){
				$repro=$repro+1;
			}
			
		}//W2
		
		// Valida si el alumno es Regular o Irregular
		if($repro!=0){
			$irregular++;
		}else{
			$regular++;
		}
	}//W1
	
	
	$porcentajes = array($totalNuevos,$totalReinscritos,$totalCiclo,$totalTraslado,$totalTrasladoBajas,$totalBajaTemporal,$totalbajaDefinitiva,$regular,$irregular);
	
	$titulo = array('NUM DE ALUMNOS INSCRITOS',
					'NUM DE ALUMNOS REINSCRITOS ',
					'TOTAL DE ALUMNOS DEL CICLO ESCOLAR',
					'NUM DE ALUMNOS DE TRASLADO(ENTRADAS)',
					'NUM DE ALUMNOS DE TRASLADOS(SALIDAS)',
					'NUM DE ALUMNOS DADOS DE BAJA(TEMPORALES)',
					'NUM DE ALUMNOS DADOS DE BAJA(DEFINITIVAS)',
					'NUM DE ALUMNOS REGULARES',
					'NUM DE ALUMNOS IRREGULARES'
					);

/*************** COMIENZA GRAFICA **********************/

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_bar.php');

// Callback function for Y-scale to get 1000 separator on labels
function separator1000($aVal) {
    return $aVal;
}

function separator1000_usd($aVal) {
    return '$'.number_format($aVal);
}


$datay=$porcentajes; 


// Create the graph and setup the basic parameters
$graph = new Graph(600,600,'auto');
$graph->img->SetMargin(80,30,30,220);
$graph->SetScale('textint');
$graph->SetShadow();
$graph->SetFrame(true); // No border around the graph

// Add some grace to the top so that the scale doesn't
// end exactly at the max value.
// The grace value is the percetage of additional scale
// value we add. Specifying 50 means that we add 50% of the
// max value
$graph->yaxis->scale->SetGrace(20);
$graph->yaxis->SetLabelFormatCallback('separator1000');

// Setup X-axis labels
$graph->xaxis->SetTickLabels($titulo);
$graph->xaxis->SetLabelAngle(90);
$graph->xaxis->SetFont(FF_FONT0);



// Setup graph title ands fonts
$tituloGrafica = utf8_decode(" Inscripción y Reinscripción ");
$graph->title->Set($tituloGrafica.' del Ciclo Escolar '.$cicloEscolar);
$graph->title->SetFont(FF_FONT2,FS_BOLD);
$graph->xaxis->title->Set('');//Ciclo escolar XXXX-XXXX
$graph->xaxis->title->SetFont(FF_FONT2,FS_BOLD);



// Create a bar pot
$bplot = new BarPlot($datay);
$bplot->SetFillColor('orange');
$bplot->SetWidth(0.5);
$bplot->SetShadow();



///// numeros arriba de las barras ///////
// Setup the values that are displayed on top of each bar
$bplot->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
$bplot->value->SetFont(FF_ARIAL,FS_BOLD);
$bplot->value->SetAngle(45);
$bplot->value->SetFormatCallback('separator1000');

// Black color for positive values and darkred for negative values
$bplot->value->SetColor('black','darkred');
$graph->Add($bplot);

// Finally stroke the graph
$graph->Stroke();

?>

