<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../index.php");
endif;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gráficas</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>

</head>
<body onload="grafias()">
<div id="contenedorAdmin" class="row">

<div id="headerAdmin" class="grid_11">
	<?php include 'menu.php'; ?>
</div>
<div id="contenido" class="grid_11" >
<br/><br/>
<div id="myDiv" class="tooltip" align="center"></div>
<table id="tablaGraficas" cellpadding="15" class="table_cons">
    <thead>
		<th>#</th>
        <th>Grafico</th>
        <th>Acciones</th>
    </thead>
<tbody>

	<tr>
		<td>1</td>
		<td>Porcentaje de bajas</td>
		<td><img src="../images/grafica.png" title="Gráfica" class="icono" onclick="graficar(1)" /></td>
	</tr>
	
	<tr>
		<td>2</td>
		<td>Inscripcion y reinscripción</td>
		<td><img src="../images/grafica.png" title="Gráfica" class="icono" onclick="graficar(2)" /></td>
	</tr>
	
	<tr>
		<td>3</td>
		<td>Promedio de aprovechamiento</td>
		<td><img src="../images/grafica.png" title="Gráfica" class="icono" onclick="graficar(3)" /></td>
	</tr>
	
	<tr>
		<td>4</td>
		<td>Porcentaje de aliumnos reprobados</td>
		<td><img src="../images/grafica.png" title="Gráfica" class="icono" onclick="graficar(4)" /></td>
	</tr>
	
	<tr>
		<td>5</td>
		<td>Promedio de asistencia</td>
		<td><img src="../images/grafica.png" title="Gráfica" class="icono" onclick="graficar(5)" /></td>
	</tr>
	
</tbody>
</table>

</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->

<!-- PORCENTAJE DE BAJAS -->
<div id="dialog-porcentajeBajas" title="Ciclo Escolar">
	<form>
		<fieldset>
			<legend>Seleccionar Ciclo Escolar</legend>
			<select id="cicloEscolar" name="cicloEscolar">
				<?php
				$obtenerCiclos = " SELECT * FROM ciclo_escolar ";
				$result = mysql_query($obtenerCiclos,$conexion);
				while($row = mysql_fetch_assoc($result)){ ?>
				
				<option value="<?php echo $row["id_ciclo"];?>"> <?php echo $row["ciclo_escolar"]; ?> </option>
				
				<?php } ?>
			</select>
		</fieldset>
		<div id="contenedor"></div>
		
	</form>
</div>

<!-- PORCENTAJE DE INSCRIPCION Y REINSCRIPCOIN -->
<div id="dialog-porcentajeInscripcion" title="Inscripción y Reinscripción">
	<form>
		<fieldset>
			<legend>Seleccionar Ciclo Escolar</legend>
			<select id="cicloEscolarReinsc" name="cicloEscolarReinsc">
				<?php
				$obtenerCiclos = " SELECT * FROM ciclo_escolar ";
				$result = mysql_query($obtenerCiclos,$conexion);
					while($row = mysql_fetch_assoc($result)){ 
				?>					
					<option value="<?php echo $row["id_ciclo"];?>"> <?php echo $row["ciclo_escolar"]; ?> </option>
				<?php } ?>
			</select>
		</fieldset>
		<div id="cont"></div>
		
	</form>
</div>

<!-- PORCENTAJE DE APROVECHAMIENTO -->
<div id="dialog-porcentajeAprovechamiento" title="Aprovechamiento">
	<form>
		<fieldset>
			<legend>Seleccionar Ciclo Escolar</legend>
			<label> Ciclo Escolar: </label><br/>
			<select id="cicloEscolarReinsc" name="cicloEscolarReinsc">
				<?php
				$obtenerCiclos = " SELECT * FROM ciclo_escolar ";
				$result = mysql_query($obtenerCiclos,$conexion);
					while($row = mysql_fetch_assoc($result)){ 
				?>					
					<option value="<?php echo $row["id_ciclo"];?>"> <?php echo $row["ciclo_escolar"]; ?> </option>
				<?php } ?>
			</select>
			<br/><br/>
			<label> Plan de Estudios: </label><br/>
			<select id="planEstudios" name="planEstudios">
				<?php
				$obtenerPlan = " SELECT * FROM plan_estudios ";
				$result = mysql_query($obtenerPlan,$conexion);
					while($resultPlan = mysql_fetch_assoc($result)){
				?>					
					<option value="<?php echo $resultPlan["id_plan"];?>"> <?php echo $resultPlan["plan_estudios"]; ?> </option>
				<?php } ?>
			</select>
		</fieldset>
		<div id="cont"></div>
		
	</form>
</div>

<!-- PORCENTAJE DE REPROBADOS -->
<div id="dialog-porcentajeReprobados" title="% de Reprobados">
	<form>
		<fieldset>
			<legend>Seleccionar Ciclo Escolar</legend>
			<label> Ciclo Escolar: </label><br/>
			<select id="cicloEscolarRepro" name="cicloEscolarRepro">
				<?php
				$obtenerCiclos = " SELECT * FROM ciclo_escolar ";
				$result = mysql_query($obtenerCiclos,$conexion);
					while($row = mysql_fetch_assoc($result)){ 
				?>					
					<option value="<?php echo $row["id_ciclo"];?>"> <?php echo $row["ciclo_escolar"]; ?> </option>
				<?php } ?>
			</select>
			<br/><br/>
			<label> Plan de Estudios: </label><br/>
			<select id="planEstudiosRepro" name="planEstudiosRepro">
				<?php
				$obtenerPlan = " SELECT * FROM plan_estudios ";
				$result = mysql_query($obtenerPlan,$conexion);
					while($resultPlan = mysql_fetch_assoc($result)){
				?>					
					<option value="<?php echo $resultPlan["id_plan"];?>"> <?php echo $resultPlan["plan_estudios"]; ?> </option>
				<?php } ?>
			</select>
		</fieldset>
		<div id="contRepro"></div>
		
	</form>
</div>

<!-- PORCENTAJE DE ASISTENCIAS -->
<div id="dialog-porcentajeAsistencia" title="% de Asistencia">
	<form>
		<fieldset>
			<legend>Seleccionar Ciclo Escolar</legend>
			<label> Ciclo Escolar: </label><br/>
			<select id="cicloEscolarAsist" name="cicloEscolarAsist">
				<?php
				$obtenerCiclos = " SELECT * FROM ciclo_escolar ";
				$result = mysql_query($obtenerCiclos,$conexion);
					while($row = mysql_fetch_assoc($result)){ 
				?>					
					<option value="<?php echo $row["id_ciclo"];?>"> <?php echo $row["ciclo_escolar"]; ?> </option>
				<?php } ?>
			</select>
			<br/><br/>
			<label> Plan de Estudios: </label><br/>
			<select id="planEstudiosAsist" name="planEstudiosAsist">
				<?php
				$obtenerPlan = " SELECT * FROM plan_estudios ";
				$result = mysql_query($obtenerPlan,$conexion);
					while($resultPlan = mysql_fetch_assoc($result)){
				?>					
					<option value="<?php echo $resultPlan["id_plan"];?>"> <?php echo $resultPlan["plan_estudios"]; ?> </option>
				<?php } ?>
			</select>
		</fieldset>
		<div id="contAsist"></div>
		
	</form>
</div>

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/graficas.js"></script>


</body>
</html>