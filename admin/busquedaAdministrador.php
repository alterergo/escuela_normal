<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
if(empty($id_usuario)){ header("Location: ../index.php"); }
//echo $id_usuario;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Alumnos</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>
</head>
<body onload="alumnos();">
<div id="contenedorAdmin" class="row">

<div id="contenido" class="grid_11">

<div id="myDiv"></div>
<img src="../images/deshacer.png" class="icono" title="Deshacer" onClick="javascript:location.reload()">
<table cellpadding="15" class="table_cons">
    <thead>
		<tr>
		<th>#</th>
        <th>Nombre</th>
        <th>Tipo</th>
        <th>Correo</th>
        <th>Acciones</th>
		</tr>
    </thead>
<tbody>
<?php 
	
	require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#	
	/*if(empty($_POST["estatus"])){
		$estatus = "activo";
	}else{
		$estatus = $_POST["estatus"];
	}*/
	$query = "Select * from usuarios WHERE estatus= '".$estatus."' ORDER BY id_usuario DESC";
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = $_GET['page'];
	$cantidad = 10;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#
	
	$i = 1;
	#--- EN LA CONSULTA DEBE IR EL LIMITE DE RESULTADOS PARA MOSTRAR ---#
	
	$q=$_POST["q"];
	$estatus = $_SESSION["estatusAdmin"];
	
	$consulta = "Select * from usuarios WHERE estatus='".$estatus."' AND (tipo_usuario = '1' OR tipo_usuario = '5') AND nombre LIKE '%".$q."%' 
	OR estatus='".$estatus."' AND (tipo_usuario = '1' OR tipo_usuario = '5') AND apellido_pat LIKE '%".$q."%' 
	OR estatus='".$estatus."' AND (tipo_usuario = '1' OR tipo_usuario = '5') AND apellido_mat LIKE '%".$q."%' 
	OR estatus='".$estatus."' AND (tipo_usuario = '1' OR tipo_usuario = '5') AND user LIKE '%".$q."%' ORDER BY id_usuario DESC  LIMIT $desde, $cantidad";
	//$consulta = "Select * from alumnos WHERE estatus='activo' ORDER BY id_alumno DESC LIMIT $desde, $cantidad ";
	$respuesta = mysql_query($consulta,$conexion);
	$totalRows = mysql_num_rows($respuesta);
	if($totalRows>=1){
	while($row = mysql_fetch_array($respuesta)){
	echo "<tr>";
	echo "<td>".$i."</td>";
	echo "<td>".utf8_encode($row["nombre"].' '.$row["apellido_pat"].' '.$row["apellido_mat"])."</td>";
	echo "<td>".$row["user"]."</td>";
	echo "<td>".$row["correo"]."</td>";
	echo "<td>";
	if($estatus=="activo"){
		/***************** OPCIONES PARA ALUMNOS ACTIVOS *************************/
		echo "
		<img src='../images/user_change.png' title='Editar' onclick='editarUsuario(".$row['id_usuario'].")' class='icono'>
		<img src='../images/delete.png' title='Eliminar' onclick='eliminarAdmin(".$row['id_usuario'].")' class='icono'>
		";
	}elseif($estatus=="temporal"){
		/***************** OPCIONES PARA ALUMNOS CON BAJA TEMPORAL *************************/
		echo "<img src='../images/restaurar.png' title='Restaurar' onclick='restaurarUser(". $row["id_usuario"].")' class='icono'>";
	}
	echo "</td>";
	echo "</tr>";
	$i++;
	}
	}else{
		echo "Sin Resultados";
	}
?>
</tbody>
</table>
<?php
#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div id='paginate' class='paginacion'>";
	$url = "busquedaAlumno.php?";
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#--------------------------------------------------------------------#
?>
</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
</body>
</html>