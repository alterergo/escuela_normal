<?php
include '../../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
if(empty($id_usuario)){ header("Location: ../../index.php"); }

$idAlumno=$_POST["idAlumno"];
//echo $idAlumno;

?>
<script>

function asignarValor(valor){
	//alert(valor);
	document.getElementById("tipoBaja").value = valor;

	var tem=document.getElementById("temp");
	var def=document.getElementById("defi");
	
	if(valor=="temporal"){			
		def.style.display="none";
		tem.style.display="";
		}
	else { //if (valor=="definitiva")
		def.style.display="";
		tem.style.display="none";
		}
	

}

function asignarMotivo(motivo){
	//alert(motivo);
	document.getElementById("motivoDef").value = motivo;
	}

</script>
<form>
<fieldset>
<legend>Baja del alumno</legend>
<div class="mensajes" id="mensaje" align="center"></div>
<label>Tipo de baja</label>
<br />
<input type="radio" id="baja" name="baja" value="temporal" onClick="asignarValor(this.value);">Temporal <br />
<div id="temp" style="display:none">Motivo:
	<input type="text" id="motivo_temp" name="motivo_temp" />
</div>

<input type="radio" id="baja" name="baja" value="definitiva" onClick="asignarValor(this.value);">Definitiva<br />
<div id="defi" style="display:none">Motivo:<br />
<ul>	Materias Reprobadas<input type="radio" id="motivo_def" name="motivo_def" value="materias" onClick="asignarMotivo(this.value);"/></ul>
<ul>    Deserción<input type="radio" id="motivo_def" name="motivo_def" value="deserta" onClick="asignarMotivo(this.value);"/></ul>
<ul>    Traslado<input type="radio" id="motivo_def" name="motivo_def" value="traslado" onClick="asignarMotivo(this.value);"/></ul>
</div>
<input type="hidden" id="tipoBaja" name="tipoBaja" />
<input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $idAlumno; ?>" />
<input type="hidden" id="motivoDef" name="motivoDef" />

</fieldset>
</form>