<?php
require('fpdf/fpdf.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$id_alumno=$_GET['id'];
$semestre_get=$_GET['S'];
//$fecha=date("d-m-Y");	
//$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	$curp=$row['curp'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	/*
	function Header()
	{		 
		 
		 //$this->Image('kardex_espa.jpg',0,0,216);
		 //$this->Image('kardex_espa_atras.jpg',0,0,216);
		 
		 //$this->Image('kardex_mate.jpg',0,0,216);
		 //$this->Image('kardex_mate_atras.jpg',0,0,216);
		 
		 //$this->Image('kardex_ingles.jpg',0,0,216);
		 //$this->Image('kardex_ingles_atras.jpg',0,0,216); 
		 //$this->Image('kardex_hist.jpg',0,0,216);
		 //$this->Image('kardex_hist_atras.jpg',0,0,216);
		 //$this->Image('kardex_tele.jpg',0,0,216);
		 //$this->Image('kardex_tele_atras.jpg',0,0,216);
		
	}
	
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf=new PDF('P','cm','Legal');
$pdf->AddPage();
$pdf->SetFont('Arial','',10); //Times, Helvetica, Arial, Courier

//$pdf->SetXY(0.1,0.1);
//$pdf->Cell(21.4,33,"".$idEspecialidad."",1,0,'L');

#----- LISTA DE MATERIAS -----# 
		$selectMateria = "SELECT * FROM materias WHERE id_semestre = '$semestre_get' AND id_especialidad = '$idEspecialidad' AND estatus='1' ORDER BY clave ASC "; 
		$resultMat = mysql_query($selectMateria,$conexion);
		
		while($resultMat_=mysql_fetch_assoc($resultMat)){
			$id_materia=$resultMat_['id_materia'];

			$queryCalif = "SELECT AVG(calificacion) FROM calif_parc where id_alumno = '$id_alumno' and id_materia = '$id_materia' ";
				
				$resultCalif = mysql_query($queryCalif,$conexion);
				while($calif = mysql_fetch_assoc($resultCalif)){										
					$calificacion[]=substr($calif['AVG(calificacion)'],0,3);	
				}
			
			// HORAS DEL CURSO							
				$sel=mysql_query("SELECT SUM(total_horas) AS tot_horas FROM horas_clase WHERE id_materia='$id_materia' ",$conexion);
							$cont_asis='';
							while($cons=mysql_fetch_assoc($sel)):
								$total_horas_clase=$cons['tot_horas'];
									
					#--- consulta de los porcentajes de ASISTENCIA
					$queryPorcenaje = " SELECT porcentaje, SUM(asistencia) AS asisten FROM pase_lista WHERE id_alumno = '$id_alumno' AND id_materia = '$id_materia'  ";					
					$resultQuery = mysql_query($queryPorcenaje,$conexion);						
						
						while($row = mysql_fetch_array($resultQuery)){
							$asist=$row["asisten"];
							$porcentaje=$row["porcentaje"];
							
							if($asist!=0):
								$porcen_asis[]=substr((($asist*100)/$total_horas_clase),0,3);																
							else:
								$porcen_asis[]=$porcentaje;
							endif;
							
							}
							endwhile;
		}
////////////////////////////// MATEMATICAS //////////////////////////////
if($idEspecialidad==5):
	
	//// 1 SEMESTRE ////
	if($semestre_get==1):
	
	// 101
	$pdf->SetXY(8.6,8.1);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,8.1);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,8.6);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,8.6);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,9.2);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.2);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,9.7);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.7);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,10.3);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.3);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,10.9);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.9);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 2 SEMESTRE
	elseif($semestre_get==2):
	//107
	$pdf->SetXY(8.6,13.5);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,13.5);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//108
	$pdf->SetXY(8.6,14);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,14);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,14.6);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,14.6);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,15.2);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,15.2);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,15.7);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,15.7);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,16.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,16.9);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.9);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 3 SEMESTRE
	elseif($semestre_get==3):
	//201
	$pdf->SetXY(8.6,19.6);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,19.6);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,20.1);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,20.1);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//203
	$pdf->SetXY(8.6,20.7);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,20.7);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//204
	$pdf->SetXY(8.6,21.2);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,21.2);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//205
	$pdf->SetXY(8.6,21.8);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,21.8);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//206
	$pdf->SetXY(8.6,22.4);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,22.4);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//207
	$pdf->SetXY(8.6,23);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,23);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 4 SEMESTRE
	elseif($semestre_get==4):
	//208
	$pdf->SetXY(8.6,25.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,25.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//209
	$pdf->SetXY(8.6,26.2);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,26.2);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//210
	$pdf->SetXY(8.6,26.8);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,26.8);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//211
	$pdf->SetXY(8.6,27.4);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,27.4);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//212
	$pdf->SetXY(8.6,28);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,28);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//213
	$pdf->SetXY(8.6,28.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,28.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//214
	$pdf->SetXY(8.6,29.1);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,29.1);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	endif;
	
	
	
	////////////////////////////// TELESECUNDARIA //////////////////////////////
	elseif($idEspecialidad==7):
	
	//// 1 SEMESTRE ////
	if($semestre_get==1):
	
	// 101
	$pdf->SetXY(8.6,8.3);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,8.3);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,8.9);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,8.9);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,9.4);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,9.4);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,10);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,10);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,10.6);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,10.6);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,11.1);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,11.1);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 2 SEMESTRE
	elseif($semestre_get==2):
	//107
	$pdf->SetXY(8.6,13.8);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,13.8);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//108
	$pdf->SetXY(8.6,14.4);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,14.4);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,14.9);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,14.9);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,15.5);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,15.5);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,16.1);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.1);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,16.6);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,16.6);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,17.2);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,17.2);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 3 SEMESTRE
	elseif($semestre_get==3):
	//201
	$pdf->SetXY(8.6,19.9);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,19.9);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,20.4);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,20.4);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//203
	$pdf->SetXY(8.6,21);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,21);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//204
	$pdf->SetXY(8.6,21.6);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,21.6);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//205
	$pdf->SetXY(8.6,22.1);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,22.1);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//206
	$pdf->SetXY(8.6,22.7);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,22.7);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//207
	$pdf->SetXY(8.6,23.3);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,23.3);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 4 SEMESTRE
	elseif($semestre_get==4):
	//208
	$pdf->SetXY(8.6,26);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.8,26);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//209
	$pdf->SetXY(8.6,26.5);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.8,26.5);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//210
	$pdf->SetXY(8.6,27.1);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.8,27.1);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//211
	$pdf->SetXY(8.6,27.7);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.8,27.7);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//212
	$pdf->SetXY(8.6,28.2);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.8,28.2);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//213
	$pdf->SetXY(8.6,28.8);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.8,28.8);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//214
	$pdf->SetXY(8.6,29.4);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.8,29.4);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	endif;
	
	
	////////////////////////////// ESPAÑOL //////////////////////////////
	elseif($idEspecialidad==6):
	
	if($semestre_get==1):
	//// 1 SEMESTRE ////
	
	// 101
	$pdf->SetXY(8.9,8.4);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(10.1,8.4);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.9,9);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(10.1,9);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.9,9.5);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(10.1,9.5);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.9,10.1);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(10.1,10.1);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.9,10.6);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(10.1,10.6);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.9,11.2);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(10.1,11.2);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 2 SEMESTRE
	elseif($semestre_get==2):
	//107
	$pdf->SetXY(8.9,13.8);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(10.1,13.8);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//108
	$pdf->SetXY(8.9,14.4);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(10.1,14.4);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.9,14.9);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(10.1,14.9);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.9,15.5);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(10.1,15.5);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.9,16.1);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(10.1,16.1);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.9,16.6);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(10.1,16.6);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.9,17.2);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(10.1,17.2);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 3 SEMESTRE
	elseif($semestre_get==3):
	//201
	$pdf->SetXY(8.9,19.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(10.1,19.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.9,20.3);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(10.1,20.3);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//203
	$pdf->SetXY(8.9,20.8);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(10.1,20.8);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//204
	$pdf->SetXY(8.9,21.4);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(10.1,21.4);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//205
	$pdf->SetXY(8.9,21.9);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(10.1,21.9);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//206
	$pdf->SetXY(8.9,22.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(10.1,22.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//207
	$pdf->SetXY(8.9,23.1);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(10.1,23.1);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 4 SEMESTRE
	elseif($semestre_get==4):
	//208
	$pdf->SetXY(8.9,25.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(10.1,25.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//209
	$pdf->SetXY(8.9,26.2);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(10.1,26.2);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//210
	$pdf->SetXY(8.9,26.8);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(10.1,26.8);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//211
	$pdf->SetXY(8.9,27.4);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(10.1,27.4);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//212
	$pdf->SetXY(8.9,27.9);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(10.1,27.9);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//213
	$pdf->SetXY(8.9,28.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(10.1,28.5);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	//214
	$pdf->SetXY(8.9,29.1);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(10.1,29.1);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	endif;
	
	
	////////////////////////////// INGLES E HISTORIA //////////////////////////////
	elseif($idEspecialidad==8 || $idEspecialidad==9):
	
	//// 1 SEMESTRE ////
	if($semestre_get==1):
	
	// 101
	$pdf->SetXY(8.6,8.4);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.7,8.4);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	// 102
	$pdf->SetXY(8.6,9);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.7,9);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	// 103
	$pdf->SetXY(8.6,9.6);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.7,9.6);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	// 104
	$pdf->SetXY(8.6,10.1);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.7,10.1);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	// 105
	$pdf->SetXY(8.6,10.7);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.7,10.7);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	// 106
	$pdf->SetXY(8.6,11.3);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.7,11.3);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	
	// 2 SEMESTRE
	elseif($semestre_get==2):
	//107
	$pdf->SetXY(8.6,13.8);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.7,13.8);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//108
	$pdf->SetXY(8.6,14.4);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.7,14.4);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//109
	$pdf->SetXY(8.6,14.9);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.7,14.9);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//110
	$pdf->SetXY(8.6,15.5);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.7,15.5);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//111
	$pdf->SetXY(8.6,16.1);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.7,16.1);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//112
	$pdf->SetXY(8.6,16.6);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.7,16.6);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//113
	$pdf->SetXY(8.6,17.2);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.7,17.2);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 3 SEMESTRE
	elseif($semestre_get==3):
	//201
	$pdf->SetXY(8.6,19.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.7,19.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//202
	$pdf->SetXY(8.6,20.3);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.7,20.3);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//203
	$pdf->SetXY(8.6,20.9);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.7,20.9);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//204
	$pdf->SetXY(8.6,21.4);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.7,21.4);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//205
	$pdf->SetXY(8.6,22);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.7,22);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//206
	$pdf->SetXY(8.6,22.6);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.7,22.6);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//207
	$pdf->SetXY(8.6,23.2);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.7,23.2);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	
	// 4 SEMESTRE
	elseif($semestre_get==4):
	//208
	$pdf->SetXY(8.6,25.7);
	$pdf->Cell(50,3,"".$calificacion[0]."",0,0,'L');
	
	$pdf->SetXY(9.7,25.7);
	$pdf->Cell(50,3,"".$porcen_asis[0]."",0,0,'L');
	
	//209
	$pdf->SetXY(8.6,26.3);
	$pdf->Cell(50,3,"".$calificacion[1]."",0,0,'L');
	
	$pdf->SetXY(9.7,26.3);
	$pdf->Cell(50,3,"".$porcen_asis[1]."",0,0,'L');
	
	//210
	$pdf->SetXY(8.6,26.8);
	$pdf->Cell(50,3,"".$calificacion[2]."",0,0,'L');
	
	$pdf->SetXY(9.7,26.8);
	$pdf->Cell(50,3,"".$porcen_asis[2]."",0,0,'L');
	
	//211
	$pdf->SetXY(8.6,27.4);
	$pdf->Cell(50,3,"".$calificacion[3]."",0,0,'L');
	
	$pdf->SetXY(9.7,27.4);
	$pdf->Cell(50,3,"".$porcen_asis[3]."",0,0,'L');
	
	//212
	$pdf->SetXY(8.6,27.9);
	$pdf->Cell(50,3,"".$calificacion[4]."",0,0,'L');
	
	$pdf->SetXY(9.7,27.9);
	$pdf->Cell(50,3,"".$porcen_asis[4]."",0,0,'L');
	
	//213
	$pdf->SetXY(8.6,28.5);
	$pdf->Cell(50,3,"".$calificacion[5]."",0,0,'L');
	
	$pdf->SetXY(9.7,28.5);
	$pdf->Cell(50,3,"".$porcen_asis[5]."",0,0,'L');
	
	//214
	$pdf->SetXY(8.6,29.1);
	$pdf->Cell(50,3,"".$calificacion[6]."",0,0,'L');
	
	$pdf->SetXY(9.7,29.1);
	$pdf->Cell(50,3,"".$porcen_asis[6]."",0,0,'L');
	
	endif;


endif;

		//}//while materias

$pdf->Output("Kardex Oficial","I");

?>