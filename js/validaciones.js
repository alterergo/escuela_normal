$(function(){ /* inicia funcion */
	
//Placeholder en todos los navegadores
$('input[placeholder]').placeholder();

/**************************************************************************/
/*FUNCION PARA VALIDAR Y REDIRECCIONAR DE PAGINA SEGUN EL TIPO DE USUARIO */
/**************************************************************************/
$("#enviar").click(function(){
	var user = $("#user").val();
	var pass= $("#pass").val();
		
	if(user==""){
		$("#user").focus();
		$("#respuestaAjax").html("El campo de usuario es requerido");
		return;
	}
	
	if(pass==""){
		$("#pass").focus();
      	$("#respuestaAjax").html("El campo de contraseña es requerido");
		return;
	}
		
	$.post('includes/validar_usuario.php',
		{ user: user, pass: pass }, 
		function(data){
			if(data=="no"){
				$("#respuestaAjax").html("Datos Incorrectos/Usuario dado de baja");
			}else if(data[0]==5){
				document.location.href="admin/";				
			}else if(data[0]==1){ 
				//alert("usuario Administrativo");
				document.location.href="admin/";
			}else if(data[0]==2){
				//alert("Docente");
				document.location.href="docente/";
			}else if(data[0]==3){
				//alert("Docente");
				document.location.href="docentes_adm/";
				//alert("doc");
			}else if(data[0]==4){
				document.location.href="admin/";
			}
			//$("#respuesta").html(data);
		});
});



}); /* fin de funcion */