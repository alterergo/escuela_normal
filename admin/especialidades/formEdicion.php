<?php
include '../../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
if(empty($id_usuario)){ header("Location: ../../index.php"); }

$idEsp = $_POST["idEsp"];

$selecEsp = "SELECT * FROM catalogoespecialidades WHERE idEspecialidad = $idEsp";

$respuesta = mysql_query($selecEsp,$conexion);

while($row = mysql_fetch_array($respuesta)){
	$especialidad = $row["nombreEspecialidad"];
	$grupo = $row["grupos"];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Especialidades</title>
</head>
<body>
<form>
<fieldset>
<legend>Editar</legend>
<div class="grid_5">
	<label>Especialidad Actual: <b><?php echo utf8_encode($especialidad); ?></b></label><br />
	<label for="nomEditar">Nuevo Nombre</label>
	<input type="text" id="nomEditar" name="nomEditar" class='textinf mayusculas' style='width:270px; text-transform:uppercase; '/> 
	<input type="hidden" id="idEsp" name="idEsp" value="<?php echo $idEsp; ?>"/>
	<input type="hidden" id="nombreOrg" name="nombreOrg" value="<?php echo utf8_encode($especialidad); ?>"/>
	<div id="errNomEdt" class="error"></div>
</div>
<br/>
<div class="grid_5">
	<label>Especialidad con grupos: </label><br/>
	 <?php if ($grupo==1) { ?>
	 	<input type="radio" id="grupoEspecialidadEdt" name="grupoEspecialidadEdt" value="1" checked> SI 
     	<input type="radio" id="grupoEspecialidadEdt" name="grupoEspecialidadEdt" value="0"> NO
	 <?php
	 } else {
	 ?>
	 	<input type="radio" id="grupoEspecialidadEdt" name="grupoEspecialidadEdt" value="1"> SI 
     	<input type="radio" id="grupoEspecialidadEdt" name="grupoEspecialidadEdt" value="0" checked> NO
	 <?php 
	 }
	 ?> 
	
</div>
</fieldset>
<div id="mensajes" class="mensajes"></div>
</form>
</body>
</html>