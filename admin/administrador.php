<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;

if(empty($id_usuario)):
	header("Location: ../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../index.php");
endif;

if(empty($_POST["estatus"])){
	$_SESSION["estatusAdmin"]="activo";
}else{
	$_SESSION["estatusAdmin"]=$_POST["estatus"];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrador</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>
</head>
<body onload="adminstrativo();">
<div id="contenedorAdministrador" class="row">
<div id="headerAdmin" class="grid_11">
	<?php include 'menu.php'; ?>
</div>

<div id="contenido" class="grid_11">
<div class="grid_3" id="nuevoAdministrativo" style="cursor:pointer">
	<img src="../images/add.png" class="icono" ><span style="color:#0066CC;">Nuevo Administrativo</span>
</div>

<div class="margen_5 search" id="buscador">
	<div id="contEstatus" class="contEstatus">
		<form method="POST" name="enviarEstatus">
		<label>Estatus</label>
		<select class="ui-widget-content" id="estatus" name="estatus" onChange="document.enviarEstatus.submit()">
			<option value="activo" <?php if($_POST["estatus"] == "activo"){ ?> selected="selected" <?php }?> >Activos</option>
			<option value="temporal" <?php if($_POST["estatus"] == "temporal"){ ?> selected="selected" <?php }?> >Baja Temporal</option>
		</select>
		</form>
	</div>
		
	<div id="contBusqueda" class="contBusqueda">
		Buscar <input type="text" id="searchAdministrador" name="searchAdministrador" onKeyUp="buscarAdministrativo()" placeholder="Administrativo" class="ui-widget-content " /> 
		<img src="../images/lupa.png" id="lupa" class="lupa">
	</div>
</div>

<div id="myDiv" class="tooltip"></div>
<table id="tablaAdministrador" cellpadding="10" class="table_cons">
    <thead>
		<th>#</th>
        <th>Nombre</th>
        <th>Tipo</th>
        <th>Correo</th>
        <th>Acciones</th>
    </thead>
<tbody>
<?php 
	require("../includes/paginacion.php");
	#----------------------------- PAGINACION ----------------------------#	
	if(empty($_POST["estatus"])){
		$estatus = "activo";
	}else{
		$estatus = $_POST["estatus"];
	}
	$query = "Select * from usuarios WHERE (tipo_usuario = '1' OR tipo_usuario = '5') AND estatus= '".$estatus."' ORDER BY id_usuario DESC";
	//$query = "Select * from usuarios WHERE tipo_usuario = 1 OR tipo_usuario = 0";
	$rsT =  mysql_query($query, $conexion);
	$total = mysql_num_rows($rsT);
	$pg = $_GET['page'];
	$cantidad = 10;
	$paginacion = new paginacion($cantidad, $pg);
	$desde = $paginacion->getFrom();
	#----------------------------------------------------------------------#
	
	$i = 1;
	#--- EN LA CONSULTA DEBE IR EL LIMITE DE RESULTADOS PARA MOSTRAR ---#
	
	/*$q=$_POST["q"];
	if(empty($q)){
		$consulta = "Select * from alumnos WHERE estatus='".$estatus."' ORDER BY apellido_paterno ASC LIMIT $desde, $cantidad ";
		
	}else{
		$consulta = "Select * from alumnos WHERE estatus='".$estatus."' AND nombre LIKE '%".$q."%' ORDER BY apellido_paterno ASC ";
		
	}*/
	$consulta = " Select * from usuarios WHERE estatus = '".$estatus."' AND (tipo_usuario = '1' OR tipo_usuario = '5' OR tipo_usuario = '4' OR tipo_usuario = '3' )  ORDER BY id_usuario DESC LIMIT $desde, $cantidad ";
	$respuesta = mysql_query($consulta,$conexion);
	while($row = mysql_fetch_array($respuesta)){
		$tipo_usr=$row['tipo_usuario']; 
			if ($tipo_usr==5):
				$tipo_usr_letra="Director";
			elseif ($tipo_usr==1):
				$tipo_usr_letra="Administrativo";
			elseif ($tipo_usr==4):
				$tipo_usr_letra="Control Escolar";
			elseif ($tipo_usr==3):
				$tipo_usr_letra="Administrador Docentes";
			endif;
		echo "<tr>";
		echo "<td>".$i."</td>";
		echo "<td>".$row["nombre"]." ".($row["apellido_pat"])." ".($row["apellido_mat"])."</td>";
		echo "<td>".$tipo_usr_letra."</td>";
		echo "<td>".$row["correo"]."</td>";
		echo "<td>";
		if($estatus=='activo'){
		echo "<img src='../images/user_change.png' title='Editar' onclick='editarUsuario(".$row['id_usuario'].")' class='icono'>";
			if($id_usuario == $row["id_usuario"]){
				echo "";
			}else{
				echo "<img src='../images/delete.png' title='Baja Temporal' onclick='eliminarAdmin(".$row['id_usuario'].")' class='icono'>";
			}
		
		}elseif($estatus=='temporal'){
		echo "<img src='../images/restaurar.png' title='Restaurar' onclick='restaurarUser(". $row["id_usuario"].")' class='icono'>";
		}
		echo "</td>";
		echo "</tr>";
		$i++;
	}
	
?>
</tbody>
</table>

<?php
	#----------------------------- PAGINACION ----------------------------#
	echo "<br />";
	echo "<div id='paginate' class='paginacion'>";
	$url = "administrador.php?";
	$classCss = "numPages";
	$back = "&laquo;Atras";
	$next = "Siguiente&raquo;";
	$paginacion->generaPaginacion($total, $back, $next, $url, $classCss);
	echo "</div>";
	echo "<br />";
	#--------------------------------------------------------------------#
?>	
	
</div><!------------------------ TERMINA CONTENIDO -------------------------------->

<!------------------ CONTENEDOR FORMUARIO DE EDICION ------------------>
<div id="dialog-editarUsuario"> <div id="frmEditarUsuario"></div> </div>

<!--------------- DIV CONTENEDOR DEL FORM DE INICIO --------------->
<div id="dialog-nuevoUser" title="Nuevo Usuario">
		
	<form id="frmNuevoAdmin" name="frmNuevoAdmin" method="POST">
		<fieldset>
		<legend>Nuevo administrador</legend>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr><td width="30%">
				Nombre
			</td>
			<td width="35%">
				<input type="text" id="nombreAdmin" name="nombreAdmin" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errNom" class="grid_3 error"></span>
            </td>    
			</tr>
			
			<tr><td width="30%">
				<label for="apellidoPat">Apellido Paterno</label>
			</td>
			<td width="35%">
				<input type="text" id="apellidoPat" name="apellidoPat" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errApp" class="grid_3 error"></span>
            </td></tr>
			
			<tr><td width="30%">
				<label for="apellidoMat">Apellido Materno</label>
			</td>
			<td width="35%">
				<input type="text" id="apellidoMat" name="apellidoMat" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errApm" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td width="30%">
				<label for="user">Usuario</label>
			</td>
			<td width="35%">
				<input type="text" id="user" name="user" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errUsr" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td width="30%">
				<label for="pass">Contraseña</label>
			</td>
			<td width="35%">
				<input type="password" id="pass" name="pass" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errPas" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td width="30%">
				<label>Sexo</label>
			</td>
			<td width="35%">
			<select id="sexo" name="sexo">
				<option value="">Selecione...</option>
				<option value="H">Hombre</option>
				<option value="M">Mujer</option>
			</select>*
			</td>
			<td width="35%">
				<span id="errSex" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td width="30%">
				<label for="correo">Correo</label>
			</td>
			<td width="35%">
				<input type="text" id="correo" name="correo" class="textinf" />*
			</td>
			<td width="35%">
				<span id="errCorreo" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td width="30%">
				<label>Privilegios</label>
			</td>
			<td width="35%">
			<select id="privilegios" name="privilegios">
				<option value="">Seleccion...</option>
				<option value="5">Director</option>
				<option value="4">Control Escolar</option>
				<option value="1">Administrativo</option>
                <option value="3">Administrador Docentes</option>
			</select>*
			</td>
			<td width="35%">
			<span id="errPriv" class="grid_3 error"></span>
            </td></tr>
		</table>
        </fieldset>
		<div id="result" class="mensajes"></div>
	</form>
    
</div>


</div><!-------------------- TERMINA CONTENEDOR PRINCIPAL ------------------------->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/administrador_admins.js"></script>
</body>
</html>