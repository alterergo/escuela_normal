<?php
session_start();
?>
<!DOCTYPE html>
<html lang="esp">
  <head>
    <title>Concentrado Calificaciones</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript">
                $(function () 
                { 
                <?php 
                $lista=$_SESSION['concentrado'];
                $nombres= array();
                $primer= array();
                $segundo = array();
                $tercero = array();
                $semestral = array();
                //recorremos el array 
                $contador=0;
                foreach ($lista as $key => $value)
                {
                    $nombres[$contador]="'".$value[1]."'";
                    $primer[$contador]=$value[2];
                    $segundo[$contador]=$value[3];
                    $tercero[$contador]=$value[4];
                    $semestral[$contador]=$value[5];

                    $contador=$contador+1;
                }
                $nombrescript=implode(",", $nombres);
                echo 'var Nombres=['.$nombrescript.'];';
                echo 'var PrimerParcial=['.implode(",",$primer).'];';
                 ?>
                $('#container').highcharts({

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Primer Parcial'
                    },
                    subtitle: {
                        text: 'Source:ensfep.edu.mx'
                    },
                    xAxis: {

                        categories: Nombres,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Calificacion ()',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Primer Parcial',
                        data: PrimerParcial
                    }]
                });
            });

    </script>

    <script type="text/javascript">
                   $(function () 
                { 
                <?php 
                //recorremos el array 
                echo 'var Nombres=['.$nombrescript.'];';
                 ?>
                 var SegundoParcial=[<?php echo implode(",",$segundo); ?>];
                $('#segundo').highcharts({

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Segundo Parcial'
                    },
                    subtitle: {
                        text: 'Source:ensfep.edu.mx'
                    },
                    xAxis: {

                        categories: Nombres,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Calificacion ()',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Segundo Parcial',
                        data: SegundoParcial
                    }]
                });
            });

    </script>
    <script type="text/javascript">
                   $(function () 
                { 
                <?php 
                //recorremos el array 
                echo 'var Nombres=['.$nombrescript.'];';
                
                 ?>
                 var TercerParcial=[<?php echo implode(",",$tercero); ?>];
                 var SegundoParcial=[<?php echo implode(",",$segundo); ?>];
                $('#tercero').highcharts({

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Tercer Parcial'
                    },
                    subtitle: {
                        text: 'Source:ensfep.edu.mx'
                    },
                    xAxis: {

                        categories: Nombres,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Calificacion ()',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Tercer Parcial',
                        data: TercerParcial
                    }]
                });
            });

    </script>
        <script type="text/javascript">
                   $(function () 
                { 
                <?php 
                //recorremos el array 
                echo 'var Nombres=['.$nombrescript.'];';
                
                 ?>
                  var Semestral=[<?php echo implode(",", $semestral) ?>];
                $('#semestral').highcharts({

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Semestral'
                    },
                    subtitle: {
                        text: 'Source:ensfep.edu.mx'
                    },
                    xAxis: {

                        categories: Nombres,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Calificacion ()',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' '
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Semestral',
                        data: Semestral
                    }]
                });
            });

    </script>

    <script type="text/javascript">
$(function () 
{
    $('#promedio').highcharts({

        chart: {
            type: 'column'
        },
        title: {
            text: 'Promedio General Por Alumno'
        },
        subtitle: {
            text: 'ensfep.edu.mx'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -90,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Calificacion (0-10)'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Calificacion Promedio: <b>{point.y:.1f} Base Diez</b>'
        },
        series: [{
            name: 'Alumnos',
            data: [<?php echo $_SESSION['promedios']; ?>],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
        </script>
</head>
<body>
    <h4><?php echo $_SESSION['grupo']; ?></h4>
    <h2>Grafica de Concentrado</h2>
    <div id="container"></div>
    <div id="segundo"></div>
    <div id="tercero"></div>
    <div id="semestral"></div>
    <h2>Grafica Promedio General</h2>
    <div id="promedio"></div>
</body>
<SCRIPT TYPE="text/javascript" src="../js/highcharts.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" src="../js/exporting.js"></SCRIPT>
</html>