<?php
$expedido=utf8_encode($_GET['expe']);
#---------------- EXPORTAR ----------------#
/*
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=boleta.xls");
*/
include "../../../includes/conexion.php";



$idAlumno = $_GET["idAlumno"];
$id_sem_get=$_GET['grado'];
$ciclo_escolar_asignado=$_GET['cicl'];
//$grado=2;
$selectAlumnos="
				SELECT
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$idAlumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad

				";

$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	//$grado = $row["grado"];
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = utf8_encode($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];

	$plan=$row['plan_estudios'];
}

switch($id_sem_get){
	case 1:
		$grado = "PRIMER";
		break;
	case 2:
		$grado = "SEGUNDO";
		break;
	case 3:
		$grado = "TERCER";
		break;
	case 4:
		$grado = "CUARTO";
		break;
	case 5:
		$grado = "QUINTO";
		break;
	case 6:
		$grado = "SEXTO";
		break;
	case 7:
		$grado = "SEPTIMO";
		break;
	case 8:
		$grado = "OCTAVO";
		break;
	case 9:
		$grado = "NOVENO";
		break;
	case 10:
		$grado = "DÉCIMO";
		break;
	case 11:
		$grado = "DECIMOPRIMERO";
		break;
	case 12:
		$grado = "DECIMOSEGUNDO";
		break;
}

/*
///////////////OBTENER PARCIAL EN CURSO/////////////////////////////////
$parc=mysql_query("SELECT * FROM parciales WHERE estatus='1'",$conexion);
if($par=mysql_fetch_assoc($parc)):
	$parcial_curso=$par['nombre_parcial'];
	$id_parcial=$par['id_parcial'];
endif;
////////////////////////////////////////////////////////////////////////////////////
*/

//onbtener ciclo escolar correspondiente//////


/////fin ciclo escolar correspondiente//
///////////////obtener CICLO ESCOLAR/////////////////////////////////
$c_e_act=mysql_query("SELECT * FROM ciclo_escolar WHERE activo='1' && en_curso='1'",$conexion);

while($ci_es_ac=mysql_fetch_assoc($c_e_act)):
		$ciclo_escolar_actual=$ci_es_ac['id_ciclo'];
		$cicloActual = $ci_es_ac["ciclo_escolar"];
endwhile;
////////////////////////////////////////////////////////////////////////////////////

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Boleta</title>
<link rel="stylesheet" href="../../../css/formatos.css"></link>
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" width="816" height="1056">
<tr align="center" valign="top"  >
    <td height="10%" width="15%" style="padding-top:15px"><img src="<?php echo $base_url;?>images/sep.jpg" width="150"></td>
    <td width="70%" style="padding-top:15px" >SECRETAR&Iacute;A DE EDUCACI&Oacute;N P&Uacute;BLICA<br/> ESCUELA NORMAL SUPERIOR FEDERALIZADA<br/> DEL ESTADO DE PUEBA <br /><br /><? echo $ciclo_escolar_asignado?></td>
    <td width="15%" style="padding-top:15px" align="left"><img src="<?php echo $base_url;?>images/escudo.jpg"></td>
</tr>

<tr><td colspan="3" align="center">
	<table cellpadding="0" cellspacing="5" border="0">
	<tr align="center">
		<td style="padding-right:22px"><span style="border: #000 solid 2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PLAN DE CUATRO A&NtildeOS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
		<td style="padding-left:22px"> <span style="border: #000 solid 2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. DE MATRICULA: &nbsp; <?php echo $matricula; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
	</tr>
    </table>
</td></tr>


<tr><td colspan="3">
	La Direcci&oacute;n de la Escuela Normal Superior Federalizada del Estado de Puebla, hace constar que, seg&uacute;n los documentos que obran en el archivo del plantel, el estudiante:<br/><br/>
	<div class="nomAlumno"><u><?php echo $nombre." ".$apellidoPat." ".$apellidoMat; ?></u></div><br/>
	Obtuvo las calificaciones en las asignaturas del <? echo $plan; ?> correspondientes al <b><u><?php echo $grado; ?> SEMESTRE</u></b> de la Licenciatura en Educaci&oacute;n Secundaria con Especialidad en: <b><u><?php echo $nombreEspecialidad; ?></u></b> Modalidad Escolarizada.
</td></tr>

<tr><td colspan="3">

	<table class="boleta" width="100%" border="1" cellpadding="0" cellspacing="0">
		<thead>
		<tr>
			<td rowspan=2 width="60%" align="center"><b>MATERIAS</b></td>
			<td colspan=2 align="center" ><b>CALIFICACIONES</b></td>
		</tr>
		<tr>
			<td width="3%" align="center"><b>NUM.</b></td>
			<td width="37%" align="center"><b>LETRA</b></td>
		</tr>
		</thead>
		<tbody>
	<?php

		//$dat=mysql_query("SELECT id_semestre FROM semestre WHERE grado='$grado_'",$conexion);

		//while($sem_mate=mysql_fetch_assoc($dat)):
			//$id_sem=$sem_mate['id_semestre'];



				/*$mate=mysql_query("SELECT t1.id_materia, t1.nombre FROM materias AS t1, calif_parc AS t2 WHERE
				t1.id_semestre='$id_sem_get'
				AND t1.id_especialidad='$idEspecialidad'
				AND t1.estatus='1'
				AND t2.id_alumno='$idAlumno'
				AND t2.id_materia=t1.id_materia
				GROUP BY t1.id_materia",$conexion);*/

				$mate=mysql_query("SELECT t1.id_materia, t1.nombre FROM materias AS t1 WHERE
				t1.id_semestre='$id_sem_get'
				AND t1.id_especialidad='$idEspecialidad'
				AND t1.estatus='1'",$conexion);


					 $i=0;
					while($mate_=mysql_fetch_assoc($mate)){
						//se calcula promedio con base en consulta
						$queryCalif1 = "SELECT DISTINCT id_parcial,calificacion FROM calif_parc where id_alumno = '$idAlumno' and id_materia = ".$mate_["id_materia"] ." ";
						$resultCalif1 = mysql_query($queryCalif1,$conexion);
						$totalCalif=0;
						$califSum = 0;
						while($calif1 = mysql_fetch_assoc($resultCalif1)){
							$califSum = $califSum+(float)$calif1['calificacion'];
							$totalCalif = $totalCalif+1;
						}
						if ($totalCalif == 0){
							$totalCalif = 1;
						}
						$califSum = round($califSum/$totalCalif, 1, PHP_ROUND_HALF_DOWN);
						if ($califSum == 0) {
							$califSum = ".";
						}
						//////////////////////////////////////////
						$nombre_materia=utf8_encode($mate_['nombre']);
						$calificacion= substr((string)$califSum,0,3);
						//$calificacion=substr($mate_['AVG(t2.calificacion)'],0,3);

						if (substr($calificacion,0,2)==10) {
							$calificacion_=$calificacion;
						} else {

							$califAlm = explode('.',$calificacion);
							if(empty($califAlm[0])){
								$calificacion_='';
							}elseif (empty($califAlm[1])){
								$calificacion_=$califAlm[0].".0";
							}else{
								//echo $califAlm[0]."-".$califAlm[1]."<br/>";
								$calificacion_=$calificacion;
							}
						}


						$entero=substr($calificacion,0,2);

						switch($entero){
							case 0.:
								$letra_entero= "cero";
								break;
							case 1.:
								$letra_entero= "uno";
								break;
							case 2.:
								$letra_entero= "dos";
								break;
							case 3.:
								$letra_entero= "tres";
								break;
							case 4.:
								$letra_entero= "cuatro";
								break;
							case 5.:
								$letra_entero= "cinco";
								break;
							case 6.:
								$letra_entero= "seis";
								break;
							case 7.:
								$letra_entero= "siete";
								break;
							case 8.:
								$letra_entero= "ocho";
								break;
							case 9.:
								$letra_entero= "nueve";
								break;
							case 10:
								$letra_entero= "diez";
								break;


						}

						$decimal=substr($calificacion,2,4); //echo "D".$decimal;

						switch($decimal){
							case 0:
								$letra_decimal= "punto cero";
								break;
							case 1:
								$letra_decimal= "punto uno";
								break;
							case 2:
								$letra_decimal= "punto dos";
								break;
							case 3:
								$letra_decimal= "punto tres";
								break;
							case 4:
								$letra_decimal= "punto cuatro";
								break;
							case 5:
								$letra_decimal= "punto cinco";
								break;
							case 6:
								$letra_decimal= "punto seis";
								break;
							case 7:
								$letra_decimal= "punto siete";
								break;
							case 8:
								$letra_decimal= "punto ocho";
								break;
							case 9:
								$letra_decimal= "punto nueve";
								break;


						}
						$letra_entero=strtoupper($letra_entero);
						$letra_decimal=strtoupper($letra_decimal);

						echo "<tr>";
						echo "<td width='70%' style='padding-left:5px;'>".$nombre_materia."</td>";
						echo "<td width='3%' align='center'>".$calificacion_."</td>";
						echo "<td width='27%' style='padding-left:5px;'>".$letra_entero." ".$letra_decimal."</td>";
						echo "</tr>";

						$suma_calif=$calificacion_+$suma_calif;

						$i++;
					}
						if ($i==0){ $i=1;}
						$promedio=$suma_calif/$i;
						$promedio=substr($promedio,0,3);

						$entero_p=substr($promedio,0,2);

						switch($entero_p){
							case 0.:
								$letra_entero_p= "cero";
								break;
							case 1.:
								$letra_entero_p= "uno";
								break;
							case 2.:
								$letra_entero_p= "dos";
								break;
							case 3.:
								$letra_entero_p= "tres";
								break;
							case 4.:
								$letra_entero_p= "cuatro";
								break;
							case 5.:
								$letra_entero_p= "cinco";
								break;
							case 6.:
								$letra_entero_p= "seis";
								break;
							case 7.:
								$letra_entero_p= "siete";
								break;
							case 8.:
								$letra_entero_p= "ocho";
								break;
							case 9.:
								$letra_entero_p= "nueve";
								break;
							case 10:
								$letra_entero_p= "diez";
								break;


						}

						$decimal_p=substr($promedio,2,4); //echo "D".$decimal;

						switch($decimal_p){
							case 0:
								$letra_decimal_p= "punto cero";
								break;
							case 1:
								$letra_decimal_p= "punto uno";
								break;
							case 2:
								$letra_decimal_p= "punto dos";
								break;
							case 3:
								$letra_decimal_p= "punto tres";
								break;
							case 4:
								$letra_decimal_p= "punto cuatro";
								break;
							case 5:
								$letra_decimal_p= "punto cinco";
								break;
							case 6:
								$letra_decimal_p= "punto seis";
								break;
							case 7:
								$letra_decimal_p= "punto siete";
								break;
							case 8:
								$letra_decimal_p= "punto ocho";
								break;
							case 9:
								$letra_decimal_p= "punto nueve";
								break;


						}
						$letra_entero_p=strtoupper($letra_entero_p);
						$letra_decimal_p=strtoupper($letra_decimal_p);

						echo "<tr>";
						echo "<td width='70%' align='right' style='padding-right:5px;'><b>PROMEDIO</b></td>";
						echo "<td width='3%' align='center'>".$promedio."</td>";
						echo "<td width='27%' style='padding-left:5px;'>".$letra_entero_p." ".$letra_decimal_p."</td>";
						echo "</tr>";

		//endwhile;



	?>
		</tbody>
	</table>
</td></tr>

<tr><td colspan="3" align="center">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr><td colspan="2" align="center">
    <!--PROMEDIO <? //echo $promedio?><br /><br />-->
	SAN JUAN B. CUAUTLANCINGO, PUE. A <span style="text-transform:uppercase;"><? echo $expedido?></span> <br />
	<br/><br/>
    </td></tr>
	<tr><td colspan="2" align="center"><br/><br/><br/>
    ATENTAMENTE<br/>
		"Ejercer la libertad para trasender en el tiempo"
    </td></tr>
    <tr><td colspan="2" align="center"><br/><br /><br /><br /><br /><br />
    MTRO. GERARDO PAUL ARVIZU SERAPIO<br/>
		DIRECTOR
    </td></tr>
    <tr><td colspan="2" align="left" style="text-align:left; font-size:11px;"><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    COTEJÓ: FERNANDO CUATECATL CUATECATL
    </td></tr>
    <tr><td colspan="2" align="left"><br/><br/>
		<span style="font-size:10px">Calle Azteca Norte, Col. Reserva Territorial Quetzalc&oacute;atl, C.P. 72150 Tel. 01 222 283 6505, E-mail: ensfep130@gmail.com</span>
    </td></tr>
    </table>
</td></tr>

</table>
</body>
</html>
