<?php
include '../includes/conexion.php';
session_start();
$id_usuario = $_SESSION['id_usuario'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Docentes</title>
<link rel="stylesheet" href="../css/style.css"></link>
<link rel="stylesheet" href="../css/cupertino/jquery-ui-1.9.2.custom.css"></link>

</head>
<body onload="docentes();">
<div id="contenedorAdmin" class="row">

<div id="headerAdmin" class="grid_12">
	<?php include 'menu.php'; ?>
</div>

<div id="contenido" class="grid_12">
       

 <table id="materias" cellpadding="10" cellspacing="0" border="0" width="100%" class='table_cons'>
<thead>
<th colspan="5" align="center" bgcolor="#F1F1F1">
Seleccionar parcial <em>(Para consulta de asistencias)</em>:
<select name="parcial_c" id="parcial_c">
	<option value="0" selected="selected">Parcial...</option>
	<option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
</select>
</th>
</thead>
<thead>
<th width="5%">Plan</th><th width="5%">Clave</th><th width="56%">Materia</th><th width="16%">Especialidad</th><th width="18%">Opciones</th>
</thead>

<?php
$c_e_act=mysql_query("SELECT * FROM ciclo_escolar WHERE activo='1' && en_curso='1'",$conexion);
while($ci_es_ac=mysql_fetch_assoc($c_e_act)):
		$ciclo_escolar_actual=$ci_es_ac['id_ciclo'];
endwhile;		
$i=1;

/////////////////////////////////////////////////////////////
/*$consulta=mysql_query("SELECT * FROM materias_maestros AS t1, materias AS t2, semestre AS t3, catalogoespecialidades AS t4, plan_estudios AS t5 WHERE 
t1.id_docente='$id_usuario'
AND  t2.id_materia=t1.id_materia
AND t3.id_semestre=t2.id_semestre
AND t4.idEspecialidad=t2.id_especialidad
AND t5.id_plan=t4.plan_estudios
ORDER BY t5.id_plan ASC  ",$conexion);*/
$consulta=mysql_query("SELECT * FROM materias_maestros AS t1, materias AS t2, plan_estudios AS t3, semestre AS t4, catalogoespecialidades AS t5 WHERE 
t1.id_docente=$id_usuario
AND t2.id_materia=t1.id_materia 
AND t5.plan_estudios=t3.id_plan
AND t4.id_semestre=t2.id_semestre
AND t5.idEspecialidad=t2.id_especialidad
AND t1.ciclo_escolar= $ciclo_escolar_actual
ORDER by t3.id_plan ASC",$conexion);

while($res=mysql_fetch_assoc($consulta)):
	$id_materia=$res['id_materia'];
	$clave=$res['clave'];
	$materia=utf8_encode($res['nombre']);
	$semestre=$res['semestre'];
	$especialidad=utf8_encode($res['nombreEspecialidad']);
	$ciclo_escolar=$res['ciclo_escolar'];
	$plan_estudios=$res['plan_estudios'];
	$grupo=$res['grupo'];
	
	if($grupo!=""){
		$grupo_="(Grupo ".$grupo.")";
	}else{
		$grupo_="";	
	}
	
	echo "<tr align='center'>
	<td width='5%'>".$plan_estudios."</td>
	<td width='5%'>".$clave."</td>
	<td width='56%'>".$materia." <b>".$grupo_."</b></td>
	<td width='13%'>".$especialidad."</td>
		<td width='18%'>"
		?>               
		<img src='../images/reloj.png' class='icono' title='Horas del curso' onclick='horas_clase(<? echo $id_materia?>,"<? echo $grupo?>")' >	
		<img src='../images/semestres.png' class='icono' title='Pase de lista' onclick='pase_lista(<? echo $id_materia?>,"<? echo $grupo?>")'>
     	<img src='../images/editar.png' class='icono' title='Editar pase de lista' onclick='pase_lista_edit(<? echo $id_materia?>,"<? echo $grupo?>")'>
		<a href="javascript:;" target="_blank" onClick="window.open('hoja_lista.php?m=<? echo $id_materia?>&g=<? echo $grupo?>&pa='+document.getElementById('parcial_c').value+'&ref=N', this.target, 'width=1000,height=700'); return false;"><img src='../images/document.png' class='icono' title='Formato de pase de lista' /></a>
		<!--<a href="hoja_lista.php?m=<?// echo $id_materia?>&g=<?// echo $grupo?>" target="_blank" onClick="window.open(this.href, this.target, 'width=1000,height=700'); return false;"><img src='../images/document.png' class='icono' title='Formato de pase de lista' /></a>	     -->
		 
		<img src='../images/calificaciones.png' class='icono' title='Calificaciones' onclick='calificaciones(<? echo $id_materia?>,"<? echo $grupo?>")'>
		
        </td>
	
		</tr>
        <?
$i++;
endwhile;


?>
</table>

</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->


</div>

<!-- INICIA FORMULARIO INFO DE DOCENTE -->
<div id="info_docente" title="Información del docente">  
<form id="updDocente_sess" name="updDocente_sess" >
<fieldset>
<legend>Información Personal</legend>
<div id="updt_msg" class="mensajes" align="center"></div>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<?php
$cosultarInformacion = " SELECT * FROM usuarios WHERE id_usuario= '$id_usuario' "; 
$respuesta = mysql_query($cosultarInformacion,$conexion);

while($row = mysql_fetch_assoc($respuesta)){
	$nom_get=utf8_encode($row['nombre']);
	$ape_p_get=utf8_encode($row['apellido_pat']);
	$ape_m_get=utf8_encode($row['apellido_mat']);
	$user_get=utf8_encode($row['user']);
	$pass_get=$row['pass'];
	$sexo_get=$row['sexo'];
	$correo_get=utf8_encode($row['correo']);
	$telefono=$row['telefono'];
	$direccion=utf8_encode($row['direccion']);
	$rfc=$row['rfc'];
	$curp=$row['curp'];
	$clv_presup=$row['clv_presup'];
	$grado_academico=utf8_encode($row['grado_academico']);
	$ced_prof=utf8_encode($row['ced_prof']);
	
	echo "<input type='hidden' name='id_docente' value='$id_usuario' />";
	
	echo "<tr align='left'><td width='30%'>Nombre:</td>
		<td width='40%' style='padding-bottom:7px'><input type='text' id='nomb_edt' name='nomb_edt' value='".$nom_get."' class='textinf' style='width:270px; text-transform:uppercase; '/>*</td>
		<td id='errNom_edt' class='error' width='30%'></td></tr>";
	
	echo "<tr align='left'><td >Apellido Paterno:</td>
		<td style='padding-bottom:7px'><input type='text' id='app_edt' name='app_edt' value='".$ape_p_get."' class='textinf' style='width:270px; text-transform:uppercase;'/>*</td>
		<td id='errApp_edt' class='error' ></td></tr>";
	
	echo "<tr align='left'><td>Apellido Materno:</td><td style='padding-bottom:7px'><input type='text' id='apm_edt' name='apm_edt' value='".$ape_m_get."' class='textinf' style='width:270px; text-transform:uppercase;'/>*</td>
	<td id='errApm_edt' class='error' ></td></tr>";
?>		
	<tr align='left'><td >Sexo:</td><td style='padding-bottom:7px'><select name='sexo_edt' class='textinf' style='width:150px'>
	<option value='H' <? if ($sexo_get=='H'):?> selected='selected' <? endif;?>>Hombre</option>
	<option value='M' <? if ($sexo_get=='M'):?> selected='selected' <? endif;?>>Mujer</option>
	</select>*</td></tr>
<?		
	echo "<tr align='left'><td >Correo:</td><td style='padding-bottom:7px'><input type='text' id='correo_edt' name='correo_edt' value='".$correo_get."' class='textinf' style='width:270px; '/>*</td>
	<td id='errCorreo_edt' class='error' ></td></tr>";
	
	echo "<tr>
        <td>Telefono</td>
        <td><input type='text' name='tel_edt' id='tel_edt' class='textinf' style='width:150px;' value='".$telefono."' />
        </td>
        <td id='errTel_edt' class='error'></td>
        </tr>";
        
       echo '<tr>
        <td>Dirección</td>
        <td><input type="text" name="direcc_edt" id="direcc_edt" class="textinf" style="width:270px;" value="'.$direccion.'" />
        </td>
        <td id="errDirecc_edt" class="error"></td>
        </tr>';
        
       echo '<tr>
        <td>RFC</td>
        <td><input type="text" name="rfc_edt" id="rfc_edt" class="textinf" style="width:150px; text-transform:uppercase;" value="'.$rfc.'"/>
        </td>
        <td id="errRfc_edt" class="error"></td>
        </tr>';
        
       echo '<tr>
        <td>CURP</td>
        <td><input type="text" name="curp_edt" id="curp_edt" class="textinf" style="width:150px; text-transform:uppercase;" value="'.$curp.'"/>
        </td>
        <td id="errCurp_edt" class="error"></td>
        </tr>';
        
        echo '<tr>
        <td>Clave Presupuestal</td>
        <td><input type="text" name="clave_p_edt" id="clave_p_edt" class="textinf" style="width:150px;" value="'.$clv_presup.'"/></td>
        <td id="errClave_p_edt" class="error"></td>
        </tr>';
         
        echo '<tr>
        <td>Grado Academico</td>
        <td><input type="text" name="grad_acad_edt" id="grad_acad_edt" class="textinf" style="width:270px; text-transform:uppercase;" value="'.$grado_academico.'"/>
        </td>
        <td id="errGrad_acad_edt" class="error"></td>
        </tr>';
        
        echo '<tr>
        <td>Cedula Profesional</td>
        <td><input type="text" name="cedula_edt" id="cedula_edt" class="textinf" style="width:270px; text-transform:uppercase;" value="'.$ced_prof.'"/></td>
        <td id="errCedula_edt" class="error"></td>
        </tr>';

	echo "<tr align='left'><td >Usuario:</td><td style='padding-bottom:7px'><input type='text' name='usuario_edt' value='".$user_get."' class='textinf' style='width:270px;' readonly/></td></tr>";
	
	echo "<tr  align='left'><td >Contraseña:</td><td style='padding-bottom:7px'><input type='password' name='pass_edt' value='' class='textinf' style='width:270px; ' placeholder='Nueva contraseña'/></td></tr>";
	
	
	
}

?>
</table>

</fieldset>
</form>
</div>
<!-- TERMINA FORMULARIO INFO DE DOCENTE -->


<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/placeholder.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/adm_doc.js"></script>
</body>
</html>