<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Alumnos</title>
<link rel="stylesheet" href="../../css/style.css"></link>
<link rel="stylesheet" href="../includes/alumnos.css"></link>
<link rel="stylesheet" href="../../css/cupertino/jquery-ui-1.9.2.custom.css"></link>

</head>
<body>
<div id="contenedor" class="row">

<div id="headerAdmin" class="grid_11">
	<?php include '../includes/menu.php'; ?>
</div>

<div id="contenido" class="grid_11" >

<div id="myDiv" class="tooltip" align="center"></div>

<table id="tablaAlumnos" cellpadding="15" class="table_cons">
    <thead>
		<th>#</th>
		<th>Clave</th>
        <th>Materia</th>
		<th>Parcial 1</th>
		<th>Parcial 2</th>
		<th>Parcial 3</th>
		<th width="9%">Final</th>
    </thead>
<tbody>

<?php
	$i = 1;
	
	$c_e_act=mysql_query("SELECT * FROM ciclo_escolar WHERE activo='1' && en_curso='1'",$conexion);
while($ci_es_ac=mysql_fetch_assoc($c_e_act)):
		$ciclo_escolar_actual=$ci_es_ac['id_ciclo'];
endwhile;


$materias="SELECT * FROM semestre_curso AS t1, semestre AS t2, materias AS t3  WHERE t1.id_alumno='$idAlumno' AND t2.id_semestre=t1.id_semestre AND t3.id_semestre=t2.id_semestre AND t3.plan_estudios=t1.plan_estudios AND t3.estatus = 1 ";
echo $$materias;
$materias_=mysql_query($materias,$conexion);
while($dat_mat=mysql_fetch_assoc($materias_)):
       $clave=utf8_encode($dat_mat['clave']);
       $nombre_materia=utf8_encode($dat_mat['nombre']);
	   $idMateria = $dat_mat['id_materia'];
		
		
				
?>	   
		<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $clave; ?></td>
		<td><?php echo $nombre_materia; ?></td>
<?		
		$calificacionParcial = " SELECT calificacion FROM calif_parc WHERE id_alumno = '$idAlumno' AND id_materia = '$idMateria' AND ciclo_escolar='$ciclo_escolar_actual'  ORDER BY id_parcial ASC ";  
		$calificacion=mysql_query($calificacionParcial,$conexion);
		$total=mysql_num_rows($calificacion); 
			while($row = mysql_fetch_assoc($calificacion)){
				$p1 = $row["calificacion"];
				
				if($total==1):
					echo "<td>$p1</td>";
					echo "<td></td>";					
				elseif($total==2):
					echo "<td>$p1</td>";									
				elseif($total==3):
					echo "<td>$p1</td>";
					$suma_calif=$suma_calif+$p1; 
				endif;		
		//echo "<td>$p1 </td>";				
		}	
				
				if($total==2):
					echo "<td></td>";			
				endif;
		if($total==3){
			$calif_final=substr(($suma_calif/3),0,3);		
			echo "<td><b>".$calif_final."</b></td>";
		}
		echo "</tr>";
$i++;
endwhile;
?>
</tbody>
</table>

</div><!-- TERMINA CONTENIDO -->

</div><!-- TERMINA CONTENEDOR PRINCIPAL -->

<div id="dialog-accesos">
	<form id="editarAccesos" name="editarAccesos" method="POST">
		<fieldset>
		<legend>Editar Accesos</legend>
		<input type="hidden" id="pass" name="pass" value="<?php echo $pass;?>" />
		<label>Nueva Contraseña</label>
		<input type="password" id="nvoPass" name="nvoPass" />
		<div class="error" id="errPass"></div>
		<input type="hidden" id="idAlumno" name="idAlumno" value="<?php echo $idAlumno?>" />
		</fieldset>
		<div class="mensajes" id="result"></div>
	</form>
</div>

<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../includes/alumnos.js"></script>

</body>
</html>