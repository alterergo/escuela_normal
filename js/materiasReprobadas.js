(function(){
/********** tooltip ************/
 $( ".icono" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
 	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});

$( ".tooltip" ).tooltip({
	show: null,
	position: {
	my: "left top",
	at: "left bottom"
	},
	open: function( event, ui ) {
	ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
	}
});	
	


$( "#dialog-editarCalificacion" ).dialog({
	autoOpen: false,
	modal: true,
	width: 950,
	height: 300,
	buttons: [
		{
			text: "Guardar",
			click: function() {
				// $( this ).dialog( "close" );
				
				var accion = $("#accion").val();
				
				var oportunidad1 	= $('#oportunidad1').val();
				var fecha1 			= $('#fecha1').val();
				var oportunidad2 	= $('#oportunidad2').val();
				var fecha2 			= $('#fecha2').val();
				var oportunidad3 	= $('#oportunidad3').val();
				var fecha3 			= $('#fecha3').val();
				var idAlumno  		= $('#idAlumno').val();
				var idMateria 		= $('#idMateria').val();
				
				if(oportunidad1 == ""){
					$("#errOp1").html("<p style='font-size:10px; color: #f00;'>Campo Requerido</p>").fadeIn(900).delay(1000).fadeOut(900);
					return;
				}
				
				if(fecha1 == ""){
					$("#errFecha1").html("<p style='font-size:10px; color: #f00;'>Campo Requerido</p>").fadeIn(900).delay(1000).fadeOut(900);
					return;
				}
				
				if(accion == 'insertar'){
					$.post("materiasReprobadas/insertarMaterias.php",
							{
							oportunidad1: oportunidad1,
							fecha1: fecha1,
							oportunidad2: oportunidad2,
							fecha2: fecha2,
							oportunidad3: oportunidad3,
							fecha3: fecha3,
							idAlumno: idAlumno,
							idMateria: idMateria
							},
							function(data){
								//alert(data);
								 if(data[0]==1){
									 $('#result').html("<p class='ui-state-highlight'>Calificaci&oacute;n Registrada</p>").fadeIn(900).delay(2000).fadeOut(900);
									 document.location.reload();
								 }else{
									 $('#result').html("<p class='ui-state-highlight'>Error. Intente de nuevo</p>").fadeIn(900).delay(2000).fadeOut(900);
									 
								 }
							});
				}else{
					$.post("materiasReprobadas/editarMaterias.php",
							{
							oportunidad1: oportunidad1,
							fecha1: fecha1,
							oportunidad2: oportunidad2,
							fecha2: fecha2,
							oportunidad3: oportunidad3,
							fecha3: fecha3,
							idAlumno: idAlumno,
							idMateria: idMateria
							},
							function(data){
								//alert(data);
								 if(data[0]==1){
									 $('#result').html("<p class='ui-state-highlight'>Calificaci&oacute;n Actualizada</p>").fadeIn(900).delay(2000).fadeOut(900);
									 document.location.reload();
								 }else{
									 $('#result').html("<p class='ui-state-highlight'>Error. Intente de nuevo</p>").fadeIn(900).delay(2000).fadeOut(900);
									 
								 }
							});
				}
				
				
				//Termina POST 
			}
		},
		{
			text: "Cancel",
			click: function() {
				$( this ).dialog( "close" );
			}
		}
	]
});	
	



})();

function agregarCalificacion(idAlumno,idMateria){
	//alert(idAlumno+'-'+idMateria);
	$("#frmAgregarCalificacion").load("materiasReprobadas/frmMateriasReprobadas.php",
	{idAlumno: idAlumno,idMateria: idMateria},
	function(data){
		//alert(data);
		$( "#dialog-editarCalificacion" ).dialog( "open" );
	});
}




