<?php
require('fpdf/fpdf.php');
require('fpdf/fpdfHTML.php');

include("../../../includes/conexion.php");

session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$id_alumno=$_GET['id'];
//$id_alumno=231;
$generacion=$_GET['G'];
$anio=$_GET['A'];
$numero=$_GET['N'];
$fecha=$_GET['F'];
$titulo_num=$_GET['TN'];
//$fecha=date("d-m-Y");	
$hora=date("h:i:s");

$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t5.plan_estudios,
					t6.ciclo_escolar,
					t7.anios,
					t7.plan_estudios
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = '$id_alumno'
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.idEspecialidad = t5.id_especialidad	
				
				";
$result = mysql_query($selectAlumnos,$conexion);


if($row = mysql_fetch_array($result)){
	$matricula = $row["matricula"];
	$nombre = utf8_encode($row["nombre"]);
	$apellidoPat = utf8_encode($row["apellido_paterno"]);
	$apellidoMat = utf8_encode($row["apellido_materno"]);
	$id_semestre = $row["id_semestre"];
	$semestre = utf8_encode($row["semestre"]);
	//$grado = $row["grado"];
	//$grado_ = $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = ($row["nombreEspecialidad"]);
	$ciclo_escolar = $row["ciclo_escolar"];
	$plan=$row['plan_estudios'];
	$curp=$row['curp'];
	
	$nombre_completo=$nombre." ".$apellidoPat." ".$apellidoMat;
}

class PDF extends FPDF
{
	
	function Header()
	{		 
		 //$this->Image('titulo atras.jpg',0,0,216);
		//$this->Image('lunketec_logo.png',186,8,17);
	}
	/*
	function Footer()
	{
		$this->SetXY(100,267);
		$this->SetTextColor(128,128,128);
		$this->Cell(60,1,'P'.html_entity_decode("&aacute;").'gina '.$this->PageNo().'/{nb}');	
	}
	*/
}//fin class
	 
	 

//$pdf->Cell( ancho,alto,msg,borde(0,1,L,R,T,B),posicion(0,1,2),align(L,C,R),dibuja fondo(true,false),link)
$pdf = new PDF_HTML('P','mm','Letter');
//$pdf=new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetFont('Arial','',10); //Times, Helvetica, Arial, Courier

// NOMBRE
$pdf->SetXY(138,10);
$pdf->Cell(50,3,"".utf8_decode($nombre_completo)."",0,0,'L');

//CURP
$pdf->SetXY(145,23);
$pdf->Cell(50,4,"".$curp."",0,0,'L');

//clave ESCUELA
$pdf->SetXY(150,47);
$pdf->Cell(50,4,"21DNL0006L",0,0,'L');

//PLAN
$pdf->SetXY(169,56);
$pdf->Cell(50,4,"".str_replace("Plan de estudios","",$plan)."",0,0,'L');

//GENERACION
$pdf->SetXY(170,65);
$pdf->Cell(50,4,"".$generacion."",0,0,'L');

//AÑO EGRESO
$pdf->SetXY(174,74);
$pdf->Cell(50,4,"".$anio."",0,0,'L');

//# ACTA
$pdf->SetXY(160,91);
$pdf->Cell(50,4,$numero,0,0,'L');

//FECHA
$pdf->SetXY(160,100);
$pdf->Cell(50,4,"".$fecha."",0,0,'L');

//EXPEDIDA
$pdf->SetXY(145,108);
$pdf->Cell(50,4,"SAN JUAN B.CUAUTLANCINGO,PUEBLA",0,0,'L');

//TITULO #
$pdf->SetXY(160,117);
$pdf->Cell(50,4,"".$titulo_num."",0,0,'L');

//FIRMA
$pdf->SetY(185);
$pdf->SetX(133);
$pdf->WriteHTML('<b>'.utf8_decode("GERARDO PAUL ARVIZU SERAPIO").'</b>');
//$pdf->SetXY(8,69);
//$pdf->Cell(83,4,"_________________________________________",0,0,'L');
$pdf->SetXY(138,194);
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE LA ESCUELA"),0,0,'L');

//---------------------------------
/**********VALORES****************/

// ENCABEZADO TITULO REVERSO
$pdf->SetY(13);
$pdf->SetX(25);
$pdf->WriteHTML('<b>'.utf8_decode("DIRECCIÓN DE CONTROL ESCOLAR").'</b>');

//REGISTRO
$pdf->SetXY(33,17);
$pdf->Cell(33,4,"".$_GET['AD1']."",0,0,'L');
//$pdf->SetXY(33,27);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//LIBRO NUM
$pdf->SetXY(33,25);
$pdf->Cell(33,4,"".$_GET['AD2']."",0,0,'L');
//$pdf->SetXY(33,34);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FOJA NUM
$pdf->SetXY(33,34);
$pdf->Cell(33,4,"".$_GET['AD3']."",0,0,'L');
//$pdf->SetXY(33,41);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//LUGAR
$pdf->SetXY(33,42);
$pdf->Cell(33,4,"".utf8_decode($_GET['AD4'])."",0,0,'L');
//$pdf->SetXY(33,48);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FECHA
$pdf->SetXY(33,52);
$pdf->Cell(33,4,"".$_GET['AD5']."",0,0,'L');
//$pdf->SetXY(33,55);
//$pdf->Cell(33,4,"____________________________",0,0,'L');

//FIRMA
$pdf->SetY(72);
$pdf->SetX(23);
$pdf->WriteHTML('<b>'.utf8_decode("ALEJANDRO GÓMEZ GARCÍA").'</b>');
//$pdf->SetXY(8,69);
//$pdf->Cell(83,4,"_________________________________________",0,0,'L');
$pdf->SetXY(15,80);
$pdf->Cell(10,4,utf8_decode("DIRECTOR DE CONTROL ESCOLAR"),0,0,'L');

//-------------------------------
$pdf->Output("Titulo atras","I");
//-------------------------------
?>