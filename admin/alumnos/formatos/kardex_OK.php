<?php
include "../../../includes/conexion.php";
session_start();
$id_usuario = $_SESSION['id_usuario'];
$usr=mysql_query("SELECT * FROM usuarios WHERE id_usuario='$id_usuario'",$conexion);

if($usr_=mysql_fetch_assoc($usr)):
	$tipo_usuario=$usr_['tipo_usuario'];
endif;
if(empty($id_usuario)):
	header("Location: ../../../index.php"); 
elseif($tipo_usuario=='2' || $tipo_usuario=='3'):
	header("Location: ../../../index.php");
endif;

$idAlumno = $_GET["idAlumno"];
//echo $idAlumno;
$selectAlumnos="
				SELECT 
					t1.matricula,
					t1.nombre,
					t1.apellido_paterno,
					t1.apellido_materno,
					t1.curp,
					t3.id_semestre,
					t3.semestre,
					t3.grado,
					t4.idEspecialidad,
					t4.nombreEspecialidad,
					t6.ciclo_escolar
				FROM 
					alumnos AS t1,
					semestre_curso AS t2,
					semestre AS t3,
					catalogoespecialidades AS t4,
					ingreso AS t5,
					ciclo_escolar AS t6,
					plan_estudios AS t7
				WHERE
					t1.id_alumno = $idAlumno
				AND
					t6.en_curso = 1
				AND
					t1.id_alumno = t2.id_alumno
				AND
					t3.id_semestre = t2.id_semestre
				AND
					t5.id_alumno = t1.id_alumno
				AND
					t4.plan_estudios=t7.id_plan
				AND
					t4.idEspecialidad = t5.id_especialidad	
				AND 
					t5.plan_estudios = t7.id_plan
				";
				
$result = mysql_query($selectAlumnos,$conexion);


while($row = mysql_fetch_array($result)){
	$matricula 		= $row["matricula"];
	$nombre 		= utf8_encode($row["nombre"]);
	$apellidoPat 	= utf8_encode($row["apellido_paterno"]);
	$apellidoMat 	= utf8_encode($row["apellido_materno"]);
	$curp 			= $row["curp"];
	$id_semestre 	= $row["id_semestre"];
	$semestre 		= $row["semestre"];
	//$grado 			= $row["grado"];
	$idEspecialidad = $row["idEspecialidad"];
	$nombreEspecialidad = utf8_encode($row["nombreEspecialidad"]);
	//$ciclo_escolar 	= $row["ciclo_escolar"];
}

if($curp==''){
	$curp = "CURP SIN ASIGNAR";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kardex</title>
<link rel="stylesheet" href="../../../css/formatos.css"></link>
</head>
<body>
<div id="contenedorCardex">

	<div id="headerKardex">
		<div class="align-left-kardex"> <img src="../../../images/sep.jpg" class="escudoKardex"> </div>
		<div class="align-left-kardex centrar"> SISTEMA EDUCATIVO NACIONAL <BR /> KARDEX DEL ALUMNO DE EDUCACION NORMAL </div>
		<div class="align-left-kardex foto" id="foto"> </div>
	</div>	
	
	<div id="datos" class="datos">
		<div class="escuela">ESCUELA NORMAL SUPERIOR FEDERALIZADA DEL ESTADO DE PUEBLA </div>
		<div class="clave"> 21DNS001J </div>
		<!-- TEXTO DE AYUDA -->
		<div class="izquierda guia guiaEscuela"> MOMBRE DE LA ESCUELA </div> 	
		<div class="derecha guia guiaClave"> CLAVE DEL C. C. T. </div>
		<!-- DIV DE AYUDA -->
		<div style="border: #fff solid 1px; width:40%; float: right;"> &nbsp;</div>
		<div class="municipio"> CUAUTLANCINGO </div>
		<div class="estado"> PUEBLA </div>
		<div class="curp"> <?php echo $curp; ?> </div>
		<!-- TEXTO DE AYUDA -->
		<div class="guia guiaMunicipio"> MUNICIPIO O DELEGACION POLITICA </div> 
		<div class="guia guiaEntidad"> ENTIDAD FEDERATIVA </div> 
		<div class="guia guiaCurp"> CLAVE UNICA DE REGISTRO D EPOBLACION (CURP)</div>
		
		<div class="nombre"> <?php echo "<span class='guiaNombre'>".$apellidoPat."</span>"." "."<span class='guiaNombre'>".$apellidoMat."</span>"." "."<span class='guiaNombre'>".$nombre."</span>"; ?> </div>
		<!-- TEXTO DE AYUDA -->
		<div class="guia guiaNombre"> PRIMER APELLIDO </div>
		<div class="guia guiaNombre"> SEGUNDO APELLIDO </div>
		<div class="guia guiaNombre"> NOMRBE (S) </div>
		
		<div class="licenciatura"> LIC. EN EDUCACION SECUNDARIA CON ESPECIALIDAD EN <?php echo $nombreEspecialidad; ?> MOD. ESCOLARIZADA PLAN DE 4 A&Ntilde;OS  </div> <div class="matricula"> <?php echo $matricula; ?> </div>
		<!-- TEXTO DE AYUDA -->
		<div class="guia guiaLic"> LICENCIATURA Y MODALIDAD </div>
		<div class="guia guiaMat"> MATRICULA </div>
	</div>
	
	
<!---------------------------------->
<!-- CALIFICACIONES DE SEMESTRE I -->
<!---------------------------------->
<?php

function cambiarNumero($num){
	switch ($num){
		case 1: echo "I"; 	break;
		case 2:	echo "II"; 	break;	
		case 3: echo "III"; break;
		case 4: echo "IV"; 	break;
		case 5:	echo "V";	break;
		case 6: echo "VI"; break;
		case 7: echo "VII"; break;
		case 8: echo "VIII"; break;
		case 9: echo "IX"; break;
		case 10: echo "X"; break;
		case 11: echo "XI"; break;
		case 12: echo "XII"; break;
	}
}

/// sacar los semestres de la especialidad /////
$semestres_espe=mysql_query("SELECT COUNT(id_semestre) AS num_sem FROM sem_espe WHERE id_especialidad='$idEspecialidad'",$conexion);
if($semestres_espe_=mysql_fetch_assoc($semestres_espe)){
	$num_sem=$semestres_espe_['num_sem']; 
	}
///////////////

for ($i = 1; $i <= $num_sem; $i++){
?>
<div id="semestre-1" class="semestre-1">
	<?php
		#----- LISTA DE MATERIAS -----# 
		$selectMateria = "SELECT * FROM materias WHERE id_semestre = '$i' AND id_especialidad = '$idEspecialidad' AND estatus='1' ORDER BY clave ASC "; #AND id_especialidad = '$idEspecialidad'
		//echo $selectMateria;
		$resultMat = mysql_query($selectMateria,$conexion);
		$total = mysql_num_rows($resultMat)+5;
		if($resultMat_=mysql_fetch_assoc($resultMat)){
			$id_materia=$resultMat_['id_materia'];
			
			// saco el grado de cada materia
			$grado_mat=mysql_query("SELECT grado FROM semestre WHERE id_semestre =$i",$conexion);
			if($grado_mat_=mysql_fetch_assoc($grado_mat)){
				$grado=$grado_mat_['grado'];
				}
			}
		// Se obtiene el ciclo escolar
		$ciclo_mat=mysql_query("SELECT t1.ciclo_escolar, t2.ciclo_escolar FROM calif_parc AS t1, ciclo_escolar AS t2 WHERE t1.id_alumno = '$idAlumno' and t1.id_materia = '$id_materia' AND t1.ciclo_escolar=t2.id_ciclo",$conexion);
		$ciclo_materia='';
		if($ciclo_mat_=mysql_fetch_assoc($ciclo_mat)){
				$ciclo_materia=$ciclo_mat_['ciclo_escolar']; 
			}
		
	?>
	
	<!-- INICIA TABLA DE CALIFICACIONES -->
	<table border=1 width="100%">
		<tr>
			<td class="sinBordes"></td>
			<td class="sinBordes"></td>
			<td class="sinBordes"></td>
			<td class="sinBordes"></td>
			<td colspan=6 class="sin-borde-izquierdo sin-borde-superior ">FECHA DE BAJA</td>
			<td rowspan=<?php echo $total; ?> width=100 class="centrar bordes"> REVISADO Y CONFIRMADO <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/> A&Ntilde;O &nbsp; MES &nbsp; DIA</td>
		</tr>
		<tr>
			<td class="centrar sin-borde-izquierdo sin-borde-derecho"><?php cambiarNumero($i); ?></td>
			<td class="sin-borde-izquierdo sin-borde-derecho">&nbsp;</td>
			<td class="sin-borde-izquierdo sin-borde-derecho">&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan=3 class="centrar bordes">TEMPORAL</td>
			<td colspan=3 class="centrar bordes">DEFINITIVA</td>
		</tr>
		<tr>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"> Semestre </td>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"> PERIODO ESCOLAR <strong><?php echo "<u>".$ciclo_materia."</u>"; ?></strong> </td>
			<td colspan=2 class="centrar sin-borde-izquierdo sin-borde-superior">GRUPO: <?php echo utf8_encode($grado."&deg;"); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td></td>
			<td></td>
		</tr>
		<!-- -->
		<tr>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"></td>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"></td>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"></td>
			<td class="centrar sin-borde-izquierdo sin-borde-superior"></td>
			
			<td colspan=6 style="margin-left: 10px;" align="center">PERIODOS DE REGULARIZACION</td>
		</tr>
		<tr class="centrar">
			<td>CLAVE</td>
			<td>ASIGNATURA</td>
			<td>CALIFICACION<BR/> FINAL</td>
			<td> % DE <BR/>ASISTENCIA </td>
			<!--  -->
			<td> FECHA </td>
			<td> CALIFICA </td>
			<td> FECHA </td>
			<td> CALIFICA </td>
			<td> FECHA </td>
			<td> CALIFICA </td>
		</tr>
			
	<?php
		$obtenerCalificaciones = mysql_query($selectMateria,$conexion);
		while($row = mysql_fetch_array($obtenerCalificaciones)){
		//echo $row["clave"]."<br/>";
		$idMateria = $row["id_materia"];
		echo "<tr>";
				echo "<td class='centrar'>".$row["clave"]."</td>";
				echo "<td height='14px'><span style='font-size: 8px;'>".utf8_encode($row["nombre"])."</span></td>";
				
				$queryCalif1 = "SELECT DISTINCT id_parcial,calificacion FROM calif_parc where id_alumno = '$idAlumno' and id_materia = ". $row["id_materia"] ." ";
				
				$resultCalif1 = mysql_query($queryCalif1,$conexion);
				$totalCalif=0;
				$califSum = 0;
				while($calif1 = mysql_fetch_assoc($resultCalif1)){
					$califSum = $califSum+(float)$calif1['calificacion'];
					$totalCalif = $totalCalif+1;
				}
				if ($totalCalif == 0){
					$totalCalif = 1;
				}
				$califSum = round($califSum/$totalCalif, 1, PHP_ROUND_HALF_DOWN);

				$queryCalif = "SELECT DISTINCT AVG(calificacion) FROM calif_parc where id_alumno = '$idAlumno' and id_materia = ". $row["id_materia"] ." ";				
				$resultCalif = mysql_query($queryCalif,$conexion);
				while($calif = mysql_fetch_assoc($resultCalif)){
					
					//$id_materia = $calif["id_materia"];
					//$cicloEscolarCalif = $calif["ciclo_escolar"];
					if ($califSum == 0) {
						$califSum = ".";	
					}

					$calificacion=substr((string)$califSum,0,3);	
					
					 if (substr($calificacion,0,2)==10) {
					 	echo "<td align='center'><b>".$calificacion."</b></td>";
					 } else {
							 $califAlm = explode('.',$calificacion);
							if(empty($califAlm[0])){
								//echo "vacio";
								echo "<td align='center'><b></b></td>";	
							}elseif (empty($califAlm[1])){
								echo "<td align='center'><b>".$califAlm[0].".0"."</b></td>";
							}else{
								//echo $califAlm[0]."-".$califAlm[1]."<br/>";
								echo "<td align='center'><b>".$calificacion."</b></td>";
							} 
					 }					
					
					// HORAS DEL CURSO
							// **FALTA HACER LA SUMA DE LOS 3 PARCIALES**
							$sel=mysql_query("SELECT SUM(total_horas) AS tot_horas FROM horas_clase WHERE id_materia='".$row["id_materia"]."' ",$conexion);
							$cont_asis='';
							while($cons=mysql_fetch_assoc($sel)):
								$total_horas_clase=$cons['tot_horas'];
									
					#--- consulta de los porcentajes de ASISTENCIA
					$queryPorcenaje = " SELECT porcentaje, SUM(asistencia) AS asisten FROM pase_lista WHERE id_alumno = '$idAlumno' AND id_materia = '".$row["id_materia"]."'  ";					
					$resultQuery = mysql_query($queryPorcenaje,$conexion);						
						$porcen_asis=0;
						while($row = mysql_fetch_array($resultQuery)){
							$asist=$row["asisten"];
							$porcentaje=$row["porcentaje"];
							
							if($asist!=0):
								$porcen_asis=substr((($asist*100)/$total_horas_clase),0,3);																
							else:
								$porcen_asis=$porcentaje;
							endif;
							echo "<td align='center'><b>".$porcen_asis."</b></td>";
							}
							endwhile;
							
					$mat_rep=mysql_query("SELECT * FROM materias_reprobadas WHERE idAlumno='$idAlumno' AND idMateria = '$idMateria' ",$conexion);
					
                                       if($mat_rep_=mysql_fetch_assoc($mat_rep)):
										   $f1=$mat_rep_['fecha1'];
										   $c1= $mat_rep_['calificacion1'];
										   if($f1=='0000-00-00'){
												$f1 = '';
										   }
										   $f2=$mat_rep_['fecha2'];
										   $c2=$mat_rep_['calificacion2'];
										   if($f2=='0000-00-00'){
												$f2 = '';
										   }
										   $f3=$mat_rep_['fecha3'];
										   $c3=$mat_rep_['calificacion3'];
										   if($f3=='0000-00-00'){
												$f3 = '';
										   }
											echo "<td style='font-size: 8px;'>".$f1."</td>";
											echo "<td align='center'>".$c1."</td>";
											echo "<td style='font-size: 8px;'>".$f2."</td>";
											echo "<td align='center'>".$c2."</td>";
											echo "<td style='font-size: 8px;'>".$f3."</td>";
											echo "<td align='center'>".$c3."</td>";
										else:
											echo "<td> </td>";
                                            echo "<td> </td>";
                                            echo "<td> </td>";
                                            echo "<td> </td>";
                                            echo "<td> </td>";
                                            echo "<td> </td>";
									  endif;
					
				
				}
				
				
		echo "</tr>";
		}

	?> 
	</table>
	<!-- TERMINA TABLA DE CALIFICACIONES -->
	
</div>


<?php
	}
	$calif="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	
	//ajuste para promedio generar
	$calificacionParcial1 = mysql_query(" SELECT DISTINCT id_materia,id_parcial,calificacion FROM calif_parc WHERE id_alumno = '$idAlumno' ORDER BY id_materia ASC,id_parcial ASC",$conexion);
	$i = 0;
	$totalizadoParciales=0;
	$promediosPorMateria = "";
	while($resultParcial1=mysql_fetch_assoc($calificacionParcial1)){
		$totalizadoParciales = $totalizadoParciales+(float)$resultParcial1['calificacion'];
		$i = $i+1;
	}
	$promedio_general = round($totalizadoParciales/$i, 1, PHP_ROUND_HALF_DOWN);
	/*
	$calificacionParcial = mysql_query(" SELECT * FROM calif_parc WHERE id_alumno = '$idAlumno' GROUP BY id_materia ",$conexion);
		$i = 0;
		$promediosPorMateria = "";
		while($resultParcial=mysql_fetch_assoc($calificacionParcial)){
		$idMateria = $resultParcial["id_materia"];
		
			
			//$calificacionFinal = "";
			
			
			//Obtenemos Calificaciones
			$calificacionMateria = mysql_query(" SELECT AVG(calificacion) FROM calif_parc WHERE id_alumno = '$idAlumno' AND id_materia = '$idMateria' ",$conexion);
			while($resultMateria = mysql_fetch_assoc($calificacionMateria)){
				$calificacion = $resultMateria["AVG(calificacion)"];
				//$calificacionFinal = $calificacionFinal + $calificacion;
				
			} //W3
			$i++;
			$promediosPorMateria = $calificacion+$promediosPorMateria;			
			
			
		}//W2
		$promedio_general=$promediosPorMateria/$i;
		$promedio_general=substr($promedio_general,0,3);		
	*/
?>
</div>
<div style="margin-top: 700px; ">
	
	<div style="border-bottom: #000 solid 1px; width: 250px;"></div>
	<span style="margin-left: 30px; margin-right: 80px; font-size: 13px; ">DIRECTOR (A) DE LA ESCUELA</span>
	
	<div style="height: 100px; width: 250px; float: right; ">
			<span style=" font-size: 10px; text-align: justify; height: 100px; width: 190px;">PROMEDIO GENERAL <br/> DE APROVECHAMIENTO</span>
			<div style="border: #000 solid 2px; height: 20px; width: 70px; float: right; margin-right: 30px; margin-top: -10px; text-align: center; "><?php echo $promedio_general; ?></div>
	</div>
	
	<div style="border-right: #000 solid 1px; border-left: #000 solid 1px; border-bottom: #000 solid 1px; height: 40px; width: 120px; float: right; font-size: 11px; margin-right: 50px; ">
		<p class="centrar">SELLO <BR/> DE LA ESCUELA</p>
	</div>
	
</div>
</body>
</html>