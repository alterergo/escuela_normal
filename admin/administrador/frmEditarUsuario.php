<?php
include '../../includes/conexion.php';
session_start();

$id = $_POST["id"];
//echo $id;

$cunsultarUsuario = " SELECT * FROM usuarios WHERE id_usuario = $id ";
$respuesta = mysql_query($cunsultarUsuario,$conexion);

while($row = mysql_fetch_array( $respuesta )){
	$id_usuario = $row["id_usuario"];
	$nombre = $row["nombre"];
	$apellido_pat = $row["apellido_pat"];
	$apellido_mat = $row["apellido_mat"];
	$user = $row["user"];
	$pass = $row["pass"];
	$sexo = $row["sexo"];
	$correo = $row["correo"];
	$tipo_usuario = $row["tipo_usuario"];
}


?>
<form id="frmEdtUsr" name="frmEdtUsr" method="POST">
		<fieldset>
		<legend>Editar Administrador</legend>
			<input type="hidden" id="idUsuario" name="idUsuario" value="<?php echo $id_usuario; ?>" />
			<table cellpadding="0" cellspacing="0" border="0" width="97%">
            <tr><td width="35%">
				<label for="nombreEdtUsr">Nombre</label>
			</td>
			<td width="30%">
				<input type="text" id="nombreEdtUsr" name="nombreEdtUsr" class="textinf" value="<?php echo $nombre; ?>" />*
			</td>
			<td width="35%">
				<span id="errNomEdt" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td>
				<label for="apellidoPatEdt">Apellido Paterno</label>
			</td>
			<td>
				<input type="text" id="apellidoPatEdt" name="apellidoPatEdt" class="textinf" value="<?php echo $apellido_pat; ?>" />*
			</td>
			<td>
				<span id="errAppEdt" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td>
				<label for="apellidoMatEdt">Apellido Materno</label>
			</td>	
			<td>
				<input type="text" id="apellidoMatEdt" name="apellidoMatEdt" class="textinf" value="<?php echo $apellido_mat; ?>" />
			</td>
			<td>
				<span id="errApmEdt" class="grid_3 error"></span>
			</tr>
			
			<tr><td>
				<label for="userEdt">Usuario</label>
			</td>
			<td>
				<input type="text" id="userEdt" name="userEdt" class="textinf" value="<?php echo $user; ?>" />*
			</td>
			<td>
				<span id="errUsrEdt" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td>
				<label for="passEdt">Contraseña</label>
			</td>
			<td>
				<input type="password" id="passEdt" name="passEdt" class="textinf" placeholder="Nueva contraseña" />
			</td>
			<td>
            
            </td></tr>
            
			<tr><td>
				<label>Sexo</label>
			</td>
			<td>
			<select id="sexoEdt" name="sexoEdt">
				<option value="">Selecione...</option>
				<option value="H" <?php if($sexo == "H"){?> selected="selected" <?php } ?> >Hombre</option>
				<option value="M" <?php if($sexo == "M"){?> selected="selected" <?php } ?> >Mujer</option>
			</select>*
			</td>
			<td>
				<span id="errSexEdt" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td>
				<label for="correoEdt">Correo</label>
			</td>
			<td>
				<input type="text" id="correoEdt" name="correoEdt" class="textinf" value="<?php echo $correo; ?>" />*
			</td>
			<td>
				<span id="errCorreoEdt" class="grid_3 error"></span>
			</td></tr>
			
			<tr><td>
				<label>Privilegios</label>
			</td>
			<td> 
			<select id="privilegiosEdt" name="privilegiosEdt">
				<option value="">Seleccion...</option>
				<option value="5" <?php if($tipo_usuario == 5){?> selected="selected" <?php } ?> >Director</option>
				<option value="4" <?php if($tipo_usuario == 4){?> selected="selected" <?php } ?> >Control Escolar</option>
				<option value="1" <?php if($tipo_usuario == 1){?> selected="selected" <?php } ?> >Administrativo</option>
                <option value="3" <?php if($tipo_usuario == 3){?> selected="selected" <?php } ?>>Administrador Docentes</option>
			</select>*
			</td>
			<td>
			<span id="errPrivEdt" class="grid_3 error"></span>
            </td></tr>
            
            
            </table>
		</fieldset>		
        <div id="result" class="mensajes" align="center"></div>

	</form>